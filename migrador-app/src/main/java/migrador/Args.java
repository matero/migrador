/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador;

import com.beust.jcommander.Parameter;

import java.util.LinkedList;
import java.util.List;

class Args
{
    @Parameter(names = {"--help", "-h"}, help = true, description = "Show this fantastic help ;)")
    boolean help;

    @Parameter(names = {"--version", "-v"}, description = "Show migrador version and exits")
    boolean showVersion;

    @Parameter(
        names = {"--log", "-l"},
        description = "level of log to use during execution, possible are: 'trace', 'debug', 'info', 'error, 'warn' (this is the default).",
        arity = 1,
        validateWith = IsLogLevel.class)
    String log = "warn";

    @Parameter(names = {"--workDir", "-w"}, description = "Migrador base work directory, states the root where to look for migration resources.")
    String workDir;

    @Parameter(names = {"--password", "-p"}, description = "Password to authenticate to neo4j db.", password = true)
    String password;

    @Parameter(names = {"--config", "-c"}, description = "URL to user defined configuration on a properties file.")
    String config = "classpath:migrador/empty-conf.properties";

    @Parameter(names = "--development", hidden = true)
    boolean devenv;

    @Parameter(description = "commands to be executed by migrador.")
    List<Migrador.Command> commands = new LinkedList<>();

    boolean hasPassword() {return null != this.password;}

    boolean hasWorkDir() {return null != this.workDir;}
}
