/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador;

import ch.qos.logback.classic.LoggerContext;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import org.aeonbits.owner.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Properties;

import static ch.qos.logback.classic.Level.toLevel;

public class App
{
    private static final Logger LOG = LoggerFactory.getLogger(App.class);
    private static final int USAGE_ERROR_EXIT_CODE = 64;
    private static final int OK_EXIT_CODE = 0;

    private final Args args;

    App(final Args args) {this.args = args;}

    public static void main(final String[] arguments)
    {
        final var args = new Args();

        final var cmdline = JCommander.newBuilder()
                                      .addObject(args)
                                      .programName(Migrador.NAME)
                                      .build();

        if (0 == arguments.length) {
            cmdline.usage();
        } else {
            try {
                cmdline.parse(arguments);
                if (args.help) {
                    cmdline.usage();
                } else {
                    new Executor(args).executeMigrador();
                }
            } catch (final ParameterException e) {
                System.err.println(e.getMessage());
                System.exit(USAGE_ERROR_EXIT_CODE);
            }
        }

        System.exit(OK_EXIT_CODE);
    }

    private void run()
    {
        if (this.args.showVersion) {
            System.out.println(Migrador.description());
        } else {
            runMigrador();
        }
    }

    private void runMigrador()
    {
        setLogLevel();
        try (final var migrador = createMigrador()) {
            migrador.execute(List.copyOf(this.args.commands));
        }
    }

    private Migrador createMigrador()
    {
        final var config = loadConfiguration();

        return Migrador.withConfig(config).create();
    }

    private MigradorConfig loadConfiguration()
    {
        if (null != this.args.config && !this.args.config.isBlank()) {
            ConfigFactory.setProperty(MigradorConfig.USER_CONFIG, this.args.config.trim());
        }
        final var config = ConfigFactory.create(MigradorConfig.class, getExtraConfigs());
        LOG.info("Migrador configuration loaded");
        return config;
    }

    private Properties getExtraConfigs()
    {
        final var properties = new Properties();
        if (this.args.devenv) {
            try {
                final File workDir = Files.createTempDirectory("dev_migrate_work_dir").toFile();
                if (!new File(workDir, "db/migrations").mkdirs()) {
                    throw new IllegalStateException("could not create a location resource on temp dir.");
                }
                properties.setProperty("workDir", workDir.getAbsolutePath());
            } catch (final IOException e) {
                throw new IllegalStateException("could not create a temp dir to work with.", e);
            }
        } else {
            if (this.args.hasWorkDir()) {
                properties.setProperty("workDir", this.args.workDir);
            }
        }
        if (this.args.hasPassword()) {
            properties.setProperty("cmdLinePassword", this.args.password);
        }
        return properties;
    }

    void setLogLevel()
    {
        final var level = toLevel(this.args.log);
        System.out.println("Setting log level to: " + level);

        final var loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        final var logger = loggerContext.getLogger("migrador");
        logger.setLevel(level);
    }
}
