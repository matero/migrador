<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright 2020-2020 matero@gmail.com.
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
  ~ this software and associated documentation files (the "Software"), to deal in
  ~ the Software without restriction, including without limitation the rights to
  ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
  ~ of the Software, and to permit persons to whom the Software is furnished to do
  ~ so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  ~ SOFTWARE.
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <groupId>com.gitlab.matero</groupId>
        <artifactId>migrador-neo4j</artifactId>
        <version>0.1.1-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>migrador-lib</artifactId>
    <name>Migrador Library</name>

    <dependencies>
        <dependency>
            <groupId>org.neo4j.driver</groupId>
            <artifactId>neo4j-java-driver</artifactId>
        </dependency>

        <dependency>
            <groupId>org.aeonbits.owner</groupId>
            <artifactId>owner-java8</artifactId>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.antlr</groupId>
            <artifactId>ST4</artifactId>
            <version>4.3</version>
        </dependency>
        <dependency>
            <groupId>org.antlr</groupId>
            <artifactId>antlr4-runtime</artifactId>
            <version>4.8-1</version>
        </dependency>
        <dependency>
            <groupId>com.github.freva</groupId>
            <artifactId>ascii-table</artifactId>
            <version>1.1.0</version>
        </dependency>

        <!-- testing dependencies -->
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-params</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-junit-jupiter</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.neo4j.test</groupId>
            <artifactId>neo4j-harness</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>1.7.30</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>

        <plugins>
            <plugin>
                <groupId>org.antlr</groupId>
                <artifactId>antlr4-maven-plugin</artifactId>
                <version>4.3</version>
                <executions>
                    <execution>
                        <id>antlr</id>
                        <goals>
                            <goal>antlr4</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <includes>
                        <include>**/*_tests.java</include>
                    </includes>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.assertj</groupId>
                <artifactId>assertj-assertions-generator-maven-plugin</artifactId>
                <version>2.1.0</version>
                <configuration>
                    <classes>
                        <param>migrador.migrations.Version</param>
                        <param>migrador.resources.ResourceName</param>
                        <param>migrador.actions.CleanResult</param>
                        <param>migrador.actions.ValidateResult</param>
                        <param>migrador.actions.MigrationsStatus</param>
                    </classes>

                    <!-- Whether generated assertions classes can be inherited with consistent assertion chaining -->
                    <hierarchical>true</hierarchical>

                    <!--
                      If true, generate assertions for all fields whatever their visibility is (including private).
                      Default to false which means assertions are only generated for public fields.
                    -->
                    <generateAssertionsForAllFields>true</generateAssertionsForAllFields>

                    <!-- Where to generate assertions entry point classes -->
                    <entryPointClassPackage>migrador.assertions</entryPointClassPackage>

                    <!-- If true, all files in targetDir are removed before generating new ones (default to false) -->
                    <cleanTargetDir>true</cleanTargetDir>

                    <!-- Select which assertions entry point classes to generate -->
                    <generateAssertions>true</generateAssertions>
                    <generateBddAssertions>false</generateBddAssertions>
                    <generateSoftAssertions>true</generateSoftAssertions>
                    <generateJUnitSoftAssertions>false</generateJUnitSoftAssertions>

                    <!--
                      Set the scope of generated sources when added to the maven build.
                      Value must be 'test' or 'compile' to and defaults to 'test'.
                    -->
                    <generatedSourcesScope>test</generatedSourcesScope>

                    <!-- If true, the plugin does not produce any logs (default to false) -->
                    <quiet>false</quiet>

                    <!--
                      The generated assertions report is written to the given file.
                      The base directory containing the report file is where the plugin is executed.
                    -->
                    <writeReportInFile>target/assertions-generation-report.txt</writeReportInFile>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>generate-assertions</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
