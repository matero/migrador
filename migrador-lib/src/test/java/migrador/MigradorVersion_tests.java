/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador;

import migrador.helpers.Classes;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class MigradorVersion_tests
{
    private final Classes classes = Classes.of(MigradorVersion_tests.class.getClassLoader());

    @Test
    void when_version_file_is_accessible_it_should_return_its_contents_discarding_any_kind_of_blanks()
    {
        final String version = MigradorVersion.READER.getVersion(this.classes);

        assertThat(version).isEqualTo("0.1.0");
    }

    @Test
    void when_version_file_doesnt_exist_it_should_fail_with_MigradorVersionNotDefined()
    {
        assertThatThrownBy(() -> MigradorVersion.READER.readVersion(this.classes, "unknown/version.txt"))
            .isInstanceOf(MigradorVersion.NotDefined.class)
            .hasMessageContaining("unknown/version.txt isn't defined!")
            .hasNoCause();
    }
}
