/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador;

import migrador.resources.Location;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.api.io.TempDir;
import org.neo4j.driver.Driver;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.fail;

final class MigradorFactory_tests extends Neo4jClientTest
{
    private static final TestMigradorConfig config = new TestMigradorConfig();
    private static final MigradorFactory factory = new MigradorFactory(config, Thread.currentThread().getContextClassLoader());

    MigradorFactory_tests(final Driver driver) {super(driver);}

    static class getWorkDir
    {
        @Test
        void when_isnt_configured_then_it_should_use_current_execution_directory()
        {
            config.withoutWorkDir();
            assertThat(factory.getWorkDir()).isEqualTo(MigradorFactory.toAbsolutePath(MigradorFactory.currentExecutionDir()));
        }

        @Test
        void when_configured_if_it_doesnt_exist_it_should_fail_with_IllegalArgumentException(@TempDir final File dir)
        {
            final var notExist = new File(dir, "test.txt");
            config.withWorkDir(notExist);
            assertThatThrownBy(factory::getWorkDir)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("workDir='" + notExist + "' doesn't exists.")
                .hasNoCause();
        }

        @Test
        void when_configured_if_its_not_a_directory_it_should_fail_with_IllegalArgumentException(@TempDir final Path dir)
            throws IOException
        {
            final var file = Files.createTempFile(dir, "conf", ".properties").toFile();
            config.withWorkDir(file);
            assertThatThrownBy(factory::getWorkDir)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("workDir='" + file + "' is not a directory.")
                .hasNoCause();
        }

        @Test
        @DisabledOnOs(OS.WINDOWS)
        void when_configured_if_its_not_readable_it_should_fail_with_IllegalArgumentException(@TempDir final Path dir)
            throws IOException
        {
            final var notReadable = Files.createTempDirectory(dir, "conf").toFile();
            try {
                if (!notReadable.setReadable(false, false) || !notReadable.setExecutable(false, false)) {
                    fail("could not create a un-readable dir");
                }

                config.withWorkDir(notReadable);
                assertThatThrownBy(factory::getWorkDir)
                    .isInstanceOf(IllegalArgumentException.class)
                    .hasMessage("workDir='" + notReadable + "' is not readable.")
                    .hasNoCause();
            } finally {
                // we need to restore the permissions, so the temp dir can be deleted by junit
                notReadable.setReadable(true, true);
                notReadable.setExecutable(true, true);
            }
        }

        @Test
        void when_configured_correctly_it_should_be_used(@TempDir final Path dir)
            throws IOException
        {
            config.withWorkDir(Files.createTempDirectory(dir, "conf"));
            assertThat(factory.getWorkDir()).isEqualTo(MigradorFactory.toAbsolutePath(config.workDir()));
        }
    }

    @Test
    @DisplayName("when no locations are configured it should try to use filesystem:db/migrations")
    void when_no_locations_are_configured_it_should_try_to_use_default_location()
    {
        config
            .withWorkDir(new File(MigradorFactory.currentExecutionDir(), "src/test/resources/"))
            .withoutLocations();
        assertThat(factory.getConfiguredLocations())
            .hasSize(1)
            .contains(Location.of(Location.FILESYSTEM_PREFIX + config.workDir().getAbsolutePath() + "/db/migrations"));
    }

    @Test
    void when_one_configured_location_doesnt_exist_should_fail_with_LocationNotFound()
    {
        config.withLocations("migradores", "peperino");
        assertThatThrownBy(factory::getConfiguredLocations)
            .isInstanceOf(Location.NotFound.class);
    }
}
