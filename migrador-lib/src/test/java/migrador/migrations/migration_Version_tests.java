/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.migrations;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import utils.Combinator;

import java.util.stream.Stream;

import static migrador.assertions.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class migration_Version_tests
{
    @Test
    void EMPTY_version_should_be_represented_by_an_empty_string()
    {
        assertThat(Version.empty()).hasVersion("");
    }

    @Test
    void NONE_version_should_be_represented_by_an_empty_string()
    {
        assertThat(Version.none()).hasVersion("");
    }

    @Test
    void LATEST_version_should_be_represented_by_the_system_maximum_long_value()
    {
        assertThat(Version.latest()).hasVersion(String.valueOf(Long.MAX_VALUE));
    }

    @ParameterizedTest
    @MethodSource("versions")
    void any_other_Version_created_from_text_should_have_its_displayText_correctly_build(final String text, final String expectedDisplayText)
    {
        //given
        final Version version = Version.make(text);

        // expect
        assertThat(version).hasDisplayText(expectedDisplayText);
    }

    @Test
    void version_should_be_NONE_when_make_receives_null()
    {
        assertThat(Version.make(null)).isNone();
    }

    @Test
    void version_should_be_EMPTY_when_make_receives_empty_string()
    {
        assertThat(Version.make("")).isEmpty();
    }

    @Test
    void version_should_be_CURRENT_when_make_receives_current_no_matter_its_case()
    {
        Combinator.of("current")
                  .forEach(currentCombination -> assertThat(Version.make(currentCombination)).isCurrent());
    }

    @Test
    void version_should_be_LATEST_when_make_receives_latest_no_matter_its_case()
    {
        Combinator.of("latest")
                  .forEach(currentCombination -> assertThat(Version.make(currentCombination)).isLatest());
    }

    @ParameterizedTest
    @MethodSource("illegalVersions")
    void make_should_fail_when_illegal_chars_are_found(final String illegalVersion, final String reportedVersion)
    {
        assertThatThrownBy(() -> Version.make(illegalVersion))
            .isInstanceOf(Version.IllegalVersion.class)
            .hasMessageContaining("Version may only contain 0..9 and . (dot). Invalid version: " + reportedVersion)
            .hasNoCause();
    }

    static Stream<Arguments> versions()
    {
        return Stream.of(
            arguments("1_1", "1.1"),
            arguments("1", "1"),
            arguments("2_1_1", "2.1.1")
                        );
    }

    static Stream<Arguments> illegalVersions()
    {
        return Stream.of(
            arguments("saracatunga", "saracatunga"),
            arguments("1_2_a", "1.2.a"),
            arguments("1.2_*", "1.2.*"),
            arguments("2_*", "2.*")
                        );
    }
}
