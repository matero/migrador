/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.migrations.resolvers;

import migrador.migrations.Version;
import migrador.resources.ResourceName;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static migrador.assertions.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class MigrationNameParser_tests
{
    private final NameInterpreter interpreter = new NameInterpreter();

    @Test
    void should_be_able_to_parse_versioned_resources()
    {
        //when
        final ResourceName name = parse("V14_08.76__Mi_Nacimiento_en_1976");

        assertThat(name)
            .isNotNull()
            .hasPrefix("V")
            .hasDescription("Mi_Nacimiento_en_1976")
            .hasVersion(Version.make("14_08.76"));
    }

    @Test
    void should_be_able_to_parse_repeatable_resources()
    {
        final ResourceName name = parse("R__Que_gust0_ti3ne_LA_s4l");

        assertThat(name)
            .isNotNull()
            .hasPrefix("R")
            .hasDescription("Que_gust0_ti3ne_LA_s4l");
    }

    @ParameterizedTest
    @ValueSource(strings = {"without_prefix", "V__withoutVersion", "V1.2withoutSeparator", "RwithoutSeparator"})
    void should_fail_with_invalid_resource_names(final String invalidResourceName)
    {
        assertThatThrownBy(() -> parse(invalidResourceName))
            .isInstanceOf(ResourceName.Invalid.class)
            .hasMessageStartingWith("Failed interpret migration name due to ")
            .hasNoCause();
    }

    private ResourceName parse(final String resourceName)
    {
        this.interpreter.reset();

        final MigrationNameLexer lexer = new MigrationNameLexer(CharStreams.fromString(resourceName));
        final MigrationNameParser parser = new MigrationNameParser(new CommonTokenStream(lexer));
        parser.addErrorListener(ResourceNameErrorListener.getInstance());
        parser.addParseListener(this.interpreter);

        parser.migrationName();

        return this.interpreter.getResourceName();
    }
}

