/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.actions;

import migrador.Migrador;
import migrador.MigrationsAsciiTable;
import migrador.Neo4jClientTest;
import migrador.helpers.Classes;
import migrador.migrations.Version;
import migrador.migrations.resolvers.CypherRenderer;
import migrador.migrations.resolvers.MigrationResolver;
import migrador.resources.Location;
import migrador.resources.LocationScannerCache;
import migrador.resources.ResourceNameCache;
import migrador.resources.ResourceScanner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.neo4j.driver.Driver;

import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.List;

class Migrate_tests extends Neo4jClientTest.withSessionSupport
{
    final Classes classes = Classes.get();
    private MigrationResolver migrationResolver;

    protected Migrate_tests(final Driver driver)
    {
        super(driver);
        final var migrate = Migrador.Command.migrate;
        final var scanner = scan("classpath:db/migrations")
            .classes(this.classes)
            .encoding(StandardCharsets.UTF_8)
            .resourceNameCache(new ResourceNameCache())
            .locationScannerCache(new LocationScannerCache())
            .build();
        this.migrationResolver = MigrationResolver.composite()
                                                  .dontSkipDefaultResolvers()
                                                  .resourceProvider(migrate.resourceProvider(false, scanner))
                                                  .classProvider(migrate.classProvider(false, scanner))
                                                  .classes(this.classes)
                                                  .renderer(CypherRenderer.textRenderer())
                                                  .build();

    }

    @BeforeAll void clean()
    {
        withNewSession(Clean.ENABLED::execute);
    }

    @AfterEach @BeforeEach void showStatus()
    {
        final var status = GetMigrationsStatus.of(Version.latest())
                           .migrationResolver(this.migrationResolver)
                           .allowOutOfOrderMigrations(true)
                           .execute(session());

        final var table = MigrationsAsciiTable.of(status);
        logger().info("\n\nSchema version: {}\n\n{}\n", status.getCurrentVersion(), table);
    }

    @Test void should_work()
    {
        // ensure that we have Migrador installed in test DB
        withNewSession(Baseline.of(Version.latest()).executedBy("jj")::execute);

        // Migrate
        Migrate.of(Version.make("1.2"))
               .executedBy("jj")
               .baseline()
               .migrationResolver(this.migrationResolver)
               .allowOutOfOrderMigrations(true)
               .allowPendingMigrations(true)
               .allowMissingMigrations(true)
               .allowIgnoredMigrations(true)
               .allowFutureMigrations(true)
               .execute(session());
    }

    private ResourceScanner.Builder scan(final String location)
    {
        return scan(Location.of(location));
    }

    private ResourceScanner.Builder scan(final Location location)
    {
        return scan(List.of(location));
    }

    private ResourceScanner.Builder scan(final Collection<Location> locations)
    {
        return ResourceScanner.scan(locations);
    }
}
