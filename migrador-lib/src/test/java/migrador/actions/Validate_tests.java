/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.actions;

import migrador.migrations.DatabaseMigrations;
import migrador.migrations.Migration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.neo4j.driver.Session;
import org.neo4j.driver.TransactionWork;

import java.util.List;

import static migrador.assertions.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

class Validate_tests
{
    @Test
    void it_should_detect_when_Migrador_is_not_installed()
    {
        final DatabaseMigrations databaseMigrations = mock(DatabaseMigrations.class);
        @SuppressWarnings("unchecked") final TransactionWork<Boolean> checkInstalled = mock(TransactionWork.class);
        final Session session = mock(Session.class);
        final Validate validate = new Validate(checkInstalled, databaseMigrations);

        when(session.readTransaction(checkInstalled))
            .thenReturn(false);

        assertThat(validate.execute(session))
            .isNotNull()
            .hasMessage("Migrador is not installed yet.");
    }

    @Nested
    class When_Migrador_is_installed
    {
        private final DatabaseMigrations databaseMigrations = mock(DatabaseMigrations.class);
        @SuppressWarnings("unchecked")
        private final TransactionWork<Boolean> checkInstalled = mock(TransactionWork.class);
        private final Session session = mock(Session.class);
        private final Validate validate = new Validate(this.checkInstalled, this.databaseMigrations);

        @BeforeEach
        void setMigradorInstalled()
        {
            reset(this.databaseMigrations, this.checkInstalled, this.session);


            when(this.session.readTransaction(this.checkInstalled))
                .thenReturn(true);
        }

        @Test
        void and_no_migration_is_found_then_an_error_is_reported()
        {
            when(this.session.readTransaction(this.checkInstalled))
                .thenReturn(true);
            when(this.databaseMigrations.findAll(this.session))
                .thenReturn(List.of());

            assertThatThrownBy(() -> this.validate.execute(this.session))
                .isInstanceOf(Validate.InvalidMigrations.class)
                .hasMessage("No migrations found. Are your locations set up correctly?")
                .hasNoCause();
        }

        @Test
        void and_validation_fails_then_they_are_reported()
        {
            final var m = mock(Migration.class);
            when(this.databaseMigrations.findAll(this.session))
                .thenReturn(List.of(m, m, m, m, m));
            when(this.databaseMigrations.validate(this.session))
                .thenReturn("errors where found.");

            assertThatThrownBy(() -> this.validate.execute(this.session))
                .isInstanceOf(Validate.InvalidMigrations.class)
                .hasMessage("errors where found.")
                .hasNoCause();
        }

        @Test
        void and_validation_is_successfull_then_a_success_message_is_reported()
        {
            final var m = mock(Migration.class);
            when(this.databaseMigrations.findAll(this.session))
                .thenReturn(List.of(m));
            when(this.databaseMigrations.validate(this.session))
                .thenReturn(null);

            assertThat(this.validate.execute(this.session))
                .isNotNull()
                .hasMessage("Successfully validated 1 migration.")
                .hasValidatedMigrations(1);

            reset(this.databaseMigrations);

            when(this.databaseMigrations.findAll(this.session))
                .thenReturn(List.of(m, m));
            when(this.databaseMigrations.validate(this.session))
                .thenReturn(null);

            assertThat(this.validate.execute(this.session))
                .isNotNull()
                .hasMessage("Successfully validated 2 migrations.")
                .hasValidatedMigrations(2);
        }
    }
}
