/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class TestMigradorConfig implements MigradorConfig
{
    private boolean skipDefaultResolvers;
    private List<String> locations;
    private Charset encoding = StandardCharsets.UTF_8;
    private boolean renderCypherScriptAsTemplates;
    private Character delimiterStart;
    private Character delimiterEnd;
    private Map<String, Object> templateParameters;
    private int connectRetries = 5;
    private String initCypher;
    private boolean cleanDisabled = true;
    private String neo4jUri;
    private String authTokenType;
    private String authPrincipal;
    private String authCredentials;
    private String authRealm;
    private String authScheme;
    private Map<String, Object> authParameters;
    private boolean baselineOnMigrate;
    private boolean validateOnMigrate;
    private File workDir;
    private String cmdLinePassword;
    private int dbPoolSize = 2;
    private long dbConnectionAcquisitionTimeout = 20;
    private TimeUnit dbConnectionAcquisitionTimeoutUnit = TimeUnit.SECONDS;
    private boolean dbConnectionEncrypted = false;
    private long dbConnectionLifetime = 30;
    private TimeUnit dbConnectionLifetimeUnit = TimeUnit.MINUTES;
    private long dbTransactionRetryTime = 15;
    private TimeUnit dbTransactionRetryTimeUnit = TimeUnit.SECONDS;
    private String dbLogger = "console";
    private String dbLoggingLevel = "WARNING";
    private String target = "latest";
    private boolean outOfOrder;
    private boolean ignorePendingMigrations;
    private boolean ignoreMissingMigrations;
    private boolean ignoreIgnoredMigrations;
    private boolean ignoreFutureMigrations;
    private String username;

    @Override public String username() {return this.username;}

    public TestMigradorConfig username(final String value)
    {
        this.username = value;
        return this;
    }

    @Override public String target() {return this.target;}

    public TestMigradorConfig target(final String value)
    {
        this.target = value;
        return this;
    }

    @Override public boolean skipDefaultResolvers() {return this.skipDefaultResolvers;}

    public TestMigradorConfig skipDefaultResolvers(final boolean value)
    {
        this.skipDefaultResolvers = value;
        return this;
    }

    @Override public List<String> locations()
    {
        if (null == this.locations) {
            this.locations = new LinkedList<>();
            this.locations.add("src/test/resources/db/migrations");
        }
        return this.locations;
    }

    public TestMigradorConfig withoutLocations()
    {
        if (null == this.locations) {
            this.locations = new LinkedList<>();
        } else {
            this.locations.clear();
        }
        return this;
    }

    public TestMigradorConfig withLocation(final String value)
    {
        if (null == this.locations) {
            this.locations = new LinkedList<>();
        } else {
            this.locations.clear();
        }
        this.locations.add(value);
        return this;
    }

    public TestMigradorConfig withLocations(final String first, final String... values)
    {
        if (null == this.locations) {
            this.locations = new LinkedList<>();
        } else {
            this.locations.clear();
        }
        this.locations.add(first);
        Collections.addAll(this.locations, values);
        return this;
    }

    public TestMigradorConfig withLocations(final Iterable<String> values)
    {
        if (null == this.locations) {
            this.locations = new LinkedList<>();
        } else {
            this.locations.clear();
        }
        values.forEach(this.locations::add);
        return this;
    }

    public TestMigradorConfig clearLocations()
    {
        if (null != this.locations) {
            this.locations.clear();
        }
        return this;
    }

    @Override public Charset encoding() {return this.encoding;}

    public TestMigradorConfig withEncoding(final Charset value)
    {
        this.encoding = value;
        return this;
    }

    @Override public boolean renderCypherScriptAsTemplates() {return this.renderCypherScriptAsTemplates;}

    public TestMigradorConfig withRenderCypherScriptAsTemplates(final boolean value)
    {
        this.renderCypherScriptAsTemplates = value;
        return this;
    }

    @Override public Character delimiterStart() {return this.delimiterStart;}

    public TestMigradorConfig withDelimiterStart(final Character value)
    {
        this.delimiterStart = value;
        return this;
    }

    @Override public Character delimiterEnd() {return this.delimiterEnd;}

    public TestMigradorConfig withDelimiterEnd(final Character value)
    {
        this.delimiterEnd = value;
        return this;
    }

    @Override public Map<String, Object> templateParameters() {return this.templateParameters;}

    public TestMigradorConfig withTemplateParameter(final String name, final Object value)
    {
        if (null == this.templateParameters) {
            this.templateParameters = new HashMap<>();
        }
        this.templateParameters.put(name, value);
        return this;
    }

    public TestMigradorConfig withTemplateParameters(final Map<String, Object> values)
    {
        if (null == this.templateParameters) {
            this.templateParameters = new HashMap<>();
        } else {
            this.templateParameters.clear();
        }
        this.templateParameters.putAll(values);
        return this;
    }

    public TestMigradorConfig clearTemplateParameters()
    {
        if (null != this.templateParameters) {
            this.templateParameters.clear();
        }
        return this;
    }

    @Override public int connectRetries() {return this.connectRetries;}

    public TestMigradorConfig withConnectRetries(final int value)
    {
        this.connectRetries = value;
        return this;
    }

    @Override public String initCypher() {return this.initCypher;}

    public TestMigradorConfig withInitCypher(final String value)
    {
        this.initCypher = value;
        return this;
    }

    @Override public boolean cleanEnabled() {return this.cleanDisabled;}

    public TestMigradorConfig withCleanDisabled()
    {
        return withCleanDisabled(true);
    }

    public TestMigradorConfig withCleanEnabled()
    {
        return withCleanDisabled(false);
    }

    public TestMigradorConfig withCleanDisabled(final boolean value)
    {
        this.cleanDisabled = value;
        return this;
    }

    @Override public String neo4jUri() {return this.neo4jUri;}

    public TestMigradorConfig withNeo4jUri(final String value)
    {
        this.neo4jUri = value;
        return this;
    }

    @Override public String authTokenType() {return this.authTokenType;}

    public TestMigradorConfig withAuthTokenType(final String value)
    {
        this.authTokenType = value;
        return this;
    }

    @Override public String authPrincipal() {return this.authPrincipal;}

    public TestMigradorConfig withAuthPrincipal(final String value)
    {
        this.authPrincipal = value;
        return this;
    }

    @Override public String authCredentials() {return this.authCredentials;}

    public TestMigradorConfig withAuthCredentials(final String value)
    {
        this.authCredentials = value;
        return this;
    }

    @Override public String authRealm() {return this.authRealm;}

    public TestMigradorConfig withAuthRealm(final String value)
    {
        this.authRealm = value;
        return this;
    }

    public TestMigradorConfig withoutAuthRealm()
    {
        return withAuthRealm(null);
    }

    @Override public String authScheme() {return this.authScheme;}

    public TestMigradorConfig withAuthScheme(final String value)
    {
        this.authScheme = value;
        return this;
    }

    @Override public Map<String, Object> authParameters() {return this.authParameters;}

    public TestMigradorConfig withAuthParameter(final String name, final Object value)
    {
        if (null == this.authParameters) {
            this.authParameters = new HashMap<>();
        }
        this.authParameters.put(name, value);
        return this;
    }

    public TestMigradorConfig withAuthParameters(final Map<String, Object> values)
    {
        if (null == this.authParameters) {
            this.authParameters = new HashMap<>();
        } else {
            this.authParameters.clear();
        }
        this.authParameters.putAll(values);
        return this;
    }

    public TestMigradorConfig clearAuthParameter()
    {
        if (null != this.authParameters) {
            this.authParameters.clear();
        }
        return this;
    }

    @Override public boolean baselineOnMigrate() {return this.baselineOnMigrate;}

    public TestMigradorConfig withBaselineOnMigrate(final boolean value)
    {
        this.baselineOnMigrate = value;
        return this;
    }

    @Override public boolean validateOnMigrate() {return this.validateOnMigrate;}

    public TestMigradorConfig withValidateOnMigrate(final boolean value)
    {
        this.validateOnMigrate = value;
        return this;
    }

    @Override public File workDir() {return this.workDir;}

    public TestMigradorConfig withoutWorkDir() {return withWorkDir((File) null);}

    public TestMigradorConfig withWorkDir(final Path value) {return withWorkDir(value.toFile());}

    public TestMigradorConfig withWorkDir(final File value)
    {
        this.workDir = value;
        return this;
    }

    @Override public String cmdLinePassword() {return this.cmdLinePassword;}

    public TestMigradorConfig withCmdLinePassword(final String value)
    {
        this.cmdLinePassword = value;
        return this;
    }

    @Override public int dbPoolSize() {return this.dbPoolSize;}

    public TestMigradorConfig dbPoolSize(final int value)
    {
        this.dbPoolSize = value;
        return this;
    }

    @Override public long dbConnectionAcquisitionTimeout() {return this.dbConnectionAcquisitionTimeout;}

    public TestMigradorConfig dbConnectionAcquisitionTimeout(final long value)
    {
        this.dbConnectionAcquisitionTimeout = value;
        return this;
    }

    @Override public TimeUnit dbConnectionAcquisitionTimeoutUnit() {return this.dbConnectionAcquisitionTimeoutUnit;}

    public TestMigradorConfig dbConnectionAcquisitionTimeoutUnit(final TimeUnit value)
    {
        this.dbConnectionAcquisitionTimeoutUnit = value;
        return this;
    }

    @Override public boolean dbConnectionEncrypted() {return this.dbConnectionEncrypted;}

    public TestMigradorConfig dbConnectionEncrypted(final boolean value)
    {
        this.dbConnectionEncrypted = value;
        return this;
    }

    @Override public long dbConnectionLifetime() {return this.dbConnectionLifetime;}

    public TestMigradorConfig dbConnectionLifetime(final long value)
    {
        this.dbConnectionLifetime = value;
        return this;
    }

    @Override public TimeUnit dbConnectionLifetimeUnit() {return this.dbConnectionLifetimeUnit;}

    public TestMigradorConfig dbConnectionLifetimeUnit(final TimeUnit value)
    {
        this.dbConnectionLifetimeUnit = value;
        return this;
    }

    @Override public long dbTransactionRetryTime() {return this.dbTransactionRetryTime;}

    public TestMigradorConfig dbTransactionRetryTime(final long value)
    {
        this.dbTransactionRetryTime = value;
        return this;
    }

    @Override public TimeUnit dbTransactionRetryTimeUnit() {return this.dbTransactionRetryTimeUnit;}

    public TestMigradorConfig dbTransactionRetryTimeUnit(final TimeUnit value)
    {
        this.dbTransactionRetryTimeUnit = value;
        return this;
    }

    @Override public String dbLogger() {return this.dbLogger;}

    public TestMigradorConfig dbLogger(final String value)
    {
        this.dbLogger = value;
        return this;
    }

    @Override public String dbLoggingLevel() {return this.dbLoggingLevel;}

    public TestMigradorConfig dbLoggingLevel(final String value)
    {
        this.dbLoggingLevel = value;
        return this;
    }

    @Override public boolean outOfOrder() {return this.outOfOrder;}

    public TestMigradorConfig withOutOfOrderDisabled()
    {
        return withOutOfOrder(false);
    }

    public TestMigradorConfig withOutOfOrderEnabled()
    {
        return withOutOfOrder(true);
    }

    public TestMigradorConfig withOutOfOrder(final boolean value)
    {
        this.outOfOrder = value;
        return this;
    }

    @Override public boolean ignorePendingMigrations() {return this.ignorePendingMigrations;}

    public TestMigradorConfig withIgnorePendingMigrationsDisabled()
    {
        return withIgnorePendingMigrations(false);
    }

    public TestMigradorConfig withIgnorePendingMigrationsEnabled()
    {
        return withIgnorePendingMigrations(true);
    }

    public TestMigradorConfig withIgnorePendingMigrations(final boolean value)
    {
        this.ignorePendingMigrations = value;
        return this;
    }

    @Override public boolean ignoreMissingMigrations() {return this.ignoreMissingMigrations;}

    public TestMigradorConfig withIgnoreMissingMigrationsDisabled()
    {
        return withIgnoreMissingMigrations(false);
    }

    public TestMigradorConfig withIgnoreMissingMigrationsEnabled()
    {
        return withIgnoreMissingMigrations(true);
    }

    public TestMigradorConfig withIgnoreMissingMigrations(final boolean value)
    {
        this.ignoreMissingMigrations = value;
        return this;
    }

    @Override public boolean ignoreIgnoredMigrations() {return this.ignoreIgnoredMigrations;}

    public TestMigradorConfig withIgnoreIgnoredMigrationsDisabled()
    {
        return withIgnoreIgnoredMigrations(false);
    }

    public TestMigradorConfig withIgnoreIgnoredMigrationsEnabled()
    {
        return withIgnoreIgnoredMigrations(true);
    }

    public TestMigradorConfig withIgnoreIgnoredMigrations(final boolean value)
    {
        this.ignoreIgnoredMigrations = value;
        return this;
    }

    @Override public boolean ignoreFutureMigrations() {return this.ignoreFutureMigrations;}

    public TestMigradorConfig withIgnoreFutureMigrationsDisabled()
    {
        return withIgnoreFutureMigrations(false);
    }

    public TestMigradorConfig withIgnoreFutureMigrationsEnabled()
    {
        return withIgnoreFutureMigrations(true);
    }

    public TestMigradorConfig withIgnoreFutureMigrations(final boolean value)
    {
        this.ignoreFutureMigrations = value;
        return this;
    }
}
