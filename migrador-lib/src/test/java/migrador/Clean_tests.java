/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador;

import migrador.actions.Action;
import migrador.actions.Clean;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.neo4j.driver.Driver;
import org.neo4j.driver.Session;

import java.util.function.BiConsumer;

import static migrador.assertions.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoInteractions;

class Clean_tests extends Neo4jClientTest.withSessionAndTransactionSupport
{
    Clean_tests(final Driver driver) {super(driver);}

    @BeforeAll void cleanDB() { Clean.ENABLED.execute(session()); }

    @Test void DISABLED_should_fail_with_UnableToExecute()
    {
        final var session = mock(Session.class);

        assertThatThrownBy(() -> Clean.DISABLED.execute(session))
            .isInstanceOf(Action.UnableToExecute.class)
            .hasMessage("Unable to execute clean as it has been disabled with the 'migrador.cleanDisabled' property.")
            .hasNoCause();

        verifyNoInteractions(session);
    }

    @ParameterizedTest @ValueSource(ints = {0, 3, 2, 8}) void ENABLED_should_delete_all_orphan_nodes_AND_nodes_with_relationships(final int count)
    {
        applyFixture(count, this::addNodesWithRelationship, (sb, i) -> sb.append(", "), this::addOrphanNode);
        assertThat(Clean.ENABLED.execute(session()))
            .isNotNull()
            .hasDeletedNodes(count * 3)
            .hasDeletedRelationships(count);
    }

    @ParameterizedTest @ValueSource(ints = {0, 5, 9, 32}) void ENABLED_should_delete_all_nodes_with_relationships(final int count)
    {
        applyFixture(count, this::addNodesWithRelationship);
        assertThat(Clean.ENABLED.execute(session()))
            .isNotNull()
            .hasDeletedNodes(count * 2)
            .hasDeletedRelationships(count);
    }

    private void addNodesWithRelationship(final StringBuilder content, final int i)
    {
        content
            .append("(a")
            .append(i)
            .append(":TestNode {value:")
            .append(i)
            .append("})-[:TEST]->(b")
            .append(i)
            .append(":TestNode {value:")
            .append(i)
            .append("})");
    }

    @ParameterizedTest @ValueSource(ints = {0, 11, 4, 22}) void ENABLED_should_delete_all_orphan_nodes(final int count)
    {
        applyFixture(count, this::addOrphanNode);
        assertThat(Clean.ENABLED.execute(session()))
            .isNotNull()
            .hasDeletedNodes(count)
            .hasDeletedRelationships(0);
    }

    @SafeVarargs private void applyFixture(
        final int amount,
        final BiConsumer<StringBuilder, Integer> contentBuilder,
        final BiConsumer<StringBuilder, Integer>... extraContentBuilderSteps)
    {
        BiConsumer<StringBuilder, Integer> builder = contentBuilder;
        if (null != extraContentBuilderSteps && 0 < extraContentBuilderSteps.length) {
            for (final var extra : extraContentBuilderSteps) {
                builder = builder.andThen(extra);
            }
        }
        fixture(create(builder, amount), true);
    }

    private String create(final BiConsumer<StringBuilder, Integer> contentBuilder, final int amount)
    {
        if (0 == amount) {
            return "";
        }
        final StringBuilder content = new StringBuilder();

        {
            int i = 0;
            content.append("CREATE ");
            contentBuilder.accept(content, i++);
            for (; i < amount; i++) {
                content.append(", ");
                contentBuilder.accept(content, i);
            }
        }

        return content.toString();
    }

    private void addOrphanNode(final StringBuilder fixture, final int i)
    {
        fixture.append("(n").append(i).append(":TestNode {value:").append(i).append("})");
    }

    @Override protected boolean shouldDiscardTransaction() {return true;}
}
