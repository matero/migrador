/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.connectors.BoltConnector;
import org.neo4j.configuration.connectors.HttpConnector;
import org.neo4j.configuration.connectors.HttpsConnector;
import org.neo4j.driver.AuthTokens;
import org.neo4j.driver.Driver;
import org.neo4j.harness.Neo4j;
import org.neo4j.harness.Neo4jBuilders;

import java.util.Base64;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class Neo4JDriverConfigurator_tests
{
    private final TestMigradorConfig migradorConfig = new TestMigradorConfig();
    private final Neo4JDriverConfigurator configurator = new Neo4JDriverConfigurator(this.migradorConfig);

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"http://localhost:7474", "peperino"})
    void getNeo4jUri_should_be_the_neo4jUri_configured_in_the_MigradorConfig(final String configuredUri)
    {
        this.migradorConfig.withNeo4jUri(configuredUri);
        assertThat(this.configurator.getNeo4jUri()).isEqualTo(configuredUri);
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_no_auth_token_type_is_configured_then_none_is_used(final String undefinedTokenType)
    {
        this.migradorConfig.withAuthTokenType(undefinedTokenType);
        assertThat(this.configurator.getAuthToken()).isEqualTo(AuthTokens.none());
    }

    @Test
    void when_none_auth_token_type_is_configured_then_none_is_used()
    {
        this.migradorConfig.withAuthTokenType("none");
        assertThat(this.configurator.getAuthToken()).isEqualTo(AuthTokens.none());
    }

    @Test
    void when_basic_auth_token_type_is_configured_then_a_basic_token_is_created()
    {
        this.migradorConfig
            .withAuthTokenType("basic")
            .withAuthPrincipal("username")
            .withAuthCredentials("password")
            .withoutAuthRealm();

        assertThat(this.configurator.getAuthToken())
            .isEqualTo(AuthTokens.basic("username", "password"));

        // AND
        this.migradorConfig.withAuthRealm("realm");

        assertThat(this.configurator.getAuthToken())
            .isEqualTo(AuthTokens.basic("username", "password", "realm"));
    }

    @Test
    void when_kerberos_auth_token_type_is_configured_then_a_kerberos_token_is_created()
    {
        final var kerberosTicket = Base64.getEncoder().encodeToString("kerberos-ticket".getBytes());
        this.migradorConfig
            .withAuthTokenType("kerberos")
            .withAuthCredentials(kerberosTicket);

        assertThat(this.configurator.getAuthToken())
            .isEqualTo(AuthTokens.kerberos(kerberosTicket));
    }

    @Test
    void when_custom_auth_token_type_is_configured_then_a_custom_token_is_created()
    {
        final Map<String, Object> parameters = Map.of("migra", "dor");

        this.migradorConfig
            .withAuthTokenType("custom")
            .withAuthPrincipal("username")
            .withAuthCredentials("password")
            .withAuthRealm("realm")
            .withAuthScheme("scheme")
            .withAuthParameters(parameters);

        assertThat(this.configurator.getAuthToken())
            .isEqualTo(AuthTokens.custom("username", "password", "realm", "scheme", parameters));
    }

    public TestMigradorConfig migradorConfig()
    {
        return this.migradorConfig;
    }

    public Neo4JDriverConfigurator configurator()
    {
        return this.configurator;
    }

    static class Having_auth_correctly_configured
    {
        private final Neo4j neo4jServer = Neo4jBuilders.newInProcessBuilder()
                                                       .withConfig(BoltConnector.enabled, true)
                                                       .withConfig(BoltConnector.encryption_level, BoltConnector.EncryptionLevel.DISABLED)
                                                       .withConfig(HttpConnector.enabled, false)
                                                       .withConfig(HttpsConnector.enabled, false)
                                                       .withConfig(GraphDatabaseSettings.auth_enabled, false)
                                                       .build();

        private final TestMigradorConfig migradorConfig = new TestMigradorConfig();
        private final Neo4JDriverConfigurator configurator = new Neo4JDriverConfigurator(this.migradorConfig);

        @AfterAll
        public void afterTests()
        {
            if (null != this.neo4jServer) {
                this.neo4jServer.close();
            }
        }

        @Test
        void getDriver_should_get_an_accessor_for_a_Neo4j_graph_database()
        {
            this.migradorConfig.withNeo4jUri(this.neo4jServer.boltURI().toString());
            try (final Driver driver = this.configurator.getDriver()) {
                assertThat(driver).isNotNull();
                final AtomicBoolean placesDefined = new AtomicBoolean();
                try (final var session = driver.session()) {
                    placesDefined.set(session.readTransaction(tx -> tx.run("MATCH (n :Place) RETURN count(*) > 0").single().get(0).asBoolean()));
                }
                assertThat(placesDefined.get()).isFalse(); // queries can be made
            }
        }
    }

    @Nested
    @DB
    class Having_auth_incorrectly_configured
    {
        private final Neo4j neo4jServer = Neo4jBuilders.newInProcessBuilder()
                                                       .withConfig(BoltConnector.enabled, true)
                                                       .withConfig(BoltConnector.encryption_level, BoltConnector.EncryptionLevel.DISABLED)
                                                       .withConfig(HttpConnector.enabled, false)
                                                       .withConfig(HttpsConnector.enabled, false)
                                                       .withConfig(GraphDatabaseSettings.auth_enabled, true)
                                                       .build();

        @AfterAll
        public void afterTests()
        {
            if (null != this.neo4jServer) {
                this.neo4jServer.close();
            }
        }

        @Test
        void getDriver_should_throw_an_AuthenticationException_when_credentials_are_wrong()
        {
            migradorConfig().withNeo4jUri(this.neo4jServer.boltURI().toString())
                            .withAuthTokenType("none");

            assertThatThrownBy(() -> configurator().getDriver())
                .isInstanceOf(org.neo4j.driver.exceptions.AuthenticationException.class)
                .hasMessage("Unsupported authentication token, scheme 'none' is only allowed when auth is disabled.")
                .hasNoCause();
        }
    }
}
