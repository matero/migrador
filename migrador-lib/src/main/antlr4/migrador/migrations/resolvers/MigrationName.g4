grammar MigrationName;

migrationName
    : versioned
    | repeatable
    ;

versioned: version description;

repeatable: 'R' description;

version: 'V' NUMBER (('_'|'.') NUMBER)*;

description: '__' (NUMBER|ALPHA)+ ('_'+ (NUMBER|ALPHA)+)+;

NUMBER: [0-9]+;
ALPHA: ([a-zA-Z]);

