/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador;

import migrador.helpers.Classes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

enum MigradorVersion
{
    READER;

    private final String migradorVersionResourceName = "migrador/version.txt";
    private String version;

    String getVersion(final Classes classes)
    {
        if (null == this.version) {
            this.version = readVersion(classes, this.migradorVersionResourceName);
        }
        return this.version;
    }

    String readVersion(final Classes classes, final String versionResourceName)
    {
        final InputStream stream = classes.getResourceStreamFor(versionResourceName);
        if (null == stream) {
            throw new NotDefined(versionResourceName);
        }

        final Logger logger = LoggerFactory.getLogger(MigradorVersion.class);
        if (logger.isDebugEnabled()) {
            final URL url = classes.getResource(versionResourceName);
            logger.debug("reading migrador version from: " + url);
        }

        return readVersionContentAt(stream);
    }

    String readVersionContentAt(final InputStream stream)
    {
        final StringBuilder content = new StringBuilder();
        try (final BufferedReader br = new BufferedReader(new InputStreamReader(stream))) {
            String line;
            while (null != (line = br.readLine())) {
                content.append(line).append('\n');
            }
        } catch (final IOException | SecurityException e) {
            throw new NotAccessible(e);
        }
        return content.toString().replaceAll("\\s", "");
    }

    static final class NotAccessible extends MigradorException
    {
        NotAccessible(final Throwable cause)
        {
            super("Unable to read Migrador version", cause);
        }
    }

    static final class NotDefined extends MigradorException
    {
        NotDefined(final String migradorVersionFilename)
        {
            super(migradorVersionFilename + " isn't defined!");
        }
    }
}
