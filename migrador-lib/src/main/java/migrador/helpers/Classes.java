/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.helpers;

import migrador.resources.Location;
import migrador.resources.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;

public final class Classes
{
    private static final Logger LOG = LoggerFactory.getLogger(Classes.class);

    private static final class Default
    {
        private static final Classes INSTANCE = new Classes(Thread.currentThread().getContextClassLoader());
    }

    private final ClassLoader classLoader;

    Classes(final ClassLoader classLoader) {this.classLoader = classLoader;}


    public static Classes get() {return Default.INSTANCE;}

    public static Classes of(final ClassLoader classLoader)
    {
        Objects.requireNonNull(classLoader, "classLoader is required");
        if (classLoader.equals(Thread.currentThread().getContextClassLoader())) {
            return Default.INSTANCE;
        }
        return new Classes(classLoader);
    }

    /**
     * Creates a new instance of this class.
     *
     * @param className The fully qualified name of the class to instantiate.
     * @param <T>       The type of the new instance.
     * @return The new instance.
     */
    // Must be synchronized for the Maven Parallel Junit runner to work
    @SuppressWarnings("unchecked")
    public synchronized <T> T instantiate(final String className)
    {
        try {
            return (T) Class.forName(className, true, this.classLoader).getDeclaredConstructor().newInstance();
        } catch (final Exception e) {
            throw new IllegalStateException("Unable to instantiate class " + className, e);
        }
    }

    /**
     * Determine whether the {@link Class} identified by the supplied name is present
     * and can be loaded. Will return {@code false} if either the class or
     * one of its dependencies is not present or cannot be loaded.
     *
     * @param className The name of the class to check.
     * @return whether the specified class is present
     */
    public boolean knowsClassNamed(final String className)
    {
        try {
            this.classLoader.loadClass(className);
            return true;
        } catch (final Throwable ex) {
            // Class or one of its dependencies is not present...
            return false;
        }
    }

    /**
     * Loads the class with this name using the class loader.
     *
     * @param implementedInterface The interface the class is expected to implement.
     * @return the newly loaded class or {@code null} if it could not be loaded.
     */
    public <I> Class<? extends I> load(final Class<I> implementedInterface, final Resource resource)
    {
        final String className = toClassName(resource);
        final Class<?> clazz;

        try {
            clazz = this.classLoader.loadClass(className);
        } catch (final ClassNotFoundException e) {
            if (LOG.isWarnEnabled()) {
                LOG.warn("Skipping " + className + " as it could not be instantiated", e);
            }
            return null;
        }

        if (!implementedInterface.isAssignableFrom(clazz)) {
            LOG.debug("Skipping class {}, as is not compatible with {}", className, implementedInterface);
            return null;
        }

        if (Modifier.isAbstract(clazz.getModifiers()) || clazz.isEnum() || clazz.isAnonymousClass()) {
            LOG.debug("Skipping non-instantiable class: {}", className);
            return null;
        }

        LOG.debug("Found class: {}", className);
        return (Class<? extends I>) clazz;
    }

    public InputStream getResourceStreamFor(final String resourceName) {return this.classLoader.getResourceAsStream(resourceName);}

    public URL getResource(final String resourceName) {return this.classLoader.getResource(resourceName);}


    private String toClassName(final Resource resource) {return toClassName(resource.getAbsolutePath());}

    /**
     * Converts this resource name to a fully qualified class name.
     *
     * @param resourceName The resource name.
     * @return The class name.
     */
    private String toClassName(final String resourceName)
    {
        final String nameWithDots = resourceName.replace("/", ".");
        return nameWithDots.substring(0, (nameWithDots.length() - ".class".length()));
    }

    public boolean canResolveUrls() {return this.classLoader instanceof URLClassLoader;}

    public List<URL> resolveUrls()
    {
        if (canResolveUrls()) {
            final URLClassLoader urlClassLoader = (URLClassLoader) this.classLoader;
            return List.of(urlClassLoader.getURLs());
        }
        return List.of();
    }

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof Classes) {
            final Classes other = (Classes) o;
            return this.classLoader.equals(other.classLoader);
        }
        return false;
    }

    @Override public int hashCode() {return this.classLoader.hashCode();}

    @Override public String toString() {return "Classes(classLoader=" + this.classLoader + ')';}

    public Enumeration<URL> getResources(final Location location)
    {
        try {
            return this.classLoader.getResources(location.path());
        } catch (final IOException e) {
            throw new Location.NotResolved(location, this, e);
        }
    }

    public Enumeration<URL> getResources(final String path)
        throws IOException
    {
        return this.classLoader.getResources(path);
    }

    /**
     * Retrieves the physical location on disk of this class.
     *
     * @param aClass The class to get the location for.
     * @return The absolute path of the .class file.
     */
    public String getLocationOnDisk(final Class<?> aClass)
    {
        final ProtectionDomain protectionDomain = aClass.getProtectionDomain();
        final CodeSource codeSource = protectionDomain.getCodeSource();
        //Custom classloader with for example classes defined using URLClassLoader#defineClass(String name, byte[] b, int off, int len)
        if (null == codeSource) {
            return null;
        }
        return Urls.decode(codeSource.getLocation().getPath());
    }
}
