/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.actions;

public final class CleanResult
{
    private final int deletedNodes;
    private final int deletedRelationships;
    private final int deletedConstraints;
    private final int deletedIndexes;

    CleanResult(final int deletedNodes, final int deletedRelationships) {this(deletedNodes, deletedRelationships, 0, 0);}

    CleanResult(final int deletedNodes, final int deletedRelationships, final int deletedConstraints, final int deletedIndexes)
    {
        this.deletedNodes = deletedNodes;
        this.deletedRelationships = deletedRelationships;
        this.deletedConstraints = deletedConstraints;
        this.deletedIndexes = deletedIndexes;
    }

    public int getDeletedNodes()
    {
        return this.deletedNodes;
    }

    public int getDeletedRelationships()
    {
        return this.deletedRelationships;
    }

    public int getDeletedConstraints() {return this.deletedConstraints;}

    public int getDeletedIndexes() {return this.deletedIndexes;}

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof CleanResult) {
            final CleanResult other = (CleanResult) o;
            return this.deletedNodes == other.deletedNodes
                   && this.deletedRelationships == other.deletedRelationships
                   && this.deletedConstraints == other.deletedConstraints
                   && this.deletedIndexes == other.deletedIndexes;
        }
        return false;
    }

    @Override public int hashCode()
    {
        int result = Integer.hashCode(this.deletedNodes);
        result = 31 * result + Integer.hashCode(this.deletedRelationships);
        result = 31 * result + Integer.hashCode(this.deletedConstraints);
        result = 31 * result + Integer.hashCode(this.deletedIndexes);
        return result;
    }

    @Override public String toString()
    {
        return "CleanResult(" + stats() + ')';
    }

    public String stats()
    {
        return "deletedNodes=" + this.deletedNodes +
               ", deletedRelationships=" + this.deletedRelationships +
               ", deletedConstraints=" + this.deletedConstraints +
               ", deletedIndexes=" + this.deletedIndexes;
    }


    public static CleanResultBuilder withDeletedNodes(final int nodes)
    {
        return new CleanResultBuilder().deletedNodes(nodes);
    }

    public static final class CleanResultBuilder
    {
        private int deletedNodes;
        private int deletedRelationships;
        private int deletedConstraints;
        private int deletedIndexes;

        CleanResultBuilder() {/*nothing to do*/}

        public CleanResultBuilder deletedNodes(final int value)
        {
            this.deletedNodes = value;
            return this;
        }

        public CleanResultBuilder addDeletedNodes(final int amount)
        {
            this.deletedNodes += amount;
            return this;
        }

        public CleanResultBuilder deletedRelationships(final int value)
        {
            this.deletedRelationships = value;
            return this;
        }

        public CleanResultBuilder addDeletedRelationships(final int amount)
        {
            this.deletedRelationships += amount;
            return this;
        }

        public CleanResultBuilder deletedConstraints(final int value)
        {
            this.deletedConstraints = value;
            return this;
        }

        public CleanResultBuilder addDeletedConstraints(final int amount)
        {
            this.deletedConstraints += amount;
            return this;
        }

        public CleanResultBuilder deletedIndexes(final int value)
        {
            this.deletedIndexes = value;
            return this;
        }

        public CleanResultBuilder addDeletedIndexes(final int amount)
        {
            this.deletedIndexes += amount;
            return this;
        }

        public CleanResult build()
        {
            return new CleanResult(this.deletedNodes, this.deletedRelationships, this.deletedConstraints, this.deletedIndexes);
        }
    }
}
