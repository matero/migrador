/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.actions;

import migrador.MigradorException;
import org.neo4j.driver.Session;
import org.neo4j.driver.exceptions.Neo4jException;

import java.util.Objects;

@FunctionalInterface
public interface Action<R>
{
    R execute(final Session session);

    @SuppressWarnings("unchecked")
    static <R> Action<R> noop() { return (Action<R>) NoAction.INSTANCE; }

    final class UnableToExecute extends MigradorException
    {
        public UnableToExecute(final CharSequence message) {super(message);}
    }

    final class ExecutionFailed extends MigradorException
    {
        public ExecutionFailed(final CharSequence message, final Throwable cause) {super(message, cause);}
    }
}

abstract class BasicAction<R> implements Action<R>
{
    @Override public final R execute(final Session session)
    {
        Objects.requireNonNull(session, "session");
        try {
            return executeWith(session);
        } catch (final Neo4jException e) {
            throw new ExecutionFailed("Execution of action " + this + ", on transaction " + session + " failed", e);
        }
    }

    protected abstract R executeWith(Session session);
}

enum NoAction implements Action {
    INSTANCE;

    @Override public Object execute(final Session session) {return null;}
}
