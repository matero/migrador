/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.actions;

import migrador.migrations.AppliedMigration;
import migrador.migrations.Label;
import migrador.migrations.Type;
import org.neo4j.driver.Record;
import org.neo4j.driver.Session;
import org.neo4j.driver.Transaction;
import org.neo4j.driver.Value;

import java.util.List;
import java.util.stream.Collectors;

public final class GetAppliedMigrations extends BasicAction<List<AppliedMigration>>
{
    private static final class Unique
    {
        private static final GetAppliedMigrations INSTANCE = new GetAppliedMigrations();
    }

    GetAppliedMigrations() {/*nothing to do*/}

    /**
     * @return The list of all migrations applied on the schema in the order they were applied (oldest first).
     * An empty list if no migration has been applied so far.
     */
    @Override public List<AppliedMigration> executeWith(final Session session)
    {
        return session.readTransaction(this::getAppliedMigrations);
    }

    private List<AppliedMigration> getAppliedMigrations(final Transaction tx)
    {
        if (!CheckMigrador.IS_INSTALLED.execute(tx)) {
            return List.of();
        }
        return tx.run(
            "MATCH (n " + Label.APPLIED_MIGRATION + ") " +
            "RETURN" +
            " n.installedRank AS installedRank," +
            " n.version AS version," +
            " n.description AS description," +
            " n.script AS script," +
            " n.checksum AS checksum," +
            " n.installedOn AS installedOn," +
            " n.installedBy AS installedBy," +
            " n.executionTime AS executionTime," +
            " n.success AS success," +
            " labels(n) AS type " +
            "ORDER BY n.installedOn, n.installRank")
                 .stream()
                 .map(this::recordToAppliedMigration)
                 .collect(Collectors.toUnmodifiableList());
    }

    AppliedMigration recordToAppliedMigration(final Record record)
    {
        final Value checksum = record.get("checksum");
        return AppliedMigration
            .ranked(record.get("installedRank").asInt())
            .version(record.get("version").asString())
            .description(record.get("description").asString())
            .type(getTypeAt(record))
            .script(record.get("script").asString())
            .checksum(checksum.isNull() ? null : checksum.asInt())
            .installedOn(record.get("installedOn").asLocalDateTime())
            .installedBy(record.get("installedBy").asString())
            .executionTime(record.get("executionTime").asLong())
            .success(record.get("success").asBoolean())
            .create();
    }

    private static Type getTypeAt(final Record record)
    {
        final var labels = record.get("type").asList(Value::asString);
        if (labels.contains(Label.INSTALL_NAME)) {
            return Type.INSTALL;
        }
        if (labels.contains(Label.BASELINE_NAME)) {
            return Type.BASELINE;
        }
        if (labels.contains(Label.JAVA_NAME)) {
            return Type.JAVA;
        }
        if (labels.contains(Label.CYPHER_NAME)) {
            return Type.CYPHER;
        }
        throw new IllegalStateException("Migrador node with unexpected label '" + labels + "'.");
    }

    public static GetAppliedMigrations action() {return Unique.INSTANCE;}
}
