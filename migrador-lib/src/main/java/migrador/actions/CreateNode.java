/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.actions;

import migrador.migrations.Type;
import migrador.migrations.Version;
import org.neo4j.driver.Transaction;
import org.neo4j.driver.Value;
import org.neo4j.driver.Values;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;

final class CreateNode
{
    private static final String CHECKSUM = "checksum";
    private static final String DESCRIPTION = "description";
    private static final String EXECUTION_TIME = "executionTime";
    private static final String INSTALLED_BY = "installedBy";
    private static final String INSTALLED_ON = "installedOn";
    private static final String INSTALLED_RANK = "installedRank";
    private static final String SCRIPT = "script";
    private static final String SUCCESS = "success";
    private static final String VERSION = "version";

    private Integer checksum;
    private String description;
    private Long executionTime;
    private String installedBy;
    private LocalDateTime installedOn;
    private final int installedRank;
    private String labels;
    private String script;
    private boolean success;
    private String version;

    CreateNode(final int installedRank) {this.installedRank = installedRank;}


    CreateNode version(final Version value) {return version(value.toString());}

    CreateNode version(final String value)
    {
        this.version = value;
        return this;
    }

    CreateNode description(final String value)
    {
        if (null == value) {
            this.description = null;
        } else {
            var s = value.trim();
            if (200 < s.length()) {
                s = s.substring(0, 197) + "...";
            }
            this.description = s;
        }
        return this;
    }

    CreateNode script(final String value)
    {
        if (null == value) {
            this.script = null;
        } else {
            var s = value.trim();
            if (1000 < s.length()) {
                s = "..." + s.substring(3, 1000);
            }
            this.script = s;
        }
        return this;
    }

    CreateNode checksum(final Integer value)
    {
        this.checksum = value;
        return this;
    }

    CreateNode installedBy(final String value)
    {
        this.installedBy = value;
        return this;
    }

    CreateNode installedOn(final LocalDateTime value)
    {
        this.installedOn = value;
        return this;
    }

    CreateNode executionTime(final Duration value)
    {
        this.executionTime = null == value ? null : value.toMillis();
        return this;
    }

    CreateNode executionTime(final long value)
    {
        this.executionTime = value;
        return this;
    }

    CreateNode success(final boolean value)
    {
        this.success = value;
        return this;
    }

    CreateNode type(final Type type) {
        return labels(type.getLabels());
    }

    private CreateNode labels(final String value)
    {
        this.labels = value;
        return this;
    }

    Value asValue()
    {
        return Values.parameters(CHECKSUM, this.checksum,
                                 DESCRIPTION, this.description,
                                 EXECUTION_TIME, this.executionTime,
                                 INSTALLED_BY, this.installedBy,
                                 INSTALLED_ON, this.installedOn,
                                 INSTALLED_RANK, this.installedRank,
                                 SCRIPT, this.script,
                                 SUCCESS, this.success,
                                 VERSION, this.version);
    }

    protected Integer execute(final Transaction tx)
    {
        final var query = newCreateNodeStatement();
        final var parameters = asValue();

        final var start = Instant.now();
        final var result = tx.run(query, parameters);

        final var executionTime = Duration.between(start, Instant.now()).toMillis();
        final int id = result.single().get(0).asInt();

        tx.run("MATCH (n) WHERE ID(n) = $id SET n.executionTime = $executionTime;", Values.parameters("id", id, "executionTime", executionTime));

        return id;
    }

    private String newCreateNodeStatement()
    {
        final var create = new StringBuilder()
            .append("CREATE (n ")
            .append(this.labels)
            .append(") SET n.installedRank = $").append(INSTALLED_RANK)
            .append(", n.installedBy = $").append(INSTALLED_BY)
            .append(", n.installedOn = $").append(INSTALLED_ON)
            .append(", n.success = $").append(SUCCESS);
        if (null != this.version) {
            create.append(", n.version = $").append(VERSION);
        }
        if (null != this.description) {
            create.append(", n.description = $").append(DESCRIPTION);
        }
        if (null != this.script) {
            create.append(", n.script = $").append(SCRIPT);
        }
        if (null != this.checksum) {
            create.append(", n.checksum = $").append(CHECKSUM);
        }
        if (null != this.executionTime) {
            create.append(", n.executionTime = $").append(EXECUTION_TIME);
        }
        return create.append(" RETURN ID(n);").toString();
    }

    public static CreateNode withInstalledRank(final int value) {return new CreateNode(value);}
}
