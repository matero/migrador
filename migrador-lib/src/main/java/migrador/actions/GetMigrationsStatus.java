/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.actions;

import migrador.migrations.DatabaseMigrations;
import migrador.migrations.Version;
import migrador.migrations.resolvers.MigrationResolver;
import org.neo4j.driver.Session;

public class GetMigrationsStatus extends BasicAction<MigrationsStatus>
{
    private final DatabaseMigrations databaseMigrations;

    GetMigrationsStatus(final DatabaseMigrations databaseMigrations) {this.databaseMigrations = databaseMigrations;}

    @Override protected MigrationsStatus executeWith(final Session session)
    {
        return new MigrationsStatus(this.databaseMigrations.findAll(session));
    }

    public static GetMigrationsStatusBuilder of(final Version targetVersion)
    {
        return new GetMigrationsStatusBuilder(DatabaseMigrations.of(targetVersion));
    }

    public static final class GetMigrationsStatusBuilder
    {
        private final DatabaseMigrations.DatabaseMigrationsBuilder databaseMigrationsBuilder;

        GetMigrationsStatusBuilder(final DatabaseMigrations.DatabaseMigrationsBuilder databaseMigrationsBuilder)
        {
            this.databaseMigrationsBuilder = databaseMigrationsBuilder.allowFutureMigrations()
                                                                      .allowIgnoredMigrations()
                                                                      .allowMissingMigrations()
                                                                      .allowPendingMigrations();
        }

        public GetMigrationsStatusBuilder migrationResolver(final MigrationResolver value)
        {
            this.databaseMigrationsBuilder.migrationResolver(value);
            return this;
        }

        public GetMigrationsStatusBuilder allowOutOfOrderMigrations(final boolean value)
        {
            this.databaseMigrationsBuilder.allowOutOfOrderMigrations(value);
            return this;
        }

        public GetMigrationsStatus build() {return new GetMigrationsStatus(this.databaseMigrationsBuilder.build());}

        public MigrationsStatus execute(final Session session) {return build().execute(session);}
    }
}
