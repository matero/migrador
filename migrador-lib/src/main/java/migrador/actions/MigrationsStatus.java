/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.actions;

import migrador.migrations.Migration;
import migrador.migrations.Version;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public final class MigrationsStatus
{
    private final List<Migration> migrations;
    private Migration currentMigration;
    private List<Migration> pendingMigrations, appliedMigrations, resolvedMigrations, failedMigrations, outOfOrderMigrations, futureMigrations;

    MigrationsStatus(final List<Migration> migrations) {this.migrations = migrations;}

    public List<Migration> getMigrations() {return this.migrations;}

    public int count() {return this.migrations.size();}

    /**
     * Retrieves the information of the current applied migration, if any.
     *
     * @return The info. {@code null} if no migrations have been applied yet.
     */
    public Migration getCurrentMigration()
    {
        if (null == this.currentMigration) {
            for (final Migration migration : this.migrations) {
                if (migration.wasApplied() && migration.isVersioned()) {
                    if (null == this.currentMigration) {
                        this.currentMigration = migration;
                    } else if (0 < migration.getVersion()
                                            .compareTo(this.currentMigration.getVersion())) {
                        this.currentMigration = migration;
                    }
                }
            }

            if (null == this.currentMigration) {
                // If no versioned migration has been applied so far, fall back to the latest repeatable one
                for (int i = this.migrations.size() - 1; 0 <= i; i--) {
                    final Migration migration = this.migrations.get(i);
                    if (migration.wasApplied()) {
                        this.currentMigration = migration;
                        break;
                    }
                }
            }
        }
        return this.currentMigration;
    }

    public Version getCurrentVersion()
    {
        final Migration current = getCurrentMigration();
        if (null == current) {
            return Version.empty();
        }
        return current.getVersion();
    }

    /**
     * Retrieves the full set of infos about pending migrations, available locally, but not yet applied to the DB.
     *
     * @return The pending migrations. An empty list if none.
     */
    public List<Migration> getPendingMigrations()
    {
        if (null == this.pendingMigrations) {
            this.pendingMigrations = filter(this.migrations, Migration::inPendingState);
        }
        return this.pendingMigrations;
    }

    /**
     * Retrieves the full set of infos about the migrations applied to the DB.
     *
     * @return The applied migrations. An empty list if none.
     */
    public List<Migration> getAppliedMigrations()
    {
        if (null == this.appliedMigrations) {
            this.appliedMigrations = filter(this.migrations, Migration::wasApplied);
        }
        return this.appliedMigrations;
    }


    /**
     * Retrieves the full set of infos about the migrations resolved on the classpath.
     *
     * @return The resolved migrations. An empty list if none.
     */
    public List<Migration> getResolvedMigrations()
    {
        if (null == this.resolvedMigrations) {
            this.resolvedMigrations = filter(this.migrations, Migration::isResolved);
        }
        return this.resolvedMigrations;
    }

    /**
     * Retrieves the full set of infos about the migrations that failed.
     *
     * @return The failed migrations. An empty list if none.
     */
    public List<Migration> getFailedMigrations()
    {
        if (null == this.failedMigrations) {
            this.failedMigrations = filter(this.migrations, Migration::isFailed);
        }
        return this.failedMigrations;
    }

    /**
     * Retrieves the full set of infos about out of order migrations applied to the DB.
     *
     * @return The out of order migrations. An empty list if none.
     */
    public List<Migration> getOutOfOrderMigrations()
    {
        if (null == this.outOfOrderMigrations) {
            this.outOfOrderMigrations = filter(this.migrations, Migration::isOutOfOrder);
        }
        return this.outOfOrderMigrations;
    }

    /**
     * Retrieves the full set of infos about future migrations applied to the DB.
     *
     * @return The future migrations. An empty list if none.
     */
    public List<Migration> getFutureMigrations() {return filter(this.migrations, Migration::isInFuture);}

    private List<Migration> filter(final List<Migration> migrations, final Predicate<Migration> predicate)
    {
        final var result = new ArrayList<Migration>(migrations.size());
        for (final Migration migration : migrations) {
            if (predicate.test(migration)) {
                result.add(migration);
            }
        }
        result.trimToSize();
        return List.copyOf(result);
    }

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof MigrationsStatus) {
            final MigrationsStatus other = (MigrationsStatus) o;
            return this.migrations.equals(other.migrations);
        }
        return false;
    }

    @Override public int hashCode()
    {
        return this.migrations.hashCode();
    }

    @Override public String toString()
    {
        return "GetMigrationsStatusResult(migrations=" + this.migrations + ')';
    }

}
