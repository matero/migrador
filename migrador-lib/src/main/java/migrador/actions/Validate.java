/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.actions;

import migrador.MigradorException;
import migrador.migrations.DatabaseMigrations;
import migrador.migrations.Version;
import migrador.migrations.resolvers.MigrationResolver;
import org.neo4j.driver.Session;
import org.neo4j.driver.TransactionWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public final class Validate extends BasicAction<ValidateResult>
{
    private static final Logger LOG = LoggerFactory.getLogger(Validate.class);

    private final DatabaseMigrations databaseMigrations;
    private final TransactionWork<Boolean> checkInstalled;

    Validate(final DatabaseMigrations databaseMigrations) {this(CheckMigrador.IS_INSTALLED, databaseMigrations);}

    Validate(final TransactionWork<Boolean> checkInstalled, final DatabaseMigrations databaseMigrations)
    {
        this.checkInstalled = checkInstalled;
        this.databaseMigrations = databaseMigrations;
    }

    @Override protected ValidateResult executeWith(final Session session)
    {
        if (!session.readTransaction(this.checkInstalled)) {
            return ValidateResult.notInstalled();
        }

        LOG.debug("Validating migrations...");

        final var migrations = this.databaseMigrations.findAll(session);
        if (migrations.isEmpty()) {
            throw new InvalidMigrations("No migrations found. Are your locations set up correctly?");
        }

        final String validationError = this.databaseMigrations.validate(session);

        if (null != validationError) {
            throw new InvalidMigrations(validationError);
        }
        return ValidateResult.of(migrations.size());
    }


    public static ValidateBuilder of(final Version targetVersion) {return new ValidateBuilder(DatabaseMigrations.of(targetVersion));}

    public static Validate with(final DatabaseMigrations.DatabaseMigrationsBuilder migrations)
    {
        Objects.requireNonNull(migrations, "migrations is required");
        return Validate.with(migrations.build());
    }

    public static Validate with(final DatabaseMigrations migrations)
    {
        Objects.requireNonNull(migrations, "migrations is required");
        return new Validate(migrations);
    }

    public static final class ValidateBuilder
    {
        private final DatabaseMigrations.DatabaseMigrationsBuilder databaseMigrationsBuilder;

        ValidateBuilder(final DatabaseMigrations.DatabaseMigrationsBuilder databaseMigrationsBuilder)
        {
            this.databaseMigrationsBuilder = databaseMigrationsBuilder;
        }

        public ValidateBuilder migrationResolver(final MigrationResolver value)
        {
            this.databaseMigrationsBuilder.migrationResolver(value);
            return this;
        }

        public ValidateBuilder allowOutOfOrderMigrations(final boolean value)
        {
            this.databaseMigrationsBuilder.allowOutOfOrderMigrations(value);
            return this;
        }

        public ValidateBuilder allowPendingMigrations(final boolean value)
        {
            this.databaseMigrationsBuilder.allowPendingMigrations(value);
            return this;
        }

        public ValidateBuilder allowMissingMigrations(final boolean value)
        {
            this.databaseMigrationsBuilder.allowMissingMigrations(value);
            return this;
        }

        public ValidateBuilder allowIgnoredMigrations(final boolean value)
        {
            this.databaseMigrationsBuilder.allowIgnoredMigrations(value);
            return this;
        }

        public ValidateBuilder allowFutureMigrations(final boolean value)
        {
            this.databaseMigrationsBuilder.allowFutureMigrations(value);
            return this;
        }

        public Validate build() {return new Validate(this.databaseMigrationsBuilder.build());}

        public ValidateResult execute(final Session session) {return build().execute(session);}
    }

    public static final class InvalidMigrations extends MigradorException {
        InvalidMigrations(final String message) {super(message);}
    }
}
