/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.actions;

import migrador.migrations.Label;
import migrador.migrations.Type;
import migrador.migrations.Version;
import org.neo4j.driver.Session;
import org.neo4j.driver.exceptions.Neo4jException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.Objects;

public final class Baseline implements Action<Integer>
{
    private static final Logger LOG = LoggerFactory.getLogger(Baseline.class);

    private final Version targetVersion;
    private final String username;

    Baseline(final Version targetVersion, final String username)
    {
        this.targetVersion = targetVersion;
        this.username = username;
    }

    @Override public Integer execute(final Session session)
    {
        Objects.requireNonNull(session, "session");
        try {
            addConstraints(session);
            return addInstallNode(session);
        } catch (final Neo4jException e) {
            throw new ExecutionFailed("Execution of action " + this + ", on transaction " + session + " failed", e);
        }
    }

    private Integer addInstallNode(final Session session)
    {
        final var createInstallNode = CreateNode.withInstalledRank(0)
                                                .type(Type.INSTALL)
                                                .version(this.targetVersion)
                                                .description("Migrador version tracking facilities Installment in graph db.")
                                                .script("V0__Migrador_tracking_installment")
                                                .installedBy(this.username)
                                                .installedOn(LocalDateTime.now())
                                                .success(true);
        LOG.debug("adding INSTALL node.");
        return session.writeTransaction(createInstallNode::execute);
    }

    private void addConstraints(final Session session)
    {
        LOG.debug("adding constraints to database...");
        session.writeTransaction(tx -> tx.run(addUniqueConstraint("installedRank")));
        LOG.trace(" * installedRank UNIQUE CONSTRAINT created");
        LOG.debug("...constraints added to database.");
    }

    String addUniqueConstraint(final String property)
    {
        return "CREATE CONSTRAINT " + property + "_isUnique ON (migration " + Label.MIGRADOR + ") ASSERT migration." + property + " IS UNIQUE";
    }

    public static Builder of(final Version targetVersion)
    {
        Objects.requireNonNull(targetVersion, "targetVersion is required");
        return new Builder(targetVersion);
    }

    public static final class Builder
    {
        private final Version targetVersion;
        private String username;

        public Builder(final Version targetVersion)
        {
            if (targetVersion.isAbstract()) {
                this.targetVersion = Version.empty();
            } else {
                this.targetVersion = targetVersion;
            }
        }

        public Builder executedBy(final String value)
        {
            this.username = value;
            return this;
        }

        public Baseline build()
        {
            Objects.requireNonNull(this.username, "username is required");
            return new Baseline(this.targetVersion, this.username);
        }

        public Integer execute(final Session session) {return build().execute(session);}
    }
}
