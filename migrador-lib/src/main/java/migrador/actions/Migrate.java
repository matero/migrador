/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.actions;


import migrador.MigradorException;
import migrador.migrations.DatabaseMigrations;
import migrador.migrations.Version;
import migrador.migrations.resolvers.MigrationResolver;
import org.neo4j.driver.Session;
import org.neo4j.driver.TransactionConfig;
import org.neo4j.driver.TransactionWork;

import java.util.Objects;

public class Migrate extends BasicAction<Integer>
{
    private static final int DEFAULT_BATCH_CAPACITY = 100;// 100 statements per commit

    private final Action<ValidateResult> validate;
    private final boolean shouldBaseline;
    private final String username;
    private final TransactionWork<Boolean> checkInstalled;
    private final DatabaseMigrations dbMigrations;

    Migrate(
        final Action<ValidateResult> validate,
        final boolean shouldBaseline,
        final String username,
        final DatabaseMigrations dbMigrations)
    {
        this(validate, shouldBaseline, username, dbMigrations, CheckMigrador.IS_INSTALLED);
    }

    Migrate(
        final Action<ValidateResult> validate,
        final boolean shouldBaseline,
        final String username,
        final DatabaseMigrations dbMigrations,
        final TransactionWork<Boolean> checkInstalled)
    {
        this.validate = validate;
        this.shouldBaseline = shouldBaseline;
        this.username = username;
        this.checkInstalled = checkInstalled;
        this.dbMigrations = dbMigrations;
    }

    @Override protected Integer executeWith(final Session session)
    {
        this.validate.execute(session);

        if (!session.readTransaction(this.checkInstalled)) {
            if (!this.shouldBaseline) {
                throw new BaselineRequired();
            }
            Baseline.of(this.dbMigrations.getTargetVersion()).executedBy(this.username).execute(session);
            return 1;
        }
        return migrateWith(session);
    }

    private Integer migrateWith(final Session session)
    {
        final var migrations = new MigrationsStatus(this.dbMigrations.findAll(session));
        final var executor = new ResolvedMigrationExecutor(this.username, session, DEFAULT_BATCH_CAPACITY, TransactionConfig.empty());
        for (final var pending : migrations.getPendingMigrations()) {
            pending.migrateWith(executor);
        }
        return migrations.getFutureMigrations().size();
    }

    public static MigrateBuilder of(final Version targetVersion)
    {
        return new MigrateBuilder(DatabaseMigrations.of(targetVersion));
    }

    public static final class MigrateBuilder
    {
        private final DatabaseMigrations.DatabaseMigrationsBuilder databaseMigrationsBuilder;
        private boolean validate;
        private boolean baseline;
        private String username;

        MigrateBuilder(final DatabaseMigrations.DatabaseMigrationsBuilder databaseMigrationsBuilder)
        {
            this.databaseMigrationsBuilder = databaseMigrationsBuilder;
        }

        public MigrateBuilder validate()
        {
            return validate(true);
        }

        public MigrateBuilder dontValidate()
        {
            return validate(false);
        }

        public MigrateBuilder validate(final boolean value)
        {
            this.validate = value;
            return this;
        }

        public MigrateBuilder baseline()
        {
            return baseline(true);
        }

        public MigrateBuilder dontBaseline()
        {
            return baseline(false);
        }

        public MigrateBuilder baseline(final boolean value)
        {
            this.baseline = value;
            return this;
        }

        public MigrateBuilder migrationResolver(final MigrationResolver.Builder valueBuilder)
        {
            Objects.requireNonNull(valueBuilder, "valueBuilder is required");
            this.databaseMigrationsBuilder.migrationResolver(valueBuilder);
            return this;
        }

        public MigrateBuilder migrationResolver(final MigrationResolver value)
        {
            this.databaseMigrationsBuilder.migrationResolver(value);
            return this;
        }

        public MigrateBuilder allowOutOfOrderMigrations(final boolean value)
        {
            this.databaseMigrationsBuilder.allowOutOfOrderMigrations(value);
            return this;
        }

        public MigrateBuilder allowPendingMigrations(final boolean value)
        {
            this.databaseMigrationsBuilder.allowPendingMigrations(value);
            return this;
        }

        public MigrateBuilder allowMissingMigrations(final boolean value)
        {
            this.databaseMigrationsBuilder.allowMissingMigrations(value);
            return this;
        }

        public MigrateBuilder allowIgnoredMigrations(final boolean value)
        {
            this.databaseMigrationsBuilder.allowIgnoredMigrations(value);
            return this;
        }

        public MigrateBuilder allowFutureMigrations(final boolean value)
        {
            this.databaseMigrationsBuilder.allowFutureMigrations(value);
            return this;
        }

        public Migrate build()
        {
            final var databaseMigrations = this.databaseMigrationsBuilder.build();
            if (this.validate) {
                // Always ignore pending migrations when validating before migrating
                final var validate = Validate.with(this.databaseMigrationsBuilder.ignorePendingMigrations());
                return new Migrate(validate, this.baseline, this.username, databaseMigrations);
            } else {
                return new Migrate(Action.noop(), this.baseline, this.username, databaseMigrations);
            }
        }

        public Integer execute(final Session session) {return build().execute(session);}

        public MigrateBuilder executedBy(final String value)
        {
            this.username = value;
            return this;
        }
    }

    public static final class BaselineRequired extends MigradorException
    {
        BaselineRequired()
        {
            super("Database isn't empty  but no Migrations were found. Use baseline() or set baselineOnMigrate to true to initialize Migrations.");
        }
    }
}
