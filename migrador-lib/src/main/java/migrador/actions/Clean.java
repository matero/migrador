/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.actions;

import org.neo4j.driver.Session;
import org.neo4j.driver.exceptions.Neo4jException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public enum Clean implements Action<CleanResult>
{
    ENABLED {
        @Override public CleanResult execute(final Session session)
        {
            Objects.requireNonNull(session, "session");
            try {
                LOG.debug("start removing all the elements in graph db");
                final var partialResult = cleanData(session);
                cleanScheme(session, partialResult);

                final var result = partialResult.build();
                LOG.info("all db elements removed ({})", result.stats());

                return result;
            } catch (final Neo4jException e) {
                throw new ExecutionFailed("Execution of action " + this + ", on transaction " + session + " failed", e);
            }
        }

        private CleanResult.CleanResultBuilder cleanData(final Session session)
        {
            LOG.trace("deleting nodes with relationships");
            return session.writeTransaction(tx -> {
                final var deletion = tx.run("MATCH (n)-[r]->() DELETE n, r RETURN count(n) AS nodes, count(r) AS rels").single();
                final var result = CleanResult.withDeletedNodes(deletion.get("nodes").asInt()).deletedRelationships(deletion.get("rels").asInt());

                LOG.trace("deleting orphan nodes");
                final var orphanNodesDeleted = tx.run("MATCH (n) DELETE n RETURN count(n) AS nodes").single().get("nodes").asInt();

                return result.addDeletedNodes(orphanNodesDeleted);
            });
        }

        private int cleanScheme(final Session session, final CleanResult.CleanResultBuilder result)
        {
            LOG.trace("deleting constraints");
            final List<String> constraints = session.readTransaction(tx -> tx.run("CALL db.constraints")
                                                                             .stream()
                                                                             .map(constraint -> constraint.get(0).asString())
                                                                             .collect(Collectors.toList()));
            if (!constraints.isEmpty()) {
                session.writeTransaction(tx -> {
                    for (final var constraintName : constraints) {
                        tx.run("DROP constraint " + constraintName);
                    }
                    return null;
                });
                result.deletedConstraints(constraints.size());
            }

            LOG.trace("deleting indexes");
            final List<String> indexes = session.readTransaction(tx -> tx.run("CALL db.indexes")
                                                                         .stream()
                                                                         .map(constraint -> constraint.get(0).asString())
                                                                         .collect(Collectors.toList()));
            if (!indexes.isEmpty()) {
                session.writeTransaction(tx -> {
                    for (final var indexName : indexes) {
                        session.run("DROP index " + indexName);
                    }

                    return null;
                });
                result.deletedIndexes(indexes.size());
            }
            return constraints.size();
        }
    },
    DISABLED {
        @Override public CleanResult execute(final Session s)
        {
            throw new UnableToExecute("Unable to execute clean as it has been disabled with the 'migrador.cleanDisabled' property.");
        }
    };

    private static final Logger LOG = LoggerFactory.getLogger(Clean.class);

    public static Clean enabled(final boolean value)
    {
        return value ? ENABLED : DISABLED;
    }
}
