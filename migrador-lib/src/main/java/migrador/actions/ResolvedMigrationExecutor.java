/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.actions;

import migrador.MigradorException;
import migrador.migrations.Label;
import migrador.migrations.resolvers.CypherResolvedMigration;
import migrador.migrations.resolvers.JavaResolvedMigration;
import migrador.migrations.resolvers.ResolvedMigration;
import migrador.resources.Location;
import org.neo4j.driver.Session;
import org.neo4j.driver.Transaction;
import org.neo4j.driver.TransactionConfig;
import org.neo4j.driver.exceptions.Neo4jException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class ResolvedMigrationExecutor
{
    private static final Logger LOG = LoggerFactory.getLogger(ResolvedMigrationExecutor.class);

    private final String username;
    private final Session session;
    private final int batchCapacity;
    private final TransactionConfig txConfig;

    public ResolvedMigrationExecutor(final String username, final Session session, final int batchCapacity, final TransactionConfig txConfig)
    {
        this.username = username;
        this.session = session;
        this.batchCapacity = batchCapacity;
        this.txConfig = txConfig;
    }

    public void executeCypherScript(final CypherResolvedMigration cypherResolvedMigration)
    {
        try {
            runScript(cypherResolvedMigration.getPhysicalLocation());
        } catch (final MigradorException | Neo4jException e) {
            migrationExecuted(cypherResolvedMigration, false);
            throw e;
        }
        migrationExecuted(cypherResolvedMigration, true);
    }

    public void executeJavaMigration(final JavaResolvedMigration javaResolvedMigration)
    {
        try {
            javaResolvedMigration.getJavaMigration().migrate(this.session);
        } catch (final MigradorException | Neo4jException e) {
            migrationExecuted(javaResolvedMigration, false);
            throw e;
        }
        migrationExecuted(javaResolvedMigration, true);
    }

    private void migrationExecuted(final ResolvedMigration migration, final boolean succeded)
    {
        final int installedRank = getRank(migration);
        final var track = CreateNode.withInstalledRank(installedRank)
                                    .version(migration.getVersion())
                                    .description(migration.getDescription())
                                    .script(migration.getScript())
                                    .checksum(migration.getChecksum())
                                    .installedOn(LocalDateTime.now())
                                    .installedBy(this.username)
                                    .success(succeded)
                                    .type(migration.getType());
        this.session.writeTransaction(track::execute);
    }

    private int getRank(final ResolvedMigration migration)
    {
        if (migration.hasInstallType()) {
            return 0;
        }
        return this.session.readTransaction(this::getNextInstalledRank);
    }

    /**
     * Calculates the installed rank for the new migration to be inserted.
     *
     * @return The installed rank.
     */
    private int getNextInstalledRank(final Transaction tx)
    {
        return tx.run("MATCH (n " + Label.APPLIED_MIGRATION + ") RETURN count(*)+1").single().get(0).asInt();
    }

    enum StatementType
    {
        UNKNOWN, SCHEMA, DATA;

        static StatementType of(final String stmt)
        {
            if (isSchemaOperation(stmt)) {
                return SCHEMA;
            } else {
                return DATA;
            }
        }

        private static boolean isSchemaOperation(final String stmt) {return stmt.matches("(?is).*(create|drop)\\s+(index|constraint).*");}
    }

    /**
     * runs each statement in the script, all semicolon separated.
     */
    public void runScript(final String scriptLocation)
    {
        for (final var statementsBatch : getStatementsBatchesFor(scriptLocation)) {
            this.session.writeTransaction(tx -> {
                for (final var stmt : statementsBatch) {
                    tx.run(stmt);
                }
                return null;
            }, this.txConfig);
        }
    }

    private List<List<String>> getStatementsBatchesFor(final String scriptLocation)
    {
        final var batches = new ArrayList<List<String>>();
        final var statements = new ArrayList<String>(this.batchCapacity);
        var currentType = StatementType.UNKNOWN;

        try (final var source = new FileReader(scriptLocation);
             final var scanner = makeScannerWith(source)) {
            while (scanner.hasNext()) {
                final String stmt = removeShellControlCommands(scanner.next());
                if (stmt.isEmpty()) {
                    continue;
                }
                final var stmtType = StatementType.of(stmt);
                if (stmtType != currentType) {
                    if (StatementType.UNKNOWN != currentType) {
                        batches.add(List.copyOf(statements));
                        statements.clear();
                    }
                    currentType = stmtType;
                }
                statements.add(stmt);

                if (this.batchCapacity == statements.size()) {
                    batches.add(List.copyOf(statements));
                    statements.clear();
                }
            }
            if (!statements.isEmpty()) {
                batches.add(statements);
            }
        } catch (final IOException e) {
            throw new Location.NotReadable(scriptLocation, e);
        }
        return List.copyOf(batches);
    }

    private Scanner makeScannerWith(final Reader source)
    {
        return new Scanner(source).useDelimiter(";\r?\n");
    }

    /*
     * Taken from APOC
     */
    private final static Pattern SHELL_CONTROL = Pattern.compile("^:?\\b(begin|commit|rollback)\\b", Pattern.CASE_INSENSITIVE);

    private String removeShellControlCommands(final String stmt)
    {
        final var matcher = SHELL_CONTROL.matcher(stmt.trim());
        if (matcher.find()) {
            // an empty file get transformed into ":begin\n:commit" and that statement is not matched by the pattern
            // because ":begin\n:commit".replaceAll("") => "\n:commit" with the recursion we avoid the problem
            return removeShellControlCommands(matcher.replaceAll(""));
        }
        return stmt.trim();
    }
}
