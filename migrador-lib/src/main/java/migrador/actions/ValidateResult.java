/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.actions;

public final class ValidateResult
{

    private static final class Unique
    {
        private static final ValidateResult NOT_INSTALLED = new ValidateResult("Migrador is not installed yet.", 0);
    }

    private final String message;
    private final int validatedMigrations;

    ValidateResult(final String message, final int validatedMigrations)
    {
        this.message = message;
        this.validatedMigrations = validatedMigrations;
    }

    public String getMessage() {return this.message;}

    public int getValidatedMigrations()
    {
        return this.validatedMigrations;
    }

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof ValidateResult) {
            final ValidateResult other = (ValidateResult) o;
            return this.validatedMigrations == other.validatedMigrations && this.message.equals(other.message);
        }
        return false;
    }

    @Override public int hashCode()
    {
        int result = this.message.hashCode();
        result = 31 * result + Integer.hashCode(this.validatedMigrations);
        return result;
    }

    @Override public String toString()
    {
        return "ValidateResult(validatedMigrations=" + this.validatedMigrations + ", message='" + this.message + "')";
    }

    static ValidateResult of(final int validatedMigrations)
    {
        if (1 == validatedMigrations) {
            return new ValidateResult("Successfully validated 1 migration.", 1);
        } else {
            return new ValidateResult("Successfully validated " + validatedMigrations + " migrations.", validatedMigrations);
        }
    }

    static ValidateResult notInstalled()
    {
        return Unique.NOT_INSTALLED;
    }
}
