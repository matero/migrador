package migrador;

import migrador.helpers.ToCharSet;
import migrador.helpers.ToCharacter;
import org.aeonbits.owner.Config;

import java.io.File;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.aeonbits.owner.Config.LoadPolicy;
import static org.aeonbits.owner.Config.LoadType.MERGE;
import static org.aeonbits.owner.Config.Sources;

@LoadPolicy(MERGE)
@Sources({
             "system:env",
             "system:properties",
             "${" + MigradorConfig.USER_CONFIG + '}',
             "file:${user.home}/.config/migrador/migrador.properties",
             "file:${user.home}/.migrador.properties",
             "file:/etc/migrador.properties",
             "classpath:migrador/migrador.properties"
         })
public interface MigradorConfig extends Config
{
    String USER_CONFIG = "userConfig";

    String username();

    /**
     * Gets the target version up to which Migrador should consider migrations.
     * Migrations with a higher version number will be ignored.
     * Special values:
     * <ul>
     * <li>{@code current}: designates the current version of the schema</li>
     * <li>{@code latest}: the latest version of the schema, as defined by the migration with the highest version</li>
     * </ul>
     * Defaults to {@code latest}.
     *
     * @return The target version up to which Migrador should consider migrations. Defaults to {@code latest}
     */
    @DefaultValue("latest")
    String target();

    @DefaultValue("false")
    boolean skipDefaultResolvers();

    @DefaultValue("filesystem:db/migrations")
    List<String> locations();

    @DefaultValue("UTF-8")
    @ConverterClass(ToCharSet.class)
    Charset encoding();

    @DefaultValue("false")
    boolean renderCypherScriptAsTemplates();

    @DefaultValue("{")
    @Key("template.delimiter.start")
    @ConverterClass(ToCharacter.class)
    Character delimiterStart();

    @DefaultValue("}")
    @Key("template.delimiter.end")
    @ConverterClass(ToCharacter.class)
    Character delimiterEnd();

    @Key("template.parameters")
    Map<String, Object> templateParameters();

    @DefaultValue("5")
    @Key("db.connect.retries")
    int connectRetries();

    @Key("db.initCypher")
    String initCypher();

    @DefaultValue("true")
    boolean cleanEnabled();

    @DefaultValue("neo4j://localhost:7687")
    @Key("db.uri")
    String neo4jUri();

    @DefaultValue("none")
    @Key("db.auth.type")
    String authTokenType();

    @Key("db.auth.principal")
    String authPrincipal();

    @Key("db.auth.credentials")
    String authCredentials();

    @Key("db.auth.realm")
    String authRealm();

    @Key("db.auth.scheme")
    String authScheme();

    @Key("db.auth.parameters")
    Map<String, Object> authParameters();

    @DefaultValue("true")
    boolean baselineOnMigrate();

    @DefaultValue("false")
    boolean validateOnMigrate();

    File workDir();

    String cmdLinePassword();

    @DefaultValue("2")
    int dbPoolSize();

    @DefaultValue("20")
    long dbConnectionAcquisitionTimeout();

    @DefaultValue("SECONDS")
    TimeUnit dbConnectionAcquisitionTimeoutUnit();

    @DefaultValue("false")
    boolean dbConnectionEncrypted();

    @DefaultValue("30")
    long dbConnectionLifetime();

    @DefaultValue("MINUTES")
    TimeUnit dbConnectionLifetimeUnit();

    @DefaultValue("15")
    long dbTransactionRetryTime();

    @DefaultValue("SECONDS")
    TimeUnit dbTransactionRetryTimeUnit();

    @DefaultValue("jul")
    String dbLogger();

    @DefaultValue("WARNING")
    String dbLoggingLevel();

    /**
     * Allows migrations to be run "out of order".
     * <p>If you already have versions 1 and 3 applied, and now a version 2 is found, it will be applied too instead of being ignored.</p>
     *
     * @return {@code true} if outOfOrder migrations should be applied, {@code false} if not. (default: {@code false})
     */
    @DefaultValue("false")
    boolean outOfOrder();

    /**
     * Ignore missing migrations when reading the schema history table. These are migrations that were performed by an
     * older deployment of the application that are no longer available in this version. For example: we have migrations
     * available on the classpath with versions 1.0 and 3.0. The schema history table indicates that a migration with version 2.0
     * (unknown to us) has also been applied. Instead of bombing out (fail fast) with an exception, a
     * warning is logged and Flyway continues normally. This is useful for situations where one must be able to deploy
     * a newer version of the application even though it doesn't contain migrations included with an older one anymore.
     * Note that if the most recently applied migration is removed, Flyway has no way to know it is missing and will
     * mark it as future instead.
     *
     * @return {@code true} to continue normally and log a warning, {@code false} to fail fast with an exception.
     * (default: {@code false})
     */
    @DefaultValue("false")
    boolean ignoreMissingMigrations();

    /**
     * Ignore ignored migrations when reading the schema history table. These are migrations that were added in between
     * already migrated migrations in this version. For example: we have migrations available on the classpath with
     * versions from 1.0 to 3.0. The schema history table indicates that version 1 was finished on 1.0.15, and the next
     * one was 2.0.0. But with the next release a new migration was added to version 1: 1.0.16. Such scenario is ignored
     * by migrate command, but by default is rejected by validate. When ignoreIgnoredMigrations is enabled, such case
     * will not be reported by validate command. This is useful for situations where one must be able to deliver
     * complete set of migrations in a delivery package for multiple versions of the product, and allows for further
     * development of older versions.
     *
     * @return {@code true} to continue normally, {@code false} to fail fast with an exception.
     * (default: {@code false})
     */
    @DefaultValue("false")
    boolean ignoreIgnoredMigrations();

    /**
     * Ignore pending migrations when reading the schema history table. These are migrations that are available
     * but have not yet been applied. This can be useful for verifying that in-development migration changes
     * don't contain any validation-breaking changes of migrations that have already been applied to a production
     * environment, e.g. as part of a CI/CD process, without failing because of the existence of new migration versions.
     *
     * @return {@code true} to continue normally, {@code false} to fail fast with an exception.
     * (default: {@code false})
     */
    @DefaultValue("false")
    boolean ignorePendingMigrations();

    /**
     * Ignore future migrations when reading the schema history table. These are migrations that were performed by a
     * newer deployment of the application that are not yet available in this version. For example: we have migrations
     * available on the classpath up to version 3.0. The schema history table indicates that a migration to version 4.0
     * (unknown to us) has already been applied. Instead of bombing out (fail fast) with an exception, a
     * warning is logged and Flyway continues normally. This is useful for situations where one must be able to redeploy
     * an older version of the application after the database has been migrated by a newer one.
     *
     * @return {@code true} to continue normally and log a warning, {@code false} to fail fast with an exception.
     * (default: {@code true})
     */
    @DefaultValue("true")
    boolean ignoreFutureMigrations();
}
