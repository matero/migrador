/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador;

import migrador.actions.Clean;
import migrador.helpers.Classes;
import migrador.migrations.JavaMigration;
import migrador.migrations.Version;
import migrador.migrations.resolvers.CypherRenderer;
import migrador.migrations.resolvers.MigrationResolver;
import migrador.resources.Location;
import migrador.resources.LocationScannerCache;
import migrador.resources.ResourceNameCache;
import migrador.resources.ResourceScanner;
import org.neo4j.driver.Driver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.List;

public class MigradorFactory implements Migrador.Factory
{
    private static final Logger LOG = LoggerFactory.getLogger(MigradorFactory.class);

    private final ClassLoader classLoader;
    private final MigradorConfig config;

    MigradorFactory(final MigradorConfig config, final ClassLoader classLoader)
    {
        this.classLoader = classLoader;
        this.config = config;
    }

    @Override public Migrador create()
    {

        final var targetVersion = Version.make(this.config.target());
        final List<Location> locations = getConfiguredLocations();
        final CypherRenderer renderer = CypherRenderer.from(this.config);
        final List<MigrationResolver> customMigrationResolvers = getCustomMigrationResolvers();
        final List<JavaMigration> fixedJavaMigrations = getFixedJavaMigrations();
        final Classes classes = Classes.of(this.classLoader);
        final Charset encoding = this.config.encoding();
        final ResourceNameCache resourceNameCache = new ResourceNameCache();
        final LocationScannerCache locationScannerCache = new LocationScannerCache();

        final var scanner = ResourceScanner.scan(locations)
                                           .classes(classes)
                                           .encoding(encoding)
                                           .resourceNameCache(resourceNameCache)
                                           .locationScannerCache(locationScannerCache)
                                           .build();

        final var neo4jDriver = getDriver();
        final var migrador = new Migrador(
                neo4jDriver,
                classes,
                renderer,
                scanner,
                this.config.skipDefaultResolvers(),
                Clean.enabled(this.config.cleanEnabled()),
                customMigrationResolvers,
                fixedJavaMigrations,
                targetVersion,
                this.config.outOfOrder(),
                this.config.ignorePendingMigrations(),
                this.config.ignoreMissingMigrations(),
                this.config.ignoreIgnoredMigrations(),
                this.config.ignoreFutureMigrations(),
                getUsername(),
                this.config.baselineOnMigrate());

        LOG.info("migrador successfully configured");
        return migrador;
    }

    private String getUsername()
    {
        final var username = this.config.username();
        if (null == username || username.isBlank()) {
            return System.getProperty("user.name");
        } else {
            return username;
        }
    }

    Driver getDriver()
    {
        LOG.trace("Connecting to neo4j...");
        final var driver = new Neo4JDriverConfigurator(this.config).getDriver();
        LOG.trace("Connection to neo4j established");
        return driver;
    }

    List<Location> getConfiguredLocations()
    {
        LOG.trace("loading configured locations...");
        final var locations = this.config.locations();
        final var workDir = getWorkDir();

        final var result = new java.util.ArrayList<Location>(locations.size());
        if (null == locations || locations.isEmpty()) {
            result.add(Location.of(Location.FILESYSTEM_PREFIX + workDir + "db/migrations").validate());
        } else {
            for (final var locationPath : locations) {
                if (Location.isFileSystem(locationPath)) {
                    result.add(locationOnFileSystem(workDir, locationPath));
                } else {
                    result.add(Location.of(locationPath).validate());
                }
            }
        }
        result.trimToSize();
        LOG.trace("configured locations loaded.");
        return List.copyOf(result);
    }

    private Location locationOnFileSystem(final String workDir, final String path)
    {
        final String newPath = path.substring(Location.FILESYSTEM_PREFIX.length());
        File file = new File(newPath);
        if (!file.isAbsolute()) {
            file = new File(workDir, newPath);
        }
        return Location.of(Location.FILESYSTEM_PREFIX + file.getAbsolutePath()).validate();
    }

    String getWorkDir()
    {
        final File workDir = null == this.config.workDir() ? currentExecutionDir() : this.config.workDir();
        if (!workDir.exists()) {
            throw new IllegalArgumentException("workDir='" + workDir + "' doesn't exists.");
        }
        if (!workDir.isDirectory()) {
            throw new IllegalArgumentException("workDir='" + workDir + "' is not a directory.");
        }
        if (!workDir.canRead()) {
            throw new IllegalArgumentException("workDir='" + workDir + "' is not readable.");
        }
        return toAbsolutePath(workDir);
    }

    static String toAbsolutePath(final File workDir)
    {
        final var absolutePath = workDir.getAbsolutePath();
        return absolutePath.endsWith("/") ? absolutePath : absolutePath + '/';
    }

    static File currentExecutionDir()
    {
        return Paths.get("").toAbsolutePath().toFile();
    }

    List<MigrationResolver> getCustomMigrationResolvers()
    {
        return List.of(); // FIXME: has to interpret the values at configuration
    }

    List<JavaMigration> getFixedJavaMigrations()
    {
        return List.of(); // FIXME: has to interpret the values at configuration
    }
}
