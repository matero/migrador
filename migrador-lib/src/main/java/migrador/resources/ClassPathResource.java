package migrador.resources;

import migrador.MigradorException;
import migrador.helpers.Classes;
import migrador.helpers.Urls;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

final class ClassPathResource implements Resource
{
    private final Classes classes;
    private final String fileNameWithAbsolutePath;
    private final String fileNameWithRelativePath;
    private final Charset encoding;

    ClassPathResource(
        final Classes classes,
        final String fileNameWithAbsolutePath,
        final String fileNameWithRelativePath,
        final Charset encoding
                     )
    {
        this.classes = classes;
        this.fileNameWithAbsolutePath = fileNameWithAbsolutePath;
        this.fileNameWithRelativePath = fileNameWithRelativePath;
        this.encoding = encoding;
    }

    @Override public String getRelativePath()
    {
        return this.fileNameWithRelativePath;
    }

    @Override public String getAbsolutePath()
    {
        return this.fileNameWithAbsolutePath;
    }

    @Override public String getAbsolutePathOnDisk()
    {
        if (!exists()) {
            throw new NotFound(this.fileNameWithAbsolutePath);
        }
        return new File(Urls.decode(getUrl())).getAbsolutePath();
    }

    @Override public Reader read()
    {
        final InputStream inputStream = this.classes.getResourceStreamFor(this.fileNameWithAbsolutePath);
        if (inputStream == null) {
            throw new UnableToLoad(this);
        }
        return new InputStreamReader(inputStream, this.encoding.newDecoder());
    }

    @Override public String getFilename()
    {
        return this.fileNameWithAbsolutePath.substring(this.fileNameWithAbsolutePath.lastIndexOf("/") + 1);
    }

    public boolean exists()
    {
        return getUrl() != null;
    }

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof ClassPathResource) {
            final ClassPathResource other = (ClassPathResource) o;
            return this.fileNameWithAbsolutePath.equals(other.fileNameWithAbsolutePath);
        }
        return false;
    }

    @Override public int hashCode()
    {
        return this.fileNameWithAbsolutePath.hashCode();
    }

    private URL getUrl()
    {
        return this.classes.getResource(this.fileNameWithAbsolutePath);
    }

    static final class UnableToLoad extends MigradorException
    {
        UnableToLoad(final ClassPathResource classPathResource)
        {
            super("Unable to load classpath resource: " + classPathResource.getAbsolutePath());
        }
    }
}
