/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Collection;

final class FileSystemScanner
{
    private static final Logger LOG = LoggerFactory.getLogger(FileSystemScanner.class);

    private final Charset encoding;

    FileSystemScanner(final Charset encoding)
    {
        this.encoding = encoding;
    }

    /**
     * Scans the FileSystem for resources under the specified location, starting with the specified prefix and ending with
     * the specified suffix.
     *
     * @param location The location in the filesystem to start searching. Subdirectories are also searched.
     * @return The resources that were found, without nulls or repeated elements.
     */
    Collection<Resource> scanAt(final Location location)
    {
        LOG.debug("Scanning for filesystem resources at '{}'", location.getRootPath());

        final var resources = new java.util.TreeSet<Resource>();

        for (final String resourceName : findResourceNamesFromFileSystem(location.path(), location.getRootPath())) {
            if (location.matchesPath(resourceName)) {
                resources.add(Resource.onFileSystemResourceAt(location).withPath(resourceName).encoding(this.encoding).create());
                LOG.debug("Found filesystem resource: {}", resourceName);
            }
        }

        return resources;
    }

    /**
     * Finds all the resource names contained in this file system folder.
     *
     * @param scanRootLocation The root location of the scan on disk.
     * @param folder           The folder to look for resources under on disk.
     * @return The resource names;
     */
    private Collection<String> findResourceNamesFromFileSystem(final String scanRootLocation, final File folder)
    {
        LOG.debug("Scanning for resources in path: {} ({})", folder.getPath(), scanRootLocation);

        final var resourceNames = new java.util.TreeSet<String>();

        final File[] files = folder.listFiles();
        if (null != files) {
            // Skip hidden directories to avoid issues with Kubernetes
            for (final File file : files) {
                if (file.canRead()) {
                    if (file.isDirectory()) {
                        if (file.isHidden()) {
                            LOG.debug("Skipping hidden directory: {}", file.getAbsolutePath());
                        } else {
                            resourceNames.addAll(findResourceNamesFromFileSystem(scanRootLocation, file));
                        }
                    } else {
                        resourceNames.add(file.getPath());
                    }
                }
            }
        }

        return resourceNames;
    }
}
