/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.resources;

import migrador.MigradorException;
import migrador.helpers.Classes;

import java.io.File;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Objects;

public interface Resource extends Comparable<Resource>
{
    /**
     * @return The absolute path and filename of the resource on the classpath or filesystem (path and filename).
     */
    String getAbsolutePath();

    /**
     * @return The absolute path and filename of this resource on disk, regardless of whether this resources
     * points at the classpath or filesystem.
     */
    String getAbsolutePathOnDisk();

    /**
     * @return The filename of this resource, without the path.
     */
    String getFilename();

    /**
     * @return The filename of this resource, as well as the path relative to the location where the resource was
     * loaded from.
     */
    String getRelativePath();

    default boolean hasRelativePath(final String resourceName)
    {
        return getRelativePath().equals(resourceName);
    }

    default boolean isClass()
    {
        return getAbsolutePath().endsWith(".class");
    }

    /**
     * Reads the contents of this resource.
     *
     * @return The reader with the contents of the resource.
     */
    Reader read();

    @Override default int compareTo(final Resource other)
    {
        return getRelativePath().compareTo(other.getRelativePath());
    }

    static Resource none()
    {
        return NoResource.Unique.INSTANCE;
    }

    static FileSystemResourceDefinition onFileSystemResourceAt(final Location location)
    {
        return new FileSystemResourceDefinition(location);
    }

    default boolean matchPrefixAndSuffix(final String prefix, final String suffix)
    {
        final String filename = getFilename();
        if (!prefix.isEmpty() && !filename.startsWith(prefix)) {
            return false;
        }
        if (filename.endsWith(suffix) && (filename.length() > (prefix.length() + suffix.length()))) {
            return true;
        }
        return false;
    }

    final class FileSystemResourceDefinition
    {
        private final Location location;
        private String fileNameWithPath;
        private Charset encoding = Charset.defaultCharset();

        FileSystemResourceDefinition(final Location location)
        {
            this.location = location;
        }

        public FileSystemResourceDefinition withPath(final String value)
        {
            this.fileNameWithPath = value;
            return this;
        }

        public FileSystemResourceDefinition encoding(final Charset value)
        {
            this.encoding = value;
            return this;
        }

        public Resource create()
        {
            Objects.requireNonNull(this.fileNameWithPath, "fileNameWithPath");
            Objects.requireNonNull(this.encoding, "encoding");

            final File file = new File(new File(this.fileNameWithPath).getPath());
            if (null == this.location) {
                return new FileSystemResource(file, file.getPath(), this.encoding);
            }
            return new FileSystemResource(file, this.location.getPathRelativeToThis(file.getPath()).replace("\\", "/"), this.encoding);
        }
    }

    static ClassPathResourceDefinition onClassPathUsing(final Classes classes)
    {
        return new ClassPathResourceDefinition(classes);
    }

    final class ClassPathResourceDefinition
    {
        private final Classes classes;
        private Location location;
        private String fileNameWithAbsolutePath;
        private Charset encoding = Charset.defaultCharset();

        ClassPathResourceDefinition(final Classes classes)
        {
            this.classes = classes;
        }

        public ClassPathResourceDefinition location(final Location value)
        {
            this.location = value;
            return this;
        }

        public ClassPathResourceDefinition withAbsolutePath(final String value)
        {
            this.fileNameWithAbsolutePath = value;
            return this;
        }

        public ClassPathResourceDefinition encoding(final Charset value)
        {
            this.encoding = value;
            return this;
        }

        public Resource create()
        {
            Objects.requireNonNull(this.fileNameWithAbsolutePath, "fileNameWithAbsolutePath");
            Objects.requireNonNull(this.encoding, "encoding");

            if (null == this.location) {
                return new ClassPathResource(this.classes, this.fileNameWithAbsolutePath, this.fileNameWithAbsolutePath, this.encoding);
            }

            return new ClassPathResource(
                this.classes,
                this.fileNameWithAbsolutePath,
                this.location.getPathRelativeToThis(this.fileNameWithAbsolutePath),
                this.encoding
            );
        }
    }

    static Resource forString(final CharSequence content)
    {
        Objects.requireNonNull(content, "content");
        return new StringResource(content.toString());
    }

    final class NotFound extends MigradorException
    {
        NotFound(final String filename)
        {
            super("Unable to find resource on disk: " + filename);
        }
    }

    final class NotReadable extends MigradorException
    {
        public NotReadable(final Resource resource, final Throwable cause)
        {
            super("Resource '" + resource + "' isn't readable.", cause);
        }
    }
}
