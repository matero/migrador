/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.resources;

import migrador.helpers.ByteOrderMark;

import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;

class ByteOrderMarkStrippingReader extends FilterReader
{
    private static final int EMPTY_STREAM = -1;

    /**
     * Creates a new BOM-stripping reader.
     *
     * @param in a Reader object providing the underlying stream.
     * @throws NullPointerException if <code>in</code> is <code>null</code>
     */
    ByteOrderMarkStrippingReader(final Reader in)
    {
        super(in);
    }

    @Override public int read()
        throws IOException
    {
        final int c = super.read();
        if (EMPTY_STREAM != c && ByteOrderMark.FILTER.isBom((char) c)) {
            return super.read(); // Skip BOM
        }
        return c;
    }
}
