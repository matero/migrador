/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.resources;

import migrador.MigradorException;
import migrador.helpers.Classes;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Location implements Comparable<Location>
{
    private static final String CLASSPATH_PREFIX = "classpath:";
    public static final String FILESYSTEM_PREFIX = "filesystem:";

    private final String prefix;
    private final String rawPath;
    // The first folder in the path. This will equal rawPath if the path does not contain any wildcards
    private final File rootPath;
    private final Pattern pathRegex;

    Location(final String prefix, final String rawPath, final File rootPath, final Pattern pathRegex)
    {
        this.prefix = prefix;
        this.rawPath = rawPath;
        this.rootPath = rootPath;
        this.pathRegex = pathRegex;
    }

    /**
     * Creates a new location.
     *
     * @param descriptor The location descriptor.
     */
    public static Location of(final String descriptor)
    {
        Objects.requireNonNull(descriptor, "descriptor");
        return new Builder(descriptor.trim()).build();
    }

    /**
     * @return Whether the given path matches this locations regex. Will always return true when the location did not contain any wildcards.
     */
    public boolean matchesPath(final String path)
    {
        if (null == this.pathRegex) {
            return true;
        }

        return this.pathRegex.matcher(path).matches();
    }

    /**
     * Returns the path relative to this location. If the location path contains wildcards, the returned path will be relative
     * to the last non-wildcard folder in the path.
     *
     * @return the path relative to this location
     */
    public String getPathRelativeToThis(final String path)
    {
        if (null != this.pathRegex && this.pathRegex.pattern().contains("?<relpath>")) {
            final Matcher matcher = this.pathRegex.matcher(path);
            if (matcher.matches()) {
                final String relPath = matcher.group("relpath");
                if (null != relPath && !relPath.isEmpty()) {
                    return relPath;
                }
            }
        }

        final int rootPathLength = this.rootPath.getPath().length();
        return 0 < rootPathLength ? path.substring(rootPathLength + 1) : path;
    }

    /**
     * Checks whether this denotes a location on the classpath.
     *
     * @return {@code true} if it does, {@code false} if it doesn't.
     */
    public boolean isClassPath()
    {
        return CLASSPATH_PREFIX.equals(this.prefix);
    }

    /**
     * Checks whether this denotes a location on the filesystem.
     *
     * @return {@code true} if it does, {@code false} if it doesn't.
     */
    public boolean isFileSystem()
    {
        return FILESYSTEM_PREFIX.equals(this.prefix);
    }

    /**
     * Checks whether this location is a parent of this other location.
     *
     * @param other The other location.
     * @return {@code true} if it is, {@code false} if it isn't.
     */
    public boolean isParentOf(final Location other)
    {
        if (null != this.pathRegex || null != other.pathRegex) {
            return false;
        }
        if (isClassPath() && other.isClassPath()) {
            return (other.getDescriptor() + "/").startsWith(getDescriptor() + "/");
        }
        if (isFileSystem() && other.isFileSystem()) {
            return (other.getDescriptor() + File.separator).startsWith(getDescriptor() + File.separator);
        }
        return false;
    }

    /**
     * @return The prefix part of the location. Can be either classpath: or filesystem:.
     */
    public String getPrefix()
    {
        return this.prefix;
    }

    /**
     * @return The root part of the path part of the location.
     */
    public File getRootPath()
    {
        return this.rootPath;
    }

    /**
     * @return The path part of the location.
     */
    public String getPath()
    {
        return this.rawPath;
    }

    /**
     * @return The the regex that matches in original path. Null if the original path did not contain any wildcards.
     */
    public Pattern getPathRegex()
    {
        return this.pathRegex;
    }

    /**
     * @return The complete location descriptor.
     */
    public String getDescriptor()
    {
        return this.prefix + this.rawPath;
    }

    @Override public int compareTo(final Location o)
    {
        return getDescriptor().compareTo(o.getDescriptor());
    }

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof Location) {
            final Location other = (Location) o;
            return getDescriptor().equals(other.getDescriptor());
        }
        return false;
    }

    public Location validate()
    {
        if (!this.rootPath.exists()) {
            throw new Location.NotFound(this.rootPath);
        }
        if (!this.rootPath.canRead()) {
            throw new Location.NotReadable(this.rootPath);
        }
        if (!this.rootPath.isDirectory()) {
            throw new Location.NotDirectory(this.rootPath);
        }
        return this;
    }

    @Override public int hashCode()
    {
        return getDescriptor().hashCode();
    }

    /**
     * @return The complete location descriptor.
     */
    @Override public String toString()
    {
        return getDescriptor();
    }

    public String path()
    {
        return this.rootPath.getPath();
    }

    public boolean isClassPathRoot()
    {
        return isClassPath() && "".equals(path());
    }

    private static final class Builder
    {
        private final String normalizedDescriptor;

        private String prefix, rawPath, rootPath;
        private Pattern pathRegex;

        private Builder(final String normalizedDescriptor)
        {
            this.normalizedDescriptor = normalizedDescriptor;
        }

        private Location build()
        {
            if (this.normalizedDescriptor.contains(":")) {
                this.prefix = this.normalizedDescriptor.substring(0, this.normalizedDescriptor.indexOf(":") + 1);
                this.rawPath = this.normalizedDescriptor.substring(this.normalizedDescriptor.indexOf(":") + 1);
            } else {
                this.prefix = CLASSPATH_PREFIX;
                this.rawPath = this.normalizedDescriptor;
            }

            if (CLASSPATH_PREFIX.equals(this.prefix)) {
                if (this.rawPath.startsWith("/")) {
                    this.rawPath = this.rawPath.substring(1);
                }
                if (this.rawPath.endsWith("/")) {
                    this.rawPath = this.rawPath.substring(0, this.rawPath.length() - 1);
                }
                processRawPath();
            } else if (FILESYSTEM_PREFIX.equals(this.prefix)) {
                processRawPath();
                this.rootPath = new File(this.rootPath).getPath();

                // if the original path contained no wildcards, also normalise it
                if (null == this.pathRegex) {
                    this.rawPath = new File(this.rawPath).getPath();
                }
            } else {
                throw new UnknownPrefix(this.normalizedDescriptor);
            }

            if (this.rawPath.endsWith(File.separator)) {
                this.rawPath = this.rawPath.substring(0, this.rawPath.length() - 1);
            }

            return new Location(this.prefix, this.rawPath, new File(this.rootPath), this.pathRegex);
        }

        /**
         * Process the rawPath into a rootPath and a regex.
         * Supported wildcards:
         * **: Match any 0 or more directories
         * *: Match any sequence of non-seperator characters
         * ?: Match any single character
         */
        private void processRawPath()
        {
            if (this.rawPath.contains("*") || this.rawPath.contains("?")) {
                // we need to figure out the root, and create the regex

                final String separator = FILESYSTEM_PREFIX.equals(this.prefix) ? File.separator : "/";
                final String escapedSeparator = separator.replace("\\", "\\\\").replace("/", "\\/");

                // split on either of the path seperators
                final String[] pathSplit = this.rawPath.split("[\\\\/]");

                final StringBuilder rootPart = new StringBuilder();
                final StringBuilder patternPart = new StringBuilder();

                boolean endsInFile = false;
                boolean skipSeperator = false;
                boolean inPattern = false;

                for (final String pathPart : pathSplit) {
                    endsInFile = false;

                    if (pathPart.contains("*") || pathPart.contains("?")) {
                        inPattern = true;
                    }

                    if (inPattern) {
                        if (skipSeperator) {
                            skipSeperator = false;
                        } else {
                            patternPart.append("/");
                        }

                        String regex;
                        if ("**".equals(pathPart)) {
                            regex = "([^/]+/)*?";

                            // this pattern contains the ending separator, so make sure we skip appending it after
                            skipSeperator = true;
                        } else {
                            endsInFile = pathPart.contains(".");

                            regex = pathPart;
                            regex = regex.replace(".", "\\.");
                            regex = regex.replace("?", "[^/]");
                            regex = regex.replace("*", "[^/]+?");
                        }

                        patternPart.append(regex);
                    } else {
                        rootPart.append(separator).append(pathPart);
                    }
                }

                // We always append a separator before each part, so ensure we skip it when setting the final rootPath
                this.rootPath = 0 < rootPart.length() ? rootPart.toString().substring(1) : "";

                // Again, skip first separator
                String pattern = patternPart.toString().substring(1);

                // Replace the temporary / with the actual escaped separator
                pattern = pattern.replace("/", escapedSeparator);

                // Append the rootpath if it is non-empty
                if (0 < rootPart.length()) {
                    pattern = this.rootPath.replace(separator, escapedSeparator) + escapedSeparator + pattern;
                }

                // if the path did not end in a file, then append the file match pattern
                if (!endsInFile) {
                    pattern = pattern + escapedSeparator + "(?<relpath>.*)";
                }

                this.pathRegex = Pattern.compile(pattern);
            } else {
                this.rootPath = this.rawPath;
            }
        }
    }

    public static final class UnknownPrefix extends MigradorException
    {
        UnknownPrefix(final String descriptor) {super("Unknown prefix for location (should be either filesystem: or classpath:): " + descriptor);}
    }

    public static final class NotFound extends MigradorException
    {
        public NotFound(final File location) {super("Location '" + location + "' doesn't exists.");}
    }

    public static final class NotReadable extends MigradorException
    {
        public NotReadable(final String location, final Throwable cause) {super("Location at '" + location + "' isn't readable.", cause);}

        public NotReadable(final File location) {super("Location at '" + location + "' isn't readable.");}
    }

    public static final class NotDirectory extends MigradorException
    {
        public NotDirectory(final File location) {super("Location at '" + location + "' isn't a directory.");}
    }

    public static final class NotResolved extends MigradorException
    {
        public NotResolved(final Location location, final Classes classes)
        {
            super("Location '" + location.rootPath + "' could not be resolved (" + classes + ')');
        }

        public NotResolved(final Location location, final Classes classes, final Throwable cause)
        {
            super("Location '" + location.rootPath + "' could not be resolved (" + classes + ')', cause);
        }
    }

    public static final class Converter implements org.aeonbits.owner.Converter<Location>
    {
        @Override public Location convert(final Method method, final String input) {return Location.of(input);}
    }


    /**
     * Checks whether this denotes a location on the classpath.
     *
     * @return {@code true} if it does, {@code false} if it doesn't.
     */
    public static boolean isClassPath(final String locationPath) {return locationPath.startsWith(CLASSPATH_PREFIX);}

    /**
     * Checks whether this denotes a location on the filesystem.
     *
     * @return {@code true} if it does, {@code false} if it doesn't.
     */
    public static boolean isFileSystem(final String locationPath) {return locationPath.startsWith(FILESYSTEM_PREFIX);}
}
