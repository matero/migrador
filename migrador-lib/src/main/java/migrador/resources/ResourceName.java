package migrador.resources;

import migrador.MigradorException;
import migrador.migrations.Version;

/**
 * Represents a resource name, parsed into its components.
 * <p>
 * Versioned and Undo migrations are named in the form prefixVERSIONseparatorDESCRIPTIONsuffix;
 * Repeatable migrations and callbacks are named in the form prefixSeparatorDESCRIPTIONsuffix
 */
public class ResourceName
{
    private final String prefix;
    private final Version version;
    private final String description;

    public ResourceName(final String prefix, final Version version, final String description)
    {
        this.prefix = prefix;
        this.version = version;
        this.description = description;
    }

    public String getPrefix()
    {
        return this.prefix;
    }

    public Version getVersion()
    {
        return this.version;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getFilename()
    {
        if ("".equals(this.description)) {
            return this.prefix + this.version;
        } else {
            return this.prefix + this.version + "__" + this.description;
        }
    }

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof ResourceName) {
            final ResourceName other = (ResourceName) o;
            return this.prefix.equals(other.prefix) && this.version.equals(other.version) && this.description.equals(other.description);
        }
        return false;
    }

    @Override public int hashCode()
    {
        int result = this.prefix.hashCode();
        result = 31 * result + this.version.hashCode();
        result = 31 * result + this.description.hashCode();
        return result;
    }

    @Override public String toString()
    {
        return "ResourceName(prefix='" + this.prefix + "', version=" + this.version + ", description='" + this.description + "')";
    }

    public static final class Invalid extends MigradorException
    {
        public Invalid(final String message)
        {
            super(message);
        }
    }
}
