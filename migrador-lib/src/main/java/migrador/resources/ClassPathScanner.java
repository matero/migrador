/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.resources;

import migrador.helpers.Classes;
import migrador.helpers.Urls;
import migrador.migrations.JavaMigration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Pattern;

final class ClassPathScanner
{
    private static final Logger LOG = LoggerFactory.getLogger(ClassPathScanner.class);

    private final Classes classes;
    private final MultiMap<Location, URL> locationUrlCache = new MultiMap<>();
    private final LocationScannerCache locationScannerCache;
    private final ResourceNameCache resourceNameCache;
    private final Charset encoding;

    ClassPathScanner(
        final Classes classes,
        final Charset encoding,
        final ResourceNameCache resourceNameCache,
        final LocationScannerCache locationScannerCache
                    )
    {
        this.classes = classes;
        this.resourceNameCache = resourceNameCache;
        this.locationScannerCache = locationScannerCache;
        this.encoding = encoding;
    }

    final static class ScanResult
    {
        final Set<Resource> resources;
        final Set<Class<? extends JavaMigration>> classes;

        ScanResult(final Set<Resource> resources, final Set<Class<? extends JavaMigration>> classes)
        {
            this.resources = Set.copyOf(resources);
            this.classes = Set.copyOf(classes);
        }
    }

    ScanResult scanAt(final Location location)
    {
        final var resources = new java.util.TreeSet<Resource>();

        LOG.debug("Scanning for classpath resources at '{}'...", location);
        for (final String resourceName : findResourceNames(location)) {
            resources.add(Resource.onClassPathUsing(this.classes).location(location).withAbsolutePath(resourceName).encoding(this.encoding).create());
            LOG.debug("Found resource: " + resourceName);
        }

        LOG.debug("Scanning for classes at {}...", location);
        final var classes = new java.util.HashSet<Class<? extends JavaMigration>>();
        for (final Resource resource : resources) {
            if (resource.isClass()) {
                final Class<? extends JavaMigration> clazz = this.classes.load(JavaMigration.class, resource);
                if (null != clazz) {
                    classes.add(clazz);
                }
            }
        }

        return new ScanResult(resources, classes);
    }

    /**
     * Finds the resources names present at this location and below on the classpath starting with this prefix and
     * ending with this suffix.
     *
     * @return The resource names.
     */
    private Set<String> findResourceNames(final Location location)
    {
        final TreeSet<String> resourceNames = new TreeSet<>();

        final List<URL> locationUrls = getLocationUrlsForPath(location);
        for (final URL url : locationUrls) {
            LOG.debug("Scanning URL: {}", url.toExternalForm());

            final String protocol = url.getProtocol();
            final ClassPathLocationScanner classPathLocationScanner = createLocationScanner(protocol);
            if (null == classPathLocationScanner) {
                final String scanRoot = Urls.toFilePath(url);
                LOG.warn("Unable to scan location: {} (unsupported protocol: {})", scanRoot, protocol);
            } else {
                Set<String> names = this.resourceNameCache.get(classPathLocationScanner, url);
                if (null == names) {
                    names = classPathLocationScanner.findResourceNames(location.path(), url);
                    this.resourceNameCache.put(classPathLocationScanner, url, names);
                }
                for (final String name : names) {
                    if (location.matchesPath(name)) {
                        resourceNames.add(name);
                    }
                }
            }
        }

        // Make an additional attempt at finding resources in jar files in case the URL scanning method above didn't
        // yield any results.
        boolean locationResolved = !locationUrls.isEmpty();

        // Starting with Java 11, resources at the root of the classpath aren't being found using the URL scanning
        // method above and we need to revert to Jar file walking.

        if (!locationResolved || location.isClassPathRoot()) {
            if (this.classes.canResolveUrls()) {
                for (final URL url : this.classes.resolveUrls()) {
                    if (isJarFile(url) && !isSystemJar(url)) {
                        // All non-system jars on disk
                        JarFile jarFile;
                        try {
                            try {
                                jarFile = new JarFile(url.toURI().getSchemeSpecificPart());
                            } catch (final URISyntaxException ex) {
                                // Fallback for URLs that are not valid URIs (should hardly ever happen).
                                jarFile = new JarFile(url.getPath().substring("file:".length()));
                            }
                        } catch (final IOException | SecurityException e) {
                            LOG.warn("Skipping unloadable jar file: " + url, e);
                            continue;
                        }

                        try {
                            final Enumeration<JarEntry> entries = jarFile.entries();
                            while (entries.hasMoreElements()) {
                                final String entryName = entries.nextElement().getName();
                                if (entryName.startsWith(location.path())) {
                                    locationResolved = true;
                                    resourceNames.add(entryName);
                                }
                            }
                        } finally {
                            try {
                                jarFile.close();
                            } catch (final IOException e) {
                                // Ignore
                            }
                        }
                    }
                }
            }
        }

        if (!locationResolved) {
            throw new Location.NotResolved(location, this.classes);
        }

        return resourceNames;
    }

    private boolean isSystemJar(final URL url)
    {
        return url.getPath().matches(".*" + Pattern.quote("/jre/lib/") + ".*");
    }

    private boolean isJarFile(final URL url)
    {
        return "file".equals(url.getProtocol()) && url.getPath().endsWith(".jar");
    }

    /**
     * Gets the physical location urls for this logical path on the classpath.
     *
     * @param location The location on the classpath.
     * @return The underlying physical URLs.
     */
    private List<URL> getLocationUrlsForPath(final Location location)
    {
        if (!this.locationUrlCache.containsKey(location)) {
            LOG.debug("Determining location urls for {} using ClassLoader {}...", location, this.classes);

            final Enumeration<URL> urls = this.classes.getResources(location);
            while (urls.hasMoreElements()) {
                this.locationUrlCache.put(location, urls.nextElement());
            }
        }
        return this.locationUrlCache.get(location);
    }

    /**
     * Creates an appropriate location scanner for this url protocol.
     *
     * @param protocol The protocol of the location url to scan.
     * @return The location scanner or {@code null} if it could not be created.
     */
    private ClassPathLocationScanner createLocationScanner(final String protocol)
    {
        if (this.locationScannerCache.knows(protocol)) {
            return this.locationScannerCache.get(protocol);
        }

        if ("file".equals(protocol)) {
            final FileSystemClassPathLocationScanner locationScanner = new FileSystemClassPathLocationScanner();
            this.locationScannerCache.put(protocol, locationScanner);
            this.resourceNameCache.register(locationScanner);
            return locationScanner;
        }

        if ("jar".equals(protocol)) {
            final ClassPathLocationScanner locationScanner = new JarFileClassPathLocationScanner("!/");
            this.locationScannerCache.put(protocol, locationScanner);
            this.resourceNameCache.register(locationScanner);
            return locationScanner;
        }

        return null;
    }

    private static final class MultiMap<K, V>
    {

        /**
         * The load factor used when none specified in constructor.
         */
        static final float DEFAULT_LOAD_FACTOR = 0.75f;
        static final int DEFAULT_CAPACITY = 16;

        private final HashMap<K, ArrayList<V>> values;

        MultiMap() {this(DEFAULT_CAPACITY, DEFAULT_LOAD_FACTOR);}

        /**
         * Constructs an empty {@code MultiMap} with the specified initial capacity and load factor.
         *
         * @param initialCapacity the initial capacity
         * @param loadFactor      the load factor
         * @throws IllegalArgumentException if the initial capacity is negative
         *                                  or the load factor is nonpositive
         */
        MultiMap(final int initialCapacity, final float loadFactor) {this.values = new HashMap<>(initialCapacity, loadFactor);}

        boolean containsKey(final Object key) {return this.values.containsKey(key);}

        List<V> get(final Object key) {return this.values.get(key);}

        MultiMap<K, V> put(final K key, final V value)
        {
            final var l = this.values.computeIfAbsent(key, k -> new ArrayList<>());
            l.add(value);
            return this;
        }
    }

}
