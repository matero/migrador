/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.resources;

import java.util.Set;

public interface ResourceProvider
{
    ResourceProvider NONE = NoopResourceProvider.INSTANCE;

    /**
     * Retrieves the resource with this name.
     *
     * @param name The name of the resource.
     * @return The resource or {@code null} if not found.
     */
    Resource getResource(String name);

    /**
     * @return Retrieve all resources whose name begins with this prefix and ends with any of these suffix.
     */
    Set<Resource> getResources(String prefix, String suffix);
}

enum NoopResourceProvider implements ResourceProvider
{
    INSTANCE;

    @Override public Resource getResource(final String name)
    {
        return Resource.none();
    }

    /**
     * Retrieve all resources whose name begins with this prefix and ends with any of these suffixes.
     *
     * @param prefix   The prefix.
     * @param suffixes The suffixes.
     * @return The matching resources.
     */
    @Override public Set<Resource> getResources(final String prefix, final String suffix)
    {
        return Set.of();
    }
}
