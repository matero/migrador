/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.resources;

import migrador.helpers.Classes;
import migrador.migrations.JavaMigration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * Scanner for Resources and Classes.
 */
public class ResourceScanner implements ResourceProvider, ClassProvider
{
    private static final Logger LOG = LoggerFactory.getLogger(ResourceScanner.class);

    private final Set<Resource> resources;
    private final Set<Class<? extends JavaMigration>> classes;

    ResourceScanner(final Set<Resource> resources, final Set<Class<? extends JavaMigration>> classes)
    {
        this.resources = Set.copyOf(resources);
        this.classes = Set.copyOf(classes);
    }

    /*
     * Constructor. Scans the given locations for resources, and classes implementing the specified interface.
     */
    public static Builder scan(final java.util.Collection<Location> locations) {return new Builder(locations);}

    @Override public Resource getResource(final String resourceName)
    {
        for (final Resource resource : this.resources) {
            if (resource.hasRelativePath(resourceName)) {
                return resource;
            }
        }
        throw new Resource.NotFound(resourceName);
    }

    /**
     * Returns all known resources starting with the specified prefix and ending with any of the specified suffixes.
     *
     * @param prefix The prefix of the resource names to match.
     * @param suffix The suffix of the resource names to match.
     * @return The resources that were found.
     */
    @Override public Set<Resource> getResources(final String prefix, final String suffix)
    {
        final var result = new java.util.HashSet<Resource>();
        for (final Resource resource : this.resources) {
            if (resource.matchPrefixAndSuffix(prefix, suffix)) {
                result.add(resource);
            } else {
                LOG.debug("Filtering out resource: {} (filename: {})", resource.getAbsolutePath(), resource.getFilename());
            }
        }
        return Set.copyOf(result);
    }

    /**
     * Scans the classpath for concrete classes under the specified package implementing the specified interface.
     * Non-instantiable abstract classes are filtered out.
     *
     * @return The non-abstract classes that were found.
     */
    @Override public Set<Class<? extends JavaMigration>> getClasses() {return this.classes;}

    public static final class Builder
    {
        private final Set<Location> locations;
        private Classes classes;
        private Charset encoding = Charset.defaultCharset();
        private ResourceNameCache resourceNameCache;
        private LocationScannerCache locationScannerCache;

        public Builder(final java.util.Collection<Location> locations)
        {
            this.locations = new java.util.HashSet<>(locations);
        }

        public Builder classes(final Classes value)
        {
            this.classes = value;
            return this;
        }

        public Builder encoding(final Charset value)
        {
            this.encoding = value;
            return this;
        }

        public Builder resourceNameCache(final ResourceNameCache value)
        {
            this.resourceNameCache = value;
            return this;
        }

        public Builder locationScannerCache(final LocationScannerCache value)
        {
            this.locationScannerCache = value;
            return this;
        }

        public ResourceScanner build()
        {
            if (this.locations.isEmpty()) {
                throw new IllegalStateException("at least one location must be defined");
            }

            requireNonNull(this.classes, "classes");
            requireNonNull(this.encoding, "encoding");
            requireNonNull(this.locations, "locations");

            if (null == this.resourceNameCache) {
                this.resourceNameCache = new ResourceNameCache();
            }
            if (null == this.locationScannerCache) {
                this.locationScannerCache = new LocationScannerCache();
            }

            final var scannedResources = new java.util.HashSet<Resource>();
            final var scannedClasses = new java.util.HashSet<Class<? extends JavaMigration>>();

            {// scan locations looking for resources
                final FileSystemScanner fileSystemScanner = new FileSystemScanner(this.encoding);
                final ClassPathScanner classPathScanner = new ClassPathScanner(
                    this.classes, this.encoding, this.resourceNameCache, this.locationScannerCache);

                for (final Location location : this.locations) {
                    if (location.isFileSystem()) {
                        scannedResources.addAll(fileSystemScanner.scanAt(location));
                    } else {
                        final ClassPathScanner.ScanResult scanned = classPathScanner.scanAt(location);
                        scannedResources.addAll(scanned.resources);
                        scannedClasses.addAll(scanned.classes);
                    }
                }
            }

            return new ResourceScanner(scannedResources, scannedClasses);
        }
    }
}

