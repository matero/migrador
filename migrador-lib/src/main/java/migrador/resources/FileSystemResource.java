/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.resources;

import migrador.MigradorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.StandardOpenOption;

final class FileSystemResource implements Resource
{
    private static final Logger LOG = LoggerFactory.getLogger(FileSystemResource.class);

    private final File file;
    private final String relativePath;
    final Charset encoding;

    FileSystemResource(final File file, final String relativePath, final Charset encoding)
    {
        this.file = file;
        this.relativePath = relativePath;
        this.encoding = encoding;
    }

    /**
     * Creates a new ClassPathResource.
     *
     * @param fileNameWithPath The path and filename of the resource on the filesystem.
     */
    static FileSystemResource of(final Location location, final String fileNameWithPath, final Charset encoding)
    {
        final File file = new File(new File(fileNameWithPath).getPath());
        if (null == location) {
            return new FileSystemResource(file, file.getPath(), encoding);
        }
        return new FileSystemResource(file, location.getPathRelativeToThis(file.getPath()).replace("\\", "/"), encoding);
    }

    /**
     * @return The location of the resource on the filesystem.
     */
    @Override public String getAbsolutePath()
    {
        return this.file.getPath();
    }

    /**
     * Retrieves the location of this resource on disk.
     *
     * @return The location of this resource on disk.
     */
    @Override public String getAbsolutePathOnDisk()
    {
        return this.file.getAbsolutePath();
    }

    @Override public Reader read()
    {
        try {
            return Channels.newReader(FileChannel.open(this.file.toPath(), StandardOpenOption.READ), this.encoding.newDecoder(), 4096);
        } catch (final IOException e) {
            if (LOG.isWarnEnabled()) {
                LOG.warn("Unable to load filesystem resource" + this.file.getPath() + " using FileChannel.open.", e);
            }
        }

        try {
            return new BufferedReader(new ByteOrderMarkStrippingReader(new InputStreamReader(new FileInputStream(this.file), this.encoding)));
        } catch (final IOException e) {
            throw new UnableToLoad(this, e);
        }
    }

    /**
     * @return The filename of this resource, without the path.
     */
    @Override public String getFilename()
    {
        return this.file.getName();
    }

    @Override public String getRelativePath()
    {
        return this.relativePath;
    }

    static final class UnableToLoad extends MigradorException
    {
        UnableToLoad(final FileSystemResource fileSystemResource, final Throwable cause)
        {
            super("Unable to load filesystem resource: " + fileSystemResource.getAbsolutePath() + " (encoding: " + fileSystemResource.encoding + ")",
                  cause);
        }
    }
}

