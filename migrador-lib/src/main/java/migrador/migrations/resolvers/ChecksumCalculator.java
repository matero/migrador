/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.migrations.resolvers;

import migrador.helpers.ByteOrderMark;
import migrador.resources.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.zip.CRC32;

interface ChecksumCalculator
{
    /**
     * Calculates the checksum of these resources. The checksum is encoding and line-ending independent.
     *
     * @return The crc-32 checksum of the bytes.
     */
    default int calculate(final java.util.List<Resource> resources)
    {
        Objects.requireNonNull(resources, "resources");
        if (resources.isEmpty()) {
            throw new IllegalArgumentException("resources must have at least 1 element");
        }
        Objects.requireNonNull(resources.get(0), "resources.get(0)");
        return calculateChecksumForResource(resources.get(0));
    }

    /**
     * Calculates the checksum of these resources. The checksum is encoding and line-ending independent.
     *
     * @return The crc-32 checksum of the bytes.
     */
    default int calculate(final Resource... resources)
    {
        Objects.requireNonNull(resources, "resources");
        if (0 == resources.length) {
            throw new IllegalArgumentException("resources must have at least 1 element");
        }
        Objects.requireNonNull(resources[0], "resources[0]");
        return calculateChecksumForResource(resources[0]);
    }

    int calculateChecksumForResource(Resource resource);
}

enum DefaultChecksumCalculator implements ChecksumCalculator
{
    INSTANCE;

    @Override public int calculateChecksumForResource(final Resource resource)
    {
        final CRC32 crc32 = new CRC32();

        try (final BufferedReader bufferedReader = new BufferedReader(resource.read(), 4096)) {
            String line = bufferedReader.readLine();

            if (null != line) {
                line = ByteOrderMark.FILTER.filterFrom(line);

                do {
                    line = trimLineBreak(line);
                    final byte[] lineBytes = line.getBytes(StandardCharsets.UTF_8);
                    crc32.update(lineBytes);
                }
                while (null != (line = bufferedReader.readLine()));
            }
        } catch (final IOException e) {
            throw new UnableToCalculateChecksum(resource.getFilename(), e);
        }

        return (int) crc32.getValue();
    }

    /**
     * Trim the trailing linebreaks (if any) from this string.
     *
     * @param str The string.
     * @return The string without trailing linebreak.
     */
    private static String trimLineBreak(final String str)
    {
        if (str.isEmpty()) {
            return str;
        }
        final StringBuilder result = new StringBuilder(str);
        while (0 < result.length() && isLineBreak(result.charAt(result.length() - 1))) {
            result.deleteCharAt(result.length() - 1);
        }
        return result.toString();
    }

    /**
     * Checks whether this character is a linebreak character.
     *
     * @param ch The character
     * @return {@code true} if it is, {@code false} if not.
     */
    private static boolean isLineBreak(final char ch)
    {
        return '\n' == ch || '\r' == ch;
    }
}
