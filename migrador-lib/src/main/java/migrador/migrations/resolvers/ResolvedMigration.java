/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.migrations.resolvers;

import migrador.actions.ResolvedMigrationExecutor;
import migrador.migrations.JavaMigration;
import migrador.migrations.Type;
import migrador.migrations.Version;
import migrador.resources.Resource;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Migration resolved through a MigrationResolver. Can be applied against a database.
 */
public abstract class ResolvedMigration implements ChecksumMatcher, Comparable<ResolvedMigration>
{
    private final Type type;
    private final Version version;
    private final String description;
    private final String physicalLocation;

    ResolvedMigration(final Type type, final Version version, final String description, final String physicalLocation)
    {
        this.type = type;
        this.version = version;
        this.description = description;
        this.physicalLocation = physicalLocation;
    }

    public final Type getType()
    {
        return this.type;
    }

    public final Version getVersion()
    {
        return this.version;
    }

    public final String getDescription()
    {
        return this.description;
    }

    public abstract Integer getChecksum();

    public final boolean hasVersion() {return !this.version.isNone();}

    @Override public final int compareTo(final ResolvedMigration other)
    {
        return getVersion().compareTo(other.getVersion());
    }

    public final String getLabels()
    {
        return this.type.getLabels();
    }

    public final boolean hasInstallType()
    {
        return this.type.isInstall();
    }

    static CypherResolvedMigrationBuilder forCypherScript(final Resource resource)
    {
        return new CypherResolvedMigrationBuilder(Type.CYPHER)
            .script(resource.getRelativePath())
            .physicalLocation(resource.getAbsolutePathOnDisk());
    }

    static JavaResolvedMigrationBuilder forInstance(final JavaMigration javaMigration)
    {
        Objects.requireNonNull(javaMigration, "javaMigration is required");
        return new JavaResolvedMigrationBuilder(javaMigration);
    }

    public abstract String getScript();

    public String getPhysicalLocation() {return this.physicalLocation;}

    public abstract void migrateWith(ResolvedMigrationExecutor executor);

    public static final class CypherResolvedMigrationBuilder
    {
        private final Type type;

        private Version version;
        private String description;
        private String script;
        private Integer checksum;
        private Integer equivalentChecksum;
        private String physicalLocation;

        CypherResolvedMigrationBuilder(final Type type)
        {
            this.type = type;
        }

        public CypherResolvedMigrationBuilder version(final Version value)
        {
            this.version = value;
            return this;
        }

        public CypherResolvedMigrationBuilder description(final String value)
        {
            this.description = value;
            return this;
        }

        public CypherResolvedMigrationBuilder script(final String value)
        {
            this.script = value;
            return this;
        }

        public CypherResolvedMigrationBuilder checksum(final Integer value)
        {
            this.checksum = value;
            return this;
        }

        public CypherResolvedMigrationBuilder equivalentChecksum(final Integer value)
        {
            this.equivalentChecksum = value;
            return this;
        }

        public CypherResolvedMigrationBuilder physicalLocation(final String value)
        {
            this.physicalLocation = value;
            return this;
        }

        public ResolvedMigration build()
        {
            requireNonNull(this.script, "script is required");
            requireNonNull(this.physicalLocation, "physicalLocation");

            if (null != this.description) {
                this.description = this.description.trim().replace('_', ' ');
            }

            return new CypherResolvedMigration(
                this.version,
                this.description,
                this.script,
                this.checksum,
                this.equivalentChecksum,
                this.type,
                this.physicalLocation);
        }
    }

    public static final class JavaResolvedMigrationBuilder
    {
        private String physicalLocation;
        private final JavaMigration javaMigration;

        JavaResolvedMigrationBuilder(final JavaMigration javaMigration)
        {
            this.javaMigration = javaMigration;
        }

        public JavaResolvedMigrationBuilder physicalLocation(final String value)
        {
            this.physicalLocation = value;
            return this;
        }

        public ResolvedMigration build()
        {
            requireNonNull(this.physicalLocation, "physicalLocation");
            final Class<? extends JavaMigration> migrationClass = this.javaMigration.getClass();
            {
                final var repeatable = migrationClass.getAnnotation(JavaMigration.Repeatable.class);
                if (null != repeatable) {
                    return new JavaResolvedMigration(this.javaMigration, Version.none(), repeatable.description(), this.physicalLocation);
                }
            }
            {
                final var versioned = migrationClass.getAnnotation(JavaMigration.Versioned.class);
                if (null != versioned) {
                    return new JavaResolvedMigration(this.javaMigration,
                                                     Version.make(versioned.version()),
                                                     versioned.description(),
                                                     this.physicalLocation);
                }
            }
            throw new IllegalStateException("Migrations implementing " + JavaMigration.class + " must be annotated with "
                                            + JavaMigration.Versioned.class + " OR with " + JavaMigration.Repeatable.class);
        }
    }
}


