/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.migrations.resolvers;

import migrador.actions.ResolvedMigrationExecutor;
import migrador.migrations.JavaMigration;
import migrador.migrations.Type;
import migrador.migrations.Version;

import java.util.Objects;

public final class JavaResolvedMigration extends ResolvedMigration
{
    private final JavaMigration javaMigration;

    JavaResolvedMigration(final JavaMigration javaMigration, final Version version, final String description, final String physicalLocation)
    {
        super(Type.JAVA, version, description, physicalLocation);
        this.javaMigration = javaMigration;
    }

    public JavaMigration getJavaMigration()
    {
        return this.javaMigration;
    }

    @Override public Integer getChecksum()
    {
        return this.javaMigration.checksum();
    }

    @Override public String getScript()
    {
        return this.javaMigration.getClass().getCanonicalName();
    }

    @Override public void migrateWith(final ResolvedMigrationExecutor executor)
    {
        executor.executeJavaMigration(this);
    }

    @Override public boolean checksumMatches(final Integer checksum)
    {
        return Objects.equals(checksum, getChecksum());
    }

    @Override public boolean checksumMatchesWithoutBeingIdentical(final Integer checksum)
    {
        return Objects.equals(checksum, getChecksum());
    }
}
