/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.migrations.resolvers;

import migrador.helpers.Classes;
import migrador.migrations.JavaMigration;

import java.util.ArrayList;
import java.util.List;

/**
 * Migration resolver for a fixed set of pre-instantiated Java-based migrations.
 */
class FixedJavaMigrationResolver extends MigrationResolver
{
    /**
     * The JavaMigration instances to use.
     */
    private final List<JavaMigration> javaMigrations;
    private final Classes classes;

    /**
     * Creates a new instance.
     *
     * @param javaMigrations The JavaMigration instances to use.
     */
    public FixedJavaMigrationResolver(final List<JavaMigration> javaMigrations, final Classes classes)
    {
        this.javaMigrations = javaMigrations;
        this.classes = classes;
    }

    @Override public List<ResolvedMigration> resolveMigrations()
    {
        final var resolvedMigrations = new ArrayList<ResolvedMigration>(this.javaMigrations.size());
        for (final var javaMigration : this.javaMigrations) {
            resolvedMigrations.add(ResolvedMigration.forInstance(javaMigration)
                                                    .physicalLocation(this.classes.getLocationOnDisk(javaMigration.getClass()))
                                                    .build());
        }
        resolvedMigrations.sort(ResolvedMigrationComparator.INSTANCE);
        return List.copyOf(resolvedMigrations);
    }
}
