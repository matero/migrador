/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.migrations.resolvers;

import migrador.actions.ResolvedMigrationExecutor;
import migrador.migrations.Type;
import migrador.migrations.Version;

import java.util.Objects;

public final class CypherResolvedMigration extends ResolvedMigration
{
    private final String script;
    private final Integer checksum;
    private final Integer equivalentChecksum;

    protected CypherResolvedMigration(
        final Version version,
        final String description,
        final String script,
        final Integer checksum,
        final Integer equivalentChecksum,
        final Type type,
        final String physicalLocation)
    {
        super(type, version, description, physicalLocation);
        this.script = script;
        this.checksum = checksum;
        this.equivalentChecksum = equivalentChecksum;
    }

    @Override public String getScript()
    {
        return this.script;
    }

    @Override public void migrateWith(final ResolvedMigrationExecutor executor)
    {
        executor.executeCypherScript(this);
    }

    @Override public Integer getChecksum()
    {
        return null == this.checksum ? this.equivalentChecksum : this.checksum;
    }

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }

        if (o instanceof CypherResolvedMigration) {
            final var other = (CypherResolvedMigration) o;

            return getType() == other.getType() &&
                   getVersion().equals(other.getVersion()) &&
                   getDescription().equals(other.getDescription()) &&
                   this.script.equals(other.script) &&
                   this.getPhysicalLocation().equals(other.getPhysicalLocation()) &&
                   Objects.equals(getChecksum(), other.getChecksum());
        }
        return false;
    }

    @Override public int hashCode()
    {
        int result = this.getVersion().hashCode();
        result = 31 * result + this.getDescription().hashCode();
        result = 31 * result + this.script.hashCode();
        result = 31 * result + Objects.hashCode(getChecksum());
        result = 31 * result + this.getType().hashCode();
        result = 31 * result + this.getPhysicalLocation().hashCode();
        return result;
    }

    @Override public boolean checksumMatches(final Integer checksum)
    {
        return Objects.equals(checksum, this.checksum) || Objects.equals(checksum, this.equivalentChecksum);
    }

    @Override public boolean checksumMatchesWithoutBeingIdentical(final Integer checksum)
    {
        // The checksum in the database matches the one calculated without replacement.
        return Objects.equals(checksum, this.equivalentChecksum);
    }
}
