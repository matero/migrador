/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.migrations.resolvers;

import migrador.resources.Resource;

import java.io.Reader;
import java.io.StringReader;

final class ResourceTemplate implements Resource
{
    private final Resource delegate;
    private final CypherRenderer renderer;

    ResourceTemplate(final Resource delegate, final CypherRenderer renderer)
    {
        this.delegate = delegate;
        this.renderer = renderer;
    }

    @Override public Reader read()
    {
        return new StringReader(this.renderer.render(this.delegate));
    }

    @Override public String getAbsolutePath()
    {
        return this.delegate.getAbsolutePath();
    }

    @Override public String getAbsolutePathOnDisk()
    {
        return this.delegate.getAbsolutePathOnDisk();
    }

    @Override public String getFilename()
    {
        return this.delegate.getFilename();
    }

    @Override public String getRelativePath()
    {
        return this.delegate.getRelativePath();
    }

    @Override public boolean hasRelativePath(final String resourceName)
    {
        return this.delegate.hasRelativePath(resourceName);
    }

    @Override public boolean isClass()
    {
        return this.delegate.isClass();
    }

    @Override public int compareTo(final Resource other)
    {
        return this.delegate.compareTo(other);
    }

    @Override public boolean matchPrefixAndSuffix(final String prefix, final String suffix) {return this.delegate.matchPrefixAndSuffix(prefix, suffix);}
}
