/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.migrations.resolvers;

import migrador.helpers.Classes;
import migrador.migrations.JavaMigration;
import migrador.resources.ClassProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Migration resolver for Java-based migrations. The classes must have a name like R__My_description, V1__Description
 * or V1_1_3__Description.
 */
final class JavaMigrationResolver extends MigrationResolver
{
    private static final Logger LOG = LoggerFactory.getLogger(JavaMigrationResolver.class);

    private final ClassProvider classProvider;
    private final Classes classes;

    JavaMigrationResolver(final Classes classes, final ClassProvider classProvider)
    {
        this.classes = classes;
        this.classProvider = classProvider;
    }

    @Override public List<ResolvedMigration> resolveMigrations()
    {
        final var resolvedClasses = this.classProvider.getClasses();
        final var resolvedMigrations = new ArrayList<ResolvedMigration>(resolvedClasses.size());

        for (final Class<? extends JavaMigration> javaMigrationClass : resolvedClasses) {
            final JavaMigration javaMigration = createMigrationFor(javaMigrationClass);
            resolvedMigrations.add(
                ResolvedMigration.forInstance(javaMigration)
                                 .physicalLocation(this.classes.getLocationOnDisk(javaMigrationClass))
                                 .build());
        }
        resolvedMigrations.trimToSize();
        resolvedMigrations.sort(ResolvedMigrationComparator.INSTANCE);
        return List.copyOf(resolvedMigrations);
    }

    private JavaMigration createMigrationFor(final Class<? extends JavaMigration> javaMigrationClass)
    {
        try {
            final Constructor<? extends JavaMigration> constructor = javaMigrationClass.getConstructor();
            return constructor.newInstance();
        } catch (final NoSuchMethodException e) {
            throw new IllegalStateException("Unable to instantiate migration class " + javaMigrationClass.getCanonicalName() +
                                            ". Please define a default constructor and add the corresponding annotations to it.", e);
        } catch (final InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new IllegalStateException("Unable to create java migration for " + javaMigrationClass + " with default constructor", e);
        }
    }
}
