/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.migrations.resolvers;

import migrador.resources.Resource;
import migrador.resources.ResourceName;
import migrador.resources.ResourceProvider;

import java.util.ArrayList;
import java.util.List;

final class CypherMigrationResolver extends MigrationResolver.WithNameInterpreter
{
    private final ResourceProvider resourceProvider;
    private final CypherRenderer renderer;
    private final ChecksumCalculator checksumCalculator;

    CypherMigrationResolver(
        final NameInterpreter nameInterpreter,
        final ResourceProvider resourceProvider,
        final CypherRenderer renderer,
        final ChecksumCalculator checksumCalculator
                           )
    {
        super(nameInterpreter);
        this.resourceProvider = resourceProvider;
        this.renderer = renderer;
        this.checksumCalculator = checksumCalculator;
    }

    @Override public List<ResolvedMigration> resolveMigrations()
    {
        final var migrations = getVersionedMigrations();
        migrations.addAll(getRepeatableMigrations());

        migrations.sort(ResolvedMigrationComparator.INSTANCE);

        return List.copyOf(migrations);
    }

    @Override protected ResourceNameErrorListener getErrorListener(final String resourceName) {return ResourceNameErrorListener.getInstance();}

    private ArrayList<ResolvedMigration> getVersionedMigrations() {return resolveMigrations("V", ".cql", false);}

    private ArrayList<ResolvedMigration> getRepeatableMigrations() {return resolveMigrations("R", ".cql", true);}

    private ArrayList<ResolvedMigration> resolveMigrations(final String prefix, final String suffix, final boolean repeatable)
    {
        final var resources = this.resourceProvider.getResources(prefix, suffix);
        if (resources.isEmpty()) {
            return new ArrayList<>();
        }

        final ArrayList<ResolvedMigration> resolvedMigrations = new ArrayList<>(resources.size());

        for (final Resource resource : resources) {
            final ResourceName resourceName = parse(resource.getFilename());
            resolvedMigrations.add(
                ResolvedMigration.forCypherScript(resource)
                                 .version(resourceName.getVersion())
                                 .description(resourceName.getDescription())
                                 .checksum(checksumOf(resource, repeatable))
                                 .equivalentChecksum(equivalentChecksumOf(resource, repeatable))
                                 .build()
                                  );
        }
        return resolvedMigrations;
    }

    private Integer checksumOf(final Resource resource, final boolean repeatable)
    {
        if (repeatable) {
            return this.checksumCalculator.calculateChecksumForResource(this.renderer.makeResource(resource));
        }
        return this.checksumCalculator.calculateChecksumForResource(resource);
    }

    private Integer equivalentChecksumOf(final Resource resource, final boolean repeatable)
    {
        if (repeatable) {
            return this.checksumCalculator.calculateChecksumForResource(resource);
        }
        return null;
    }
}
