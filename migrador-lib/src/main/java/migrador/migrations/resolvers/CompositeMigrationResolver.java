/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.migrations.resolvers;

import migrador.migrations.ErrorCode;
import migrador.migrations.Version;

import java.util.List;

/**
 * Facility for retrieving and sorting the available migrations from the classpath through the various migration
 * resolvers.
 */
class CompositeMigrationResolver extends MigrationResolver
{
    /**
     * The migration resolvers to use internally.
     */
    private final List<MigrationResolver> migrationResolvers;

    /**
     * The available migrations, sorted by version, newest first. An empty list is returned when no migrations can be
     * found.
     */
    private transient List<ResolvedMigration> availableMigrations;

    CompositeMigrationResolver(final List<MigrationResolver> migrationResolvers) {this.migrationResolvers = migrationResolvers;}

    /**
     * Finds all available migrations using all migration resolvers (sql, java, ...).
     *
     * @return The available migrations, sorted by version, oldest first. An empty list is returned when no migrations
     * can be found.
     */
    @Override public List<ResolvedMigration> resolveMigrations()
    {
        if (null == this.availableMigrations) {
            this.availableMigrations = findAvailableMigrations();
        }
        return this.availableMigrations;
    }

    /**
     * Finds all available migrations using all migration resolvers (sql, java, ...).
     *
     * @return The available migrations, sorted by version, oldest first. An empty list is returned when no migrations
     * can be found.
     */
    private List<ResolvedMigration> findAvailableMigrations()
    {
        final var migrations = new java.util.ArrayList<>(collectMigrations());
        migrations.sort(ResolvedMigrationComparator.INSTANCE);
        checkForIncompatibilities(migrations);
        return List.copyOf(migrations);
    }

    /**
     * Collects all the migrations for all migration resolvers.
     *
     * @return All migrations.
     */
    java.util.Collection<ResolvedMigration> collectMigrations()
    {
        final var migrations = new java.util.HashSet<ResolvedMigration>();
        for (final MigrationResolver migrationResolver : this.migrationResolvers) {
            migrations.addAll(migrationResolver.resolveMigrations());
        }
        return migrations;
    }

    /**
     * Checks for incompatible migrations.
     *
     * @param migrations The migrations to check.
     * @throws Version.Duplicated when two different migration with the same version number are found.
     */
    void checkForIncompatibilities(final List<ResolvedMigration> migrations)
    {
        final ResolvedMigrationComparator comparator = ResolvedMigrationComparator.INSTANCE;
        // check for more than one migration with same version
        for (int i = 0; i < migrations.size() - 1; i++) {
            final ResolvedMigration current = migrations.get(i);
            final ResolvedMigration next = migrations.get(i + 1);
            if (0 == comparator.compare(current, next)) {
                if (!current.getVersion().isNone()) {
                    throw new Version.Duplicated(current, next, ErrorCode.DUPLICATE_VERSIONED_MIGRATION);
                } else {
                    throw new Version.Duplicated(current, next, ErrorCode.DUPLICATE_REPEATABLE_MIGRATION);
                }
            }
        }
    }
}
