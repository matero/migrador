/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.migrations.resolvers;

import migrador.migrations.Version;
import migrador.resources.ResourceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class NameInterpreter extends MigrationNameBaseListener
{
    private static final Logger LOG = LoggerFactory.getLogger(NameInterpreter.class);

    private String prefix;
    private String description;
    private Version version;
    private ResourceName resourceName;

    NameInterpreter() {this(null, null, null, null);}

    NameInterpreter(final String prefix, final String description, final Version version, final ResourceName resourceName)
    {
        this.prefix = prefix;
        this.description = description;
        this.version = version;
        this.resourceName = resourceName;
    }

    void reset()
    {
        this.prefix = null;
        this.description = null;
        this.version = null;
        this.resourceName = null;
    }

    ResourceName getResourceName()
    {
        return this.resourceName;
    }

    @Override public void exitMigrationName(final MigrationNameParser.MigrationNameContext ctx)
    {
        this.resourceName = new ResourceName(this.prefix, this.version, null == this.description ? "" : this.description);
        LOG.debug("ResourceName for {} correctly interpreted as: {}", ctx.getText(), this.resourceName);
    }

    @Override public void exitVersioned(final MigrationNameParser.VersionedContext ctx)
    {
        LOG.trace("Versioned resource name recognized");
        this.prefix = "V";
    }

    @Override public void exitRepeatable(final MigrationNameParser.RepeatableContext ctx)
    {
        LOG.trace("Repeatable resource name recognized");
        this.prefix = "R";
        this.version = Version.none();
    }

    @Override public void exitVersion(final MigrationNameParser.VersionContext ctx)
    {
        final String value = ctx.getText().substring(1);
        LOG.trace("Version {} recognized", value);
        this.version = Version.make(value);
    }

    @Override public void exitDescription(final MigrationNameParser.DescriptionContext ctx)
    {
        final String text = ctx.getText();
        if (text.startsWith("__")) {
            this.description = text.substring(2);
        } else {
            this.description = text;
        }
        LOG.trace("Description {} recognized", this.description);
    }
}
