/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.migrations.resolvers;

import migrador.helpers.Classes;
import migrador.migrations.JavaMigration;
import migrador.resources.ClassProvider;
import migrador.resources.ResourceName;
import migrador.resources.ResourceProvider;
import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.util.List;
import java.util.Objects;

/**
 * Resolves available migrations. This interface can be implemented to create custom resolvers. A custom resolver
 * can be used to create additional types of migrations not covered by the standard resolvers (jdbc, sql, spring-jdbc).
 * Using the skipDefaultResolvers configuration property, the built-in resolvers can also be completely replaced.
 */
public abstract class MigrationResolver
{
    protected MigrationResolver()
    {
        // nothing to do
    }

    public abstract List<ResolvedMigration> resolveMigrations();

    public static Builder composite() {return new Builder();}

    public static final class Builder
    {
        private boolean skipDefaultResolvers;
        private ResourceProvider resourceProvider;
        private ClassProvider classProvider;
        private List<MigrationResolver> customMigrationResolvers = List.of();
        private NameInterpreter nameInterpreter;
        private ChecksumCalculator checksumCalculator;
        private CypherRenderer renderer;
        private Classes classes;
        private List<JavaMigration> fixedJavaMigrations = List.of();

        Builder() {/* nothing to do */}

        public Builder skipDefaultResolvers() {return skipDefaultResolvers(true);}

        public Builder dontSkipDefaultResolvers() {return skipDefaultResolvers(false);}

        public Builder skipDefaultResolvers(final boolean flag)
        {
            this.skipDefaultResolvers = flag;
            return this;
        }

        public Builder resourceProvider(final ResourceProvider value)
        {
            this.resourceProvider = value;
            return this;
        }

        public Builder classProvider(final ClassProvider value)
        {
            this.classProvider = value;
            return this;
        }

        public Builder customMigrationResolvers(final List<MigrationResolver> values)
        {
            this.customMigrationResolvers = (null == values) ? List.of() : values;
            return this;
        }

        public Builder fixedJavaMigrations(final List<JavaMigration> values)
        {
            this.fixedJavaMigrations = (null == values) ? List.of() : values;
            return this;
        }

        public Builder resourceNameInterpreter(final ResourceProvider value)
        {
            this.resourceProvider = value;
            return this;
        }

        public Builder checksumCalculator(final ChecksumCalculator value)
        {
            this.checksumCalculator = value;
            return this;
        }

        public Builder renderer(final CypherRenderer value)
        {
            this.renderer = value;
            return this;
        }

        public Builder classes(final Classes value)
        {
            this.classes = value;
            return this;
        }

        public CompositeMigrationResolver build()
        {
            final var migrationResolvers = new java.util.ArrayList<MigrationResolver>(3 + this.customMigrationResolvers.size());

            if (!this.skipDefaultResolvers) {
                Objects.requireNonNull(this.renderer, "renderer");
                Objects.requireNonNull(this.classes, "classes");
                Objects.requireNonNull(this.resourceProvider, "resourceProvider");
                Objects.requireNonNull(this.classProvider, "classProvider");

                if (null == this.nameInterpreter) {
                    this.nameInterpreter = new NameInterpreter();
                }
                if (null == this.checksumCalculator) {
                    this.checksumCalculator = DefaultChecksumCalculator.INSTANCE;
                }

                migrationResolvers.add(new CypherMigrationResolver(
                    this.nameInterpreter,
                    this.resourceProvider,
                    this.renderer,
                    this.checksumCalculator));
                migrationResolvers.add(new JavaMigrationResolver(this.classes, this.classProvider));
            }

            if (!this.fixedJavaMigrations.isEmpty()) {
                Objects.requireNonNull(this.classes, "classes");
                migrationResolvers.add(new FixedJavaMigrationResolver(this.fixedJavaMigrations, this.classes));
            }

            migrationResolvers.addAll(this.customMigrationResolvers);

            return new CompositeMigrationResolver(List.copyOf(migrationResolvers));
        }
    }

    public static abstract class WithNameInterpreter extends MigrationResolver
    {

        private final NameInterpreter nameInterpreter;

        protected WithNameInterpreter(final NameInterpreter nameInterpreter)
        {
            this.nameInterpreter = nameInterpreter;
        }

        protected ResourceName parse(final String resourceName)
        {
            this.nameInterpreter.reset();

            final MigrationNameLexer lexer = new MigrationNameLexer(CharStreams.fromString(resourceName));
            final MigrationNameParser parser = new MigrationNameParser(new CommonTokenStream(lexer));
            parser.addErrorListener(getErrorListener(resourceName));
            parser.addParseListener(this.nameInterpreter);

            parser.migrationName();

            return this.nameInterpreter.getResourceName();
        }

        protected abstract ANTLRErrorListener getErrorListener(String resourceName);
    }
}
