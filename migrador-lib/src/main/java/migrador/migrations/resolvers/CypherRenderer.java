/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.migrations.resolvers;

import migrador.MigradorConfig;
import migrador.resources.Resource;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class CypherRenderer
{
    CypherRenderer()
    {
        // nothing to do
    }

    public abstract String render(Resource resource);

    public static CypherRenderer from(final MigradorConfig migradorConfig)
    {
        Objects.requireNonNull(migradorConfig, "migradorConfig");
        if (migradorConfig.renderCypherScriptAsTemplates()) {
            final Map<String, Object> params = migradorConfig.templateParameters();
            final Map<String, Object> templateParameters = (null == params) ? Map.of() : Map.copyOf(params);
            return templateRenderer(migradorConfig.delimiterStart(), migradorConfig.delimiterEnd(), templateParameters);
        } else {
            return textRenderer();
        }
    }

    public static CypherRenderer textRenderer()
    {
        return TextCypherRenderer.Unique.INSTANCE;
    }

    public static CypherRenderer templateRenderer(
        final char delimiterStartChar,
        final char delimiterStopChar,
        final Map<String, Object> templateParameters)
    {
        return new StringTemplateCypherRenderer(delimiterStartChar, delimiterStopChar, templateParameters);
    }

    protected final String contentOf(final Resource resource)
    {
        try (final var resourceReader = new BufferedReader(resource.read())) {
            return resourceReader.lines().collect(Collectors.joining(System.lineSeparator()));
        } catch (final IOException e) {
            throw new Resource.NotReadable(resource, e);
        }
    }

    public abstract Resource makeResource(Resource resource);


    private static final class StringTemplateCypherRenderer extends CypherRenderer
    {
        private final STGroup definitions;
        private final Map<String, Object> parameters;

        private StringTemplateCypherRenderer(
            final char delimiterStartChar,
            final char delimiterStopChar,
            final Map<String, Object> templateParameters
                                            )
        {
            this.definitions = new STGroup(delimiterStartChar, delimiterStopChar);
            this.parameters = templateParameters;
        }

        @Override public String render(final Resource resource)
        {
            final ST template = new ST(this.definitions, contentOf(resource));
            this.parameters.forEach(template::add);
            return template.render();
        }

        @Override public Resource makeResource(final Resource resource)
        {
            return new ResourceTemplate(resource, this);
        }

    }

    private static final class TextCypherRenderer extends CypherRenderer
    {
        private static final class Unique
        {
            private static final TextCypherRenderer INSTANCE = new TextCypherRenderer();
        }

        private TextCypherRenderer()
        {
            // nothing to do
        }

        @Override public String render(final Resource resource)
        {
            return contentOf(resource);
        }

        @Override public Resource makeResource(final Resource resource)
        {
            return resource;
        }

    }
}
