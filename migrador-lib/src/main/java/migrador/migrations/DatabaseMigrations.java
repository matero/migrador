/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.migrations;

import migrador.actions.Action;
import migrador.actions.GetAppliedMigrations;
import migrador.migrations.resolvers.MigrationResolver;
import migrador.migrations.resolvers.ResolvedMigration;
import org.neo4j.driver.Session;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

public class DatabaseMigrations
{
    private final MigrationResolver migrationResolver;
    private final Action<List<AppliedMigration>> getAppliedMigrations;

    /**
     * The target version up to which to retrieve the info.
     */
    private final Version targetVersion;

    /**
     * Allows migrations to be run "out of order".
     * <p>If you already have versions 1 and 3 applied, and now a version 2 is found,
     * it will be applied too instead of being ignored.</p>
     * <p>(default: {@code false})</p>
     */
    private final boolean allowOutOfOrderMigrations;
    private final boolean allowPendingMigrations;
    private final boolean allowMissingMigrations;
    private final boolean allowIgnoredMigrations;
    private final boolean allowFutureMigrations;

    private List<Migration> migrations;

    DatabaseMigrations(
        final MigrationResolver migrationResolver,
        final Version targetVersion,
        final boolean allowOutOfOrderMigrations,
        final boolean allowPendingMigrations,
        final boolean allowMissingMigrations,
        final boolean allowIgnoredMigrations,
        final boolean allowFutureMigrations)
    {
        this(migrationResolver,
             GetAppliedMigrations.action(),
             targetVersion,
             allowOutOfOrderMigrations,
             allowPendingMigrations,
             allowMissingMigrations,
             allowIgnoredMigrations,
             allowFutureMigrations);
    }

    DatabaseMigrations(
        final MigrationResolver migrationResolver,
        final Action<List<AppliedMigration>> getAppliedMigrations,
        final Version targetVersion,
        final boolean allowOutOfOrderMigrations,
        final boolean allowPendingMigrations,
        final boolean allowMissingMigrations,
        final boolean allowIgnoredMigrations,
        final boolean allowFutureMigrations)
    {
        this.migrationResolver = migrationResolver;
        this.getAppliedMigrations = getAppliedMigrations;
        this.targetVersion = targetVersion;
        this.allowOutOfOrderMigrations = allowOutOfOrderMigrations;
        this.allowPendingMigrations = allowPendingMigrations;
        this.allowMissingMigrations = allowMissingMigrations;
        this.allowIgnoredMigrations = allowIgnoredMigrations;
        this.allowFutureMigrations = allowFutureMigrations;
    }

    public Version getTargetVersion() {return this.targetVersion;}

    /**
     * Retrieves the full set of infos about applied, current and future migrations.
     *
     * @param session
     * @return The full set of infos. An empty array if none.
     */
    public synchronized List<Migration> findAll(final Session session)
    {
        if (null == this.migrations) {
            refresh(session);
        }
        return this.migrations;
    }

    /**
     * Refreshes the info about all known migrations from both the classpath and the DB.
     */
    public synchronized void refresh(final Session session)
    {
        final var resolvedMigrations = this.migrationResolver.resolveMigrations();
        final var appliedMigrations = this.getAppliedMigrations.execute(session);

        final var resolvedVersioned = new TreeMap<VersionResolved, ResolvedMigration>();
        final var resolvedRepeatable = new TreeMap<String, ResolvedMigration>();

        final InspectionContext context = new InspectionContext(this.allowOutOfOrderMigrations,
                                                                this.allowPendingMigrations,
                                                                this.allowMissingMigrations,
                                                                this.allowIgnoredMigrations,
                                                                this.allowFutureMigrations,
                                                                this.targetVersion);

        for (final var migration : resolvedMigrations) {
            if (migration.hasVersion()) {
                final Version version = migration.getVersion();
                if (0 < version.compareTo(context.lastResolved)) {
                    context.lastResolved = version;
                }
                resolvedVersioned.put(VersionResolved.notResolved(version), migration);
            } else {
                resolvedRepeatable.put(migration.getDescription(), migration);
            }
        }

        final var appliedVersioned = new ArrayList<AppliedMigrationWithAttributes>();
        final var appliedRepeatable = new ArrayList<AppliedMigration>();

        for (final var migration : appliedMigrations) {
            if (!migration.hasVersion()) {
                appliedRepeatable.add(migration);
                continue;
            }
            if (migration.hasInstallType()) {
                context.schema = migration.getVersion();
            }
            if (migration.hasBaselineType()) {
                context.baseline = migration.getVersion();
            }
            appliedVersioned.add(new AppliedMigrationWithAttributes(migration));
        }

        for (final var applied : appliedVersioned) {
            if (applied.migration.hasVersion()) {
                final Version version = applied.migration.getVersion();
                if (0 < version.compareTo(context.lastApplied)) {
                    context.lastApplied = version;
                }
            } else {
                applied.outOfOrder = true;
            }
        }

        if (this.targetVersion.isCurrent()) {
            context.target = context.lastApplied;
        }

        final var currentMigrations = new ArrayList<Migration>();
        final var pendingResolvedVersioned = new HashSet<>(resolvedVersioned.values());

        for (final var applied : appliedVersioned) {
            final var resolvedMigration = resolvedVersioned.get(VersionResolved.notResolved(applied.migration.getVersion()));
            if (null != resolvedMigration) {
                pendingResolvedVersioned.remove(resolvedMigration);
            }
            currentMigrations.add(new Migration(resolvedMigration, applied.migration, context, applied.outOfOrder));
        }

        for (final var pending : pendingResolvedVersioned) {
            currentMigrations.add(new Migration(pending, null, context, false));
        }

        for (final var migration : appliedRepeatable) {
            if (!context.latestRepeatableRuns.containsKey(migration.getDescription()) ||
                migration.getInstalledRank() > context.latestRepeatableRuns.get(migration.getDescription())) {
                context.latestRepeatableRuns.put(migration.getDescription(), migration.getInstalledRank());
            }
        }

        final var pendingResolvedRepeatable = new HashSet<>(resolvedRepeatable.values());
        for (final var appliedRepeatableMigration : appliedRepeatable) {
            final var resolvedMigration = resolvedRepeatable.get(appliedRepeatableMigration.getDescription());
            final int latestRank = context.latestRepeatableRuns.get(appliedRepeatableMigration.getDescription());
            if (null != resolvedMigration && appliedRepeatableMigration.getInstalledRank() == latestRank
                && resolvedMigration.checksumMatches(appliedRepeatableMigration.getChecksum())) {
                pendingResolvedRepeatable.remove(resolvedMigration);
            }
            currentMigrations.add(new Migration(resolvedMigration, appliedRepeatableMigration, context, false));
        }

        for (final var migration : pendingResolvedRepeatable) {
            currentMigrations.add(new Migration(migration, null, context, false));
        }

        currentMigrations.trimToSize();
        currentMigrations.sort(Migration::compareTo);

        this.migrations = List.copyOf(currentMigrations);
    }

    /**
     * Validate all migrations for consistency.
     *
     * @return The error message, or {@code null} if everything is fine.
     */
    public String validate(final Session session)
    {
        final StringBuilder builder = new StringBuilder();
        boolean hasFailures = false;

        for (final Migration migration : findAll(session)) {
            final String message = migration.validate();
            if (null != message) {
                if (hasFailures) {
                    builder.append('\n');
                } else {
                    hasFailures = true;
                }
                builder.append(message);
            }
        }
        return hasFailures ? builder.toString() : null;
    }

    static final class InspectionContext
    {
        /**
         * Whether out of order migrations are allowed.
         */
        boolean outOfOrder;

        /**
         * Whether pending migrations are allowed.
         */
        boolean pending;

        /**
         * Whether missing migrations are allowed.
         */
        boolean missing;

        /**
         * Whether ignored migrations are allowed.
         */
        boolean ignored;

        /**
         * Whether future migrations are allowed.
         */
        boolean future;

        /**
         * The migration target.
         */
        Version target;

        /**
         * The SCHEMA migration version that was applied.
         */
        Version schema = Version.empty();

        /**
         * The BASELINE migration version that was applied.
         */
        Version baseline = Version.empty();

        /**
         * The last resolved migration.
         */
        Version lastResolved = Version.none();

        /**
         * The last applied migration.
         */
        Version lastApplied = Version.none();

        Map<String, Integer> latestRepeatableRuns = new HashMap<>();

        InspectionContext(
            final boolean outOfOrder,
            final boolean pending,
            final boolean missing,
            final boolean ignored,
            final boolean future,
            final Version target)
        {
            this.outOfOrder = outOfOrder;
            this.pending = pending;
            this.missing = missing;
            this.ignored = ignored;
            this.future = future;
            this.target = target;
        }

        @Override public boolean equals(final Object o)
        {
            if (this == o) {
                return true;
            }
            if (o instanceof InspectionContext) {
                final InspectionContext other = (InspectionContext) o;

                return this.outOfOrder == other.outOfOrder &&
                       this.pending == other.pending &&
                       this.missing == other.missing &&
                       this.ignored == other.ignored &&
                       this.future == other.future &&
                       this.target.equals(other.target) &&
                       this.schema.equals(other.schema) &&
                       this.baseline.equals(other.baseline) &&
                       this.lastResolved.equals(other.lastResolved) &&
                       this.lastApplied.equals(other.lastApplied) &&
                       this.latestRepeatableRuns.equals(other.latestRepeatableRuns);
            }
            return false;
        }

        @Override public int hashCode()
        {
            int result = Boolean.hashCode(this.outOfOrder);
            result = 31 * result + Boolean.hashCode(this.pending);
            result = 31 * result + Boolean.hashCode(this.missing);
            result = 31 * result + Boolean.hashCode(this.ignored);
            result = 31 * result + Boolean.hashCode(this.future);
            result = 31 * result + this.target.hashCode();
            result = 31 * result + this.schema.hashCode();
            result = 31 * result + this.baseline.hashCode();
            result = 31 * result + this.lastResolved.hashCode();
            result = 31 * result + this.lastApplied.hashCode();
            result = 31 * result + this.latestRepeatableRuns.hashCode();
            return result;
        }
    }

    static class VersionResolved implements Comparable<VersionResolved>
    {
        final Version version;
        final boolean resolved;

        VersionResolved(final Version version, final boolean resolved)
        {
            this.version = version;
            this.resolved = resolved;
        }

        @Override public int compareTo(final VersionResolved other)
        {
            final int versionCmp = this.version.compareTo(other.version);
            if (0 != versionCmp) {
                return versionCmp;
            }
            return Boolean.compare(this.resolved, other.resolved);
        }

        static VersionResolved notResolved(final Version version)
        {
            return new VersionResolved(version, false);
        }

        static VersionResolved resolved(final Version version)
        {
            return new VersionResolved(version, true);
        }
    }

    static final class AppliedMigrationWithAttributes
    {
        AppliedMigration migration;
        boolean outOfOrder;

        public AppliedMigrationWithAttributes(final AppliedMigration migration)
        {
            this(migration, false);
        }

        public AppliedMigrationWithAttributes(final AppliedMigration migration, final boolean outOfOrder)
        {
            this.migration = migration;
            this.outOfOrder = outOfOrder;
        }
    }

    public static final class Output implements Iterable<MigrationOutput>
    {
        private final String migradorVersion;
        private final String database;
        private final String databaseVersion;
        private final String databaseName;
        private final List<MigrationOutput> migrations;

        private Output(
            final String migradorVersion,
            final String database,
            final String databaseVersion,
            final String databaseName,
            final List<MigrationOutput> migrations
                      )
        {
            this.migradorVersion = migradorVersion;
            this.database = database;
            this.databaseVersion = databaseVersion;
            this.databaseName = databaseName;
            this.migrations = migrations;
        }

        public String getMigradorVersion() {return this.migradorVersion;}

        public String getDatabase() {return this.database;}

        public String getDatabaseVersion() {return this.databaseVersion;}

        public String getDatabaseName() {return this.databaseName;}

        public List<MigrationOutput> getMigrations() {return this.migrations;}

        @Override public Iterator<MigrationOutput> iterator() {return this.migrations.iterator();}

        @Override public String toString()
        {
            return "Info(" +
                   "migradorVersion='" + this.migradorVersion +
                   "', database='" + this.database +
                   "', schemaVersion='" + this.databaseVersion +
                   "', schemaName='" + this.databaseName +
                   "', migrations=" + this.migrations +
                   ')';
        }
    }

    public static DatabaseMigrationsBuilder of(final Version targetVersion)
    {
        Objects.requireNonNull(targetVersion, "targetVersion is required");
        return new DatabaseMigrationsBuilder(targetVersion);
    }

    public static final class DatabaseMigrationsBuilder
    {
        private final Version targetVersion;
        private boolean allowOutOfOrderMigrations;
        private MigrationResolver migrationResolver;
        private boolean allowPendingMigrations;
        private boolean allowMissingMigrations;
        private boolean allowIgnoredMigrations;
        private boolean allowFutureMigrations;

        DatabaseMigrationsBuilder(final Version targetVersion) {this.targetVersion = targetVersion;}

        public DatabaseMigrationsBuilder migrationResolver(final MigrationResolver.Builder valueBuilder)
        {
            Objects.requireNonNull(valueBuilder, "valueBuilder is required");
            return migrationResolver(valueBuilder.build());
        }

        public DatabaseMigrationsBuilder migrationResolver(final MigrationResolver value)
        {
            this.migrationResolver = value;
            return this;
        }

        public DatabaseMigrationsBuilder allowOutOfOrderMigrations()
        {
            return allowOutOfOrderMigrations(true);
        }

        public DatabaseMigrationsBuilder ignoreOutOfOrderMigrations()
        {
            return allowOutOfOrderMigrations(false);
        }

        public DatabaseMigrationsBuilder allowOutOfOrderMigrations(final boolean value)
        {
            this.allowOutOfOrderMigrations = value;
            return this;
        }

        public DatabaseMigrationsBuilder allowPendingMigrations()
        {
            return allowPendingMigrations(true);
        }

        public DatabaseMigrationsBuilder ignorePendingMigrations()
        {
            return allowPendingMigrations(false);
        }

        public DatabaseMigrationsBuilder allowPendingMigrations(final boolean value)
        {
            this.allowPendingMigrations = value;
            return this;
        }

        public DatabaseMigrationsBuilder allowMissingMigrations()
        {
            return allowMissingMigrations(true);
        }

        public DatabaseMigrationsBuilder ignoreMissingMigrations()
        {
            return allowMissingMigrations(false);
        }

        public DatabaseMigrationsBuilder allowMissingMigrations(final boolean value)
        {
            this.allowMissingMigrations = value;
            return this;
        }

        public DatabaseMigrationsBuilder allowIgnoredMigrations()
        {
            return allowIgnoredMigrations(true);
        }

        public DatabaseMigrationsBuilder ignoreIgnoredMigrations()
        {
            return allowIgnoredMigrations(false);
        }

        public DatabaseMigrationsBuilder allowIgnoredMigrations(final boolean value)
        {
            this.allowIgnoredMigrations = value;
            return this;
        }

        public DatabaseMigrationsBuilder allowFutureMigrations()
        {
            return allowFutureMigrations(true);
        }

        public DatabaseMigrationsBuilder ignoreFutureMigrations()
        {
            return allowFutureMigrations(false);
        }

        public DatabaseMigrationsBuilder allowFutureMigrations(final boolean value)
        {
            this.allowFutureMigrations = value;
            return this;
        }

        public DatabaseMigrations build()
        {
            Objects.requireNonNull(this.migrationResolver, "migrationResolver is required");
            return new DatabaseMigrations(
                this.migrationResolver,
                this.targetVersion,
                this.allowOutOfOrderMigrations,
                this.allowPendingMigrations,
                this.allowMissingMigrations,
                this.allowIgnoredMigrations,
                this.allowFutureMigrations);
        }
    }
}
