/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.migrations;

public interface Label
{
    String MIGRADOR_NAME = "Migrador";
    String APPLIED_NAME = "Applied";
    String JAVA_NAME = "Java";
    String CYPHER_NAME = "Cypher";
    String INSTALL_NAME = "Install";
    String BASELINE_NAME = "Baseline";

    String MIGRADOR = ':' + MIGRADOR_NAME;
    String APPLIED = ':' + APPLIED_NAME;
    String JAVA = ':' + JAVA_NAME;
    String CYPHER = ':' + CYPHER_NAME;
    String INSTALL = ':' + INSTALL_NAME;
    String BASELINE = ':' + BASELINE_NAME;

    String APPLIED_MIGRATION = MIGRADOR + APPLIED;
}
