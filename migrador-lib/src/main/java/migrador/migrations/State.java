/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador.migrations;

public enum State
{
    /**
     * This migration has not been applied yet.
     */
    PENDING("Pending", true, false, false),

    /**
     * This migration has not been applied yet, and won't be applied because target is set to a lower version.
     */
    ABOVE_TARGET("Above Target", true, false, false),

    /**
     * This migration was not applied against this DB, because the schema history table was baselined with a higher version.
     */
    BELOW_BASELINE("Below Baseline", true, false, false),

    /**
     * This migration has baselined this DB.
     */
    BASELINE("Baseline", true, true, false),

    /**
     * <p>This usually indicates a problem.</p>
     * <p>
     * This migration was not applied against this DB, because a migration with a higher version has already been
     * applied. This probably means some checkins happened out of order.
     * </p>
     * <p>Fix by increasing the version number, run clean and migrate again or rerun migration with outOfOrder enabled.</p>
     */
    IGNORED("Ignored", true, false, false),

    /**
     * <p>This migration succeeded.</p>
     * <p>
     * This migration was applied against this DB, but it is not available locally.
     * This usually results from multiple older migration files being consolidated into a single one.
     * </p>
     */
    MISSING_SUCCESS("Missing", false, true, false),

    /**
     * <p>This migration failed.</p>
     * <p>
     * This migration was applied against this DB, but it is not available locally.
     * This usually results from multiple older migration files being consolidated into a single one.
     * </p>
     * <p>This should rarely, if ever, occur in practice.</p>
     */
    MISSING_FAILED("Failed (Missing)", false, true, true),

    /**
     * This migration succeeded.
     */
    SUCCESS("Success", true, true, false),

    /**
     * This versioned migration succeeded, but has since been undone.
     */
    UNDONE("Undone", true, true, false),

    /**
     * This undo migration is ready to be applied if desired.
     */
    AVAILABLE("Available", true, false, false),

    /**
     * This migration failed.
     */
    FAILED("Failed", true, true, true),

    /**
     * <p>This migration succeeded.</p>
     * <p>
     * This migration succeeded, but it was applied out of order.
     * Rerunning the entire migration history might produce different results!
     * </p>
     */
    OUT_OF_ORDER("Out of Order", true, true, false),

    /**
     * <p>This migration succeeded.</p>
     * <p>
     * This migration has been applied against the DB, but it is not available locally.
     * Its version is higher than the highest version available locally.
     * It was most likely successfully installed by a future version of this deployable.
     * </p>
     */
    FUTURE_SUCCESS("Future", false, true, false),

    /**
     * <p>This migration failed.</p>
     * <p>
     * This migration has been applied against the DB, but it is not available locally.
     * Its version is higher than the highest version available locally.
     * It most likely failed during the installation of a future version of this deployable.
     * </p>
     */
    FUTURE_FAILED("Failed (Future)", false, true, true),

    /**
     * This is a repeatable migration that is outdated and should be re-applied.
     */
    OUTDATED("Outdated", true, true, false),

    /**
     * This is a repeatable migration that is outdated and has already been superseded by a newer run.
     */
    SUPERSEDED("Superseded", true, true, false);

    /**
     * The name suitable for display to the end-user.
     */
    private final String displayName;

    /**
     * Flag indicating if this migration is available on the classpath or not.
     */
    private final boolean resolved;

    /**
     * Flag indicating if this migration has been applied or not.
     */
    private final boolean applied;

    /**
     * Flag indicating if this migration has failed when it was applied or not.
     */
    private final boolean failed;

    /**
     * Creates a new MigrationState.
     *
     * @param displayName The name suitable for display to the end-user.
     * @param resolved    Flag indicating if this migration is available on the classpath or not.
     * @param applied     Flag indicating if this migration has been applied or not.
     * @param failed      Flag indicating if this migration has failed when it was applied or not.
     */
    State(final String displayName, final boolean resolved, final boolean applied, final boolean failed)
    {
        this.displayName = displayName;
        this.resolved = resolved;
        this.applied = applied;
        this.failed = failed;
    }

    public String getDisplayName() {return this.displayName;}

    public boolean isResolved() {return this.resolved;}

    public boolean isApplied() {return this.applied;}

    public boolean isFailed() {return this.failed;}
}
