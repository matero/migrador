/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.migrations;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * A migration applied to the database (maps to a row in the schema history table).
 */
public final class AppliedMigration implements Comparable<AppliedMigration>
{
    /**
     * The order in which this migration was applied amongst all others. (For out of order detection)
     */
    private final int installedRank;

    /**
     * The target version of this migration. {@link Version#none()} if it is a repeatable migration.
     */
    private final Version version;

    /**
     * The description of the migration.
     */
    private final String description;

    /**
     * The type of migration (BASELINE, SQL, ...)
     */
    private final Type type;

    /**
     * The name of the script to execute for this migration, relative to its classpath location.
     */
    private final String script;

    /**
     * The checksum of the migration. (Optional)
     */
    private final Integer checksum;

    /**
     * The timestamp when this migration was installed.
     */
    private final LocalDateTime installedOn;

    /**
     * The user that installed this migration.
     */
    private final String installedBy;

    /**
     * The execution time of this migration.
     */
    private final long executionTime;

    /**
     * Flag indicating whether the migration was successful or not.
     */
    private final boolean success;

    AppliedMigration(
        final int installedRank,
        final Version version,
        final String description,
        final Type type,
        final String script,
        final Integer checksum,
        final LocalDateTime installedOn,
        final String installedBy,
        final long executionTime,
        final boolean success)
    {
        this.installedRank = installedRank;
        this.version = version;
        this.description = description;
        this.type = type;
        this.script = script;
        this.checksum = checksum;
        this.installedOn = installedOn;
        this.installedBy = installedBy;
        this.executionTime = executionTime;
        this.success = success;
    }

    public int getInstalledRank()
    {
        return this.installedRank;
    }

    public Version getVersion()
    {
        return this.version;
    }

    public String getDescription()
    {
        return this.description;
    }

    public Type getType()
    {
        return this.type;
    }

    public String getScript()
    {
        return this.script;
    }

    public Integer getChecksum()
    {
        return this.checksum;
    }

    public LocalDateTime getInstalledOn()
    {
        return this.installedOn;
    }

    public String getInstalledBy()
    {
        return this.installedBy;
    }

    public long getExecutionTime()
    {
        return this.executionTime;
    }

    public boolean isSuccess()
    {
        return this.success;
    }

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof AppliedMigration) {
            final AppliedMigration other = (AppliedMigration) o;

            return this.installedRank == other.installedRank &&
                   this.success == other.success &&
                   this.type == other.type &&
                   this.executionTime == other.executionTime &&
                   this.version.equals(other.version) &&
                   this.description.equals(other.description) &&
                   this.script.equals(other.script) &&
                   this.installedOn.equals(other.installedOn) &&
                   this.installedBy.equals(other.installedBy) &&
                   Objects.equals(this.checksum, other.checksum);
        }
        return false;
    }

    @Override public int hashCode()
    {
        int result = this.installedRank;
        result = 31 * result + this.version.hashCode();
        result = 31 * result + this.description.hashCode();
        result = 31 * result + this.type.hashCode();
        result = 31 * result + this.script.hashCode();
        result = 31 * result + Objects.hashCode(this.checksum);
        result = 31 * result + this.installedOn.hashCode();
        result = 31 * result + this.installedBy.hashCode();
        result = 31 * result + Long.hashCode(this.executionTime);
        result = 31 * result + Boolean.hashCode(this.success);
        return result;
    }

    @Override public String toString()
    {
        return "AppliedMigration(installedRank=" + this.installedRank +
               ", version=" + this.version +
               ", description='" + this.description +
               "', type=" + this.type +
               ", script='" + this.script +
               "', checksum=" + this.checksum +
               ", installedOn=" + this.installedOn +
               ", installedBy='" + this.installedBy +
               "', execution=" + this.executionTime +
               ", success=" + this.success + ')';
    }

    @Override public int compareTo(final AppliedMigration o)
    {
        return this.installedRank - o.installedRank;
    }

    public static AppliedMigrationBuilder ranked(final int installedRank)
    {
        if (0 > installedRank) {
            throw new IllegalArgumentException("installedRank must be zero or positive");
        }
        return new AppliedMigrationBuilder(installedRank);
    }

    public boolean hasBaselineType()
    {
        return Type.BASELINE == this.type;
    }

    public boolean hasVersion()
    {
        return !getVersion().isNone();
    }

    public boolean hasSyntheticType()
    {
        return this.type.isSynthetic();
    }

    public boolean isInstall()
    {
        return this.type.isInstall();
    }

    public String getNodeLabels()
    {
        return this.type.getLabels();
    }

    public boolean hasInstallType()
    {
        return Type.INSTALL == this.type;
    }

    public static final class AppliedMigrationBuilder
    {
        private final int installedRank;
        private Version version;
        private String description;
        private Type type;
        private String script;
        private Integer checksum;
        private LocalDateTime installedOn;
        private String installedBy;
        private long executionTime;
        private boolean success;

        AppliedMigrationBuilder(final int installedRank)
        {
            this.installedRank = installedRank;
        }

        public AppliedMigrationBuilder version(final String value)
        {
            return version(Version.make(value));
        }

        public AppliedMigrationBuilder version(final Version value)
        {
            this.version = value;
            return this;
        }

        public AppliedMigrationBuilder description(final String value)
        {
            this.description = value;
            return this;
        }

        public AppliedMigrationBuilder type(final String value)
        {
            return type(Type.valueOf(value));
        }

        public AppliedMigrationBuilder type(final Type value)
        {
            this.type = value;
            return this;
        }

        public AppliedMigrationBuilder script(final String value)
        {
            this.script = value;
            return this;
        }

        public AppliedMigrationBuilder checksum(final Integer value)
        {
            this.checksum = value;
            return this;
        }

        public AppliedMigrationBuilder installedOn(final LocalDateTime value)
        {
            this.installedOn = value;
            return this;
        }

        public AppliedMigrationBuilder installedBy(final String value)
        {
            this.installedBy = value;
            return this;
        }

        public AppliedMigrationBuilder executionTime(final long valueInMillis)
        {
            this.executionTime = valueInMillis;
            return this;
        }

        public AppliedMigrationBuilder success(final boolean value)
        {
            this.success = value;
            return this;
        }

        public AppliedMigration create()
        {
            return new AppliedMigration(
                this.installedRank,
                Objects.requireNonNull(this.version, "version is required"),
                this.description,
                Objects.requireNonNull(this.type, "type is required"),
                Objects.requireNonNull(this.script, "script is required"),
                this.checksum,
                Objects.requireNonNull(this.installedOn, "installedOn is required"),
                Objects.requireNonNull(this.installedBy, "installedBy is required"),
                this.executionTime,
                this.success);
        }
    }
}
