/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.migrations;

import migrador.MigradorException;
import migrador.migrations.resolvers.ResolvedMigration;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public final class Version implements Comparable<Version>
{

    private static final class Unique
    {
        private static final Version EMPTY_VERSION = new Version("");

        private static final Version LATEST_VERSION = new Version(-1, "<< Latest Version >>");

        /**
         * Current version. Only a marker. For the real version use Migrador.info().current() instead.
         */
        private static final Version CURRENT_VERSION = new Version(-2, "<< Current Version >>");
        /**
         * NO version. Only a marker to use instead of {@code null}.
         */
        private static final Version NONE_VERSION = new Version(Integer.MIN_VALUE, "");

        /**
         * Regex for matching proper version format
         */
        private static final Pattern SPLIT_REGEX = Pattern.compile("\\.(?=\\d)");
    }

    /**
     * The individual parts this version string is composed of. Ex. 1.2.3.4.0 -> [1, 2, 3, 4, 0]
     */
    private final List<BigInteger> parts;

    /**
     * The printable text to represent the version.
     */
    private final String displayText;

    Version(final String displayText) {this(List.of(), displayText);}

    Version(final int part, final String displayText) {this(List.of(BigInteger.valueOf(part)), displayText);}

    Version(final List<BigInteger> parts, final String displayText)
    {
        this.parts = parts;
        this.displayText = displayText;
    }

    public static Version none() {return Unique.NONE_VERSION;}

    public static Version empty() {return Unique.EMPTY_VERSION;}

    public static Version latest() {return Unique.LATEST_VERSION;}

    /**
     * @return The textual representation of the version.
     */
    @Override public String toString() {return this.displayText;}

    /**
     * @return Numeric version as String
     */
    public String getVersion()
    {
        if (equals(Unique.EMPTY_VERSION)) {
            return "";
        }
        if (equals(Unique.NONE_VERSION)) {
            return "";
        }
        if (equals(Unique.LATEST_VERSION)) {
            return Long.toString(Long.MAX_VALUE);
        }
        return this.displayText;
    }

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof Version) {
            final Version other = (Version) o;
            return 0 == compareTo(other);
        }
        return false;
    }

    @Override public int hashCode() {return this.parts.hashCode();}

    /**
     * Convenience method for quickly checking whether this version is at least as new as this other version.
     *
     * @param otherVersion The other version.
     * @return {@code true} if this version is equal or newer, {@code false} if it is older.
     */
    public boolean isAtLeast(final String otherVersion) {return 0 <= compareTo(Version.make(otherVersion));}

    /**
     * Convenience method for quickly checking whether this version is newer than this other version.
     *
     * @param otherVersion The other version.
     * @return {@code true} if this version is newer, {@code false} if it is not.
     */
    public boolean isNewerThan(final String otherVersion) {return 0 < compareTo(Version.make(otherVersion));}

    /**
     * Convenience method for quickly checking whether this major version is newer than this other major version.
     *
     * @param otherVersion The other version.
     * @return {@code true} if this major version is newer, {@code false} if it is not.
     */
    public boolean isMajorNewerThan(final String otherVersion) {return 0 < getMajor().compareTo(Version.make(otherVersion).getMajor());}

    /**
     * @return The major version.
     */
    public BigInteger getMajor() {return this.parts.get(0);}

    /**
     * @return The major version as a string.
     */
    public String getMajorAsString() {return this.parts.get(0).toString();}

    /**
     * @return The minor version as a string.
     */
    public String getMinorAsString()
    {
        if (1 == this.parts.size()) {
            return "0";
        }
        return this.parts.get(1).toString();
    }

    public boolean isLatest() {return Unique.LATEST_VERSION.equals(this);}

    public boolean isCurrent() {return Unique.CURRENT_VERSION.equals(this);}

    public boolean isEmpty() {return Unique.EMPTY_VERSION.equals(this);}

    public boolean isNone() {return Unique.NONE_VERSION.equals(this);}

    public boolean isAbstract()
    {
        return isLatest() || isCurrent() || isEmpty() || isNone();
    }

    @Override public int compareTo(final Version other)
    {
        if (null == other) {
            return 1;
        }

        if (this == Unique.EMPTY_VERSION) {
            if (other == Unique.EMPTY_VERSION) {
                return 0;
            } else {
                return -1;
            }
        }

        if (this == Unique.CURRENT_VERSION) {
            return other == Unique.CURRENT_VERSION ? 0 : -1;
        }

        if (this == Unique.LATEST_VERSION) {
            if (other == Unique.LATEST_VERSION) {
                return 0;
            } else {
                return 1;
            }
        }

        if (other == Unique.EMPTY_VERSION) {
            return 1;
        }

        if (other == Unique.CURRENT_VERSION) {
            return 1;
        }

        if (other == Unique.LATEST_VERSION) {
            return -1;
        }

        final int largestNumberOfParts = Math.max(this.parts.size(), other.parts.size());
        for (int i = 0; i < largestNumberOfParts; i++) {
            final int compared = getOrZero(i).compareTo(other.getOrZero(i));
            if (0 != compared) {
                return compared;
            }
        }
        return 0;
    }

    private BigInteger getOrZero(final int i) {return i < this.parts.size() ? this.parts.get(i) : BigInteger.ZERO;}

    /**
     * Create a MigrationVersion from a value String.
     *
     * @param value The value String. The value {@code current} will be interpreted as MigrationVersion.CURRENT,
     *              a marker for the latest value that has been applied to the database.
     * @return The MigrationVersion
     */
    public static Version make(final String value)
    {
        if (null == value) {
            return Unique.NONE_VERSION;
        }
        if (isEmpty(value)) {
            return Unique.EMPTY_VERSION;
        }
        if (isCurrent(value)) {
            return Unique.CURRENT_VERSION;
        }
        if (isLatest(value)) {
            return Unique.LATEST_VERSION;
        }
        return makeVersionFrom(value);
    }

    private static boolean isEmpty(final String version) {return "".equals(version);}

    private static boolean isCurrent(final String version) {return "current".equalsIgnoreCase(version);}

    private static boolean isLatest(final String version)
    {
        return "latest".equalsIgnoreCase(version) || Unique.LATEST_VERSION.getVersion().equals(version);
    }

    /**
     * Creates a Version using this version string.
     *
     * @param version The version in one of the following formats: 6, 6.0, 005, 1.2.3.4, 201004200021. <br/>{@code null}
     *                means that this version refers to an empty schema.
     */
    private static Version makeVersionFrom(final String version)
    {
        final String normalizedVersion = version.replace('_', '.');
        final List<BigInteger> parts = getVersionPartsFrom(normalizedVersion);
        return new Version(parts, normalizedVersion);
    }

    /**
     * Splits this string into list of Long
     *
     * @param text The string to split.
     * @return The resulting array.
     */
    private static List<BigInteger> getVersionPartsFrom(final String text)
    {
        final var parts = new ArrayList<BigInteger>();
        for (final String part : Unique.SPLIT_REGEX.split(text)) {
            parts.add(toBigInteger(text, part));
        }

        for (int i = parts.size() - 1; 0 < i; i--) {
            if (!parts.get(i).equals(BigInteger.ZERO)) {
                break;
            }
            parts.remove(i);
        }

        return List.copyOf(parts);
    }

    private static BigInteger toBigInteger(final String version, final String part)
    {
        try {
            return new BigInteger(part);
        } catch (final NumberFormatException e) {
            throw new IllegalVersion(version);
        }
    }

    public static final class IllegalVersion extends MigradorException
    {
        private IllegalVersion(final String version)
        {
            super("Version may only contain 0..9 and . (dot). Invalid version: " + version);
        }
    }

    public static final class Duplicated extends MigradorException
    {
        public Duplicated(final ResolvedMigration current, final ResolvedMigration next, final ErrorCode errorCode)
        {
            super(String.format("Found more than one migration with version %s\nOffenders:\n-> %s (%s)\n-> %s (%s)",
                                current.getVersion(),
                                current.getPhysicalLocation(),
                                current.getType(),
                                next.getPhysicalLocation(),
                                next.getType()));
        }
    }
}
