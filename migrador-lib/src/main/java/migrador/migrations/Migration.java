/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.migrations;

import migrador.actions.ResolvedMigrationExecutor;
import migrador.migrations.resolvers.ResolvedMigration;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Migration implements Comparable<Migration>
{
    final ResolvedMigration resolvedMigration;
    final AppliedMigration appliedMigration;
    private final DatabaseMigrations.InspectionContext context;
    private final boolean outOfOrder;

    Migration(
        final ResolvedMigration resolvedMigration,
        final AppliedMigration appliedMigration,
        final DatabaseMigrations.InspectionContext context,
        final boolean outOfOrder)
    {
        this.resolvedMigration = resolvedMigration;
        this.appliedMigration = appliedMigration;
        this.context = context;
        this.outOfOrder = outOfOrder;
    }

    public Type getType()
    {
        if (null != this.appliedMigration) {
            return this.appliedMigration.getType();
        }
        return this.resolvedMigration.getType();
    }

    public Integer getChecksum()
    {
        if (null != this.appliedMigration) {
            return this.appliedMigration.getChecksum();
        }
        return this.resolvedMigration.getChecksum();
    }

    public Version getVersion()
    {
        if (null != this.appliedMigration) {
            return this.appliedMigration.getVersion();
        }
        return this.resolvedMigration.getVersion();
    }

    public String getDescription()
    {
        if (null != this.appliedMigration) {
            return this.appliedMigration.getDescription();
        }
        return this.resolvedMigration.getDescription();
    }

    public String getScript()
    {
        if (null != this.appliedMigration) {
            return this.appliedMigration.getScript();
        }
        return this.resolvedMigration.getScript();
    }

    public State getState()
    {
        if (null == this.appliedMigration) {
            if (this.resolvedMigration.hasVersion()) {
                if (0 > this.resolvedMigration.getVersion().compareTo(this.context.baseline)) {
                    return State.BELOW_BASELINE;
                }
                if (!this.context.target.isNone() && 0 < this.resolvedMigration.getVersion().compareTo(this.context.target)) {
                    return State.ABOVE_TARGET;
                }
                if ((0 > this.resolvedMigration.getVersion().compareTo(this.context.lastApplied)) && !this.context.outOfOrder) {
                    return State.IGNORED;
                }
            }
            return State.PENDING;
        }


        if (this.appliedMigration.hasInstallType()) {
            return State.SUCCESS;
        }

        if (this.appliedMigration.hasBaselineType()) {
            return State.BASELINE;
        }

        if (null == this.resolvedMigration) {
            if (0 > getVersion().compareTo(this.context.lastResolved)) {
                if (this.appliedMigration.isSuccess()) {
                    return State.MISSING_SUCCESS;
                }
                return State.MISSING_FAILED;
            } else {
                if (this.appliedMigration.isSuccess()) {
                    return State.FUTURE_SUCCESS;
                }
                return State.FUTURE_FAILED;
            }
        }

        if (!this.appliedMigration.isSuccess()) {
            return State.FAILED;
        }

        if (!this.appliedMigration.hasVersion()) {
            if (this.appliedMigration.getInstalledRank() == this.context.latestRepeatableRuns.get(this.appliedMigration.getDescription())) {
                if (this.resolvedMigration.checksumMatches(this.appliedMigration.getChecksum())) {
                    return State.SUCCESS;
                }
                return State.OUTDATED;
            }
            return State.SUPERSEDED;
        }

        if (this.outOfOrder) {
            return State.OUT_OF_ORDER;
        }
        return State.SUCCESS;
    }

    public LocalDateTime getInstalledOn()
    {
        if (null != this.appliedMigration) {
            return this.appliedMigration.getInstalledOn();
        }
        return null;
    }

    public String getInstalledBy()
    {
        if (null != this.appliedMigration) {
            return this.appliedMigration.getInstalledBy();
        }
        return null;
    }

    public Integer getInstalledRank()
    {
        if (null != this.appliedMigration) {
            return this.appliedMigration.getInstalledRank();
        }
        return null;
    }

    public long getExecutionTime()
    {
        if (null != this.appliedMigration) {
            return this.appliedMigration.getExecutionTime();
        }
        return 0;
    }

    public String getPhysicalLocation()
    {
        if (null != this.resolvedMigration) {
            return this.resolvedMigration.getPhysicalLocation();
        }
        return "";
    }

    /**
     * Validates this migrationInfo for consistency.
     *
     * @return The error message, or {@code null} if everything is fine.
     */
    public String validate()
    {
        final State state = getState();

        // Ignore any migrations above the current target as they are out of scope.
        if (State.ABOVE_TARGET == state) {
            return null;
        }

        if (state.isFailed() && (!this.context.future || State.FUTURE_FAILED != state)) {
            if (getVersion().isNone()) {
                return "Detected failed repeatable migration: " + getDescription();
            }
            return "Detected failed migration to version " + getVersion() + " (" + getDescription() + ")";
        }

        if ((null == this.resolvedMigration) &&
            !this.appliedMigration.hasSyntheticType() &&
            this.appliedMigration.hasVersion() &&
            (!this.context.missing || (State.MISSING_SUCCESS != state && State.MISSING_FAILED != state)) &&
            (!this.context.future || (State.FUTURE_SUCCESS != state && State.FUTURE_FAILED != state))) {
            return "Detected applied migration not resolved locally: " + getVersion();
        }

        if (!this.context.pending && State.PENDING == state || (!this.context.ignored && State.IGNORED == state)) {
            if (getVersion().isNone()) {
                return "Detected resolved migration not applied to database: " + getVersion();
            }
            return "Detected resolved repeatable migration not applied to database: " + getDescription();
        }

        if (!this.context.pending && State.OUTDATED == state) {
            return "Detected outdated resolved repeatable migration that should be re-applied to database: " + getDescription();
        }

        if (null != this.resolvedMigration && null != this.appliedMigration) {
            final String migrationIdentifier = this.appliedMigration.hasVersion()
                                               ? "version " + this.appliedMigration.getVersion() // Versioned migrations
                                               : this.appliedMigration.getScript(); // Repeatable migrations

            if (getVersion().isNone() || 0 < getVersion().compareTo(this.context.baseline)) {
                if (this.resolvedMigration.getType() != this.appliedMigration.getType()) {
                    return createMismatchMessage("type", migrationIdentifier, this.appliedMigration.getType(), this.resolvedMigration.getType());
                }
                if (!this.resolvedMigration.hasVersion() || pendingNotOutdatedNorSuperseded(state)) {
                    if (!this.resolvedMigration.checksumMatches(this.appliedMigration.getChecksum())) {
                        return createMismatchMessage(
                            "checksum",
                            migrationIdentifier,
                            this.appliedMigration.getChecksum(),
                            this.resolvedMigration.getChecksum());
                    }
                }
                if (descriptionMismatch(this.resolvedMigration, this.appliedMigration)) {
                    return createMismatchMessage(
                        "description",
                        migrationIdentifier,
                        this.appliedMigration.getDescription(),
                        this.resolvedMigration.getDescription());
                }
            }
        }

        return null;
    }

    private boolean pendingNotOutdatedNorSuperseded(final State state)
    {
        return this.context.pending && State.OUTDATED != state && State.SUPERSEDED != state;
    }

    private boolean descriptionMismatch(final ResolvedMigration resolvedMigration, final AppliedMigration appliedMigration)
    {
        return !resolvedMigration.getDescription().equals(appliedMigration.getDescription());
    }

    /**
     * Creates a message for a mismatch.
     *
     * @param mismatch            The type of mismatch.
     * @param migrationIdentifier The offending version.
     * @param applied             The applied value.
     * @param resolved            The resolved value.
     * @return The message.
     */
    private String createMismatchMessage(final String mismatch, final String migrationIdentifier, final Object applied, final Object resolved)
    {
        return String.format("Migration %s mismatch for migration %s\n-> Applied to database : %s\n-> Resolved locally    : %s",
                             mismatch, migrationIdentifier, applied, resolved);
    }

    @Override public int compareTo(final Migration other)
    {
        final Integer thisInstalledRank = getInstalledRank();
        final Integer otherInstalledRank = other.getInstalledRank();

        if (null != thisInstalledRank && null != otherInstalledRank) {
            return thisInstalledRank - otherInstalledRank;
        }

        final State thisState = getState();
        final State otherState = other.getState();

        // Below baseline migrations come before applied ones
        if (State.BELOW_BASELINE == thisState && otherState.isApplied()) {
            return -1;
        }
        if (thisState.isApplied() && State.BELOW_BASELINE == otherState) {
            return 1;
        }

        if (State.IGNORED == thisState && otherState.isApplied()) {
            if (!getVersion().isNone() && !other.getVersion().isNone()) {
                return getVersion().compareTo(other.getVersion());
            }
        }
        if (thisState.isApplied() && State.IGNORED == otherState) {
            if (!getVersion().isNone() && !other.getVersion().isNone()) {
                return getVersion().compareTo(other.getVersion());
            }
        }

        // Sort installed before pending
        if (null != thisInstalledRank) {
            return -1;
        }
        if (null != otherInstalledRank) {
            return 1;
        }

        // No migration installed, sort according to other criteria
        // Two versioned migrations: sort by version
        if (!getVersion().isNone() && !other.getVersion().isNone()) {
            return getVersion().compareTo(other.getVersion());
        }

        // One versioned and one repeatable migration: versioned migration goes before repeatable one
        if (!getVersion().isNone()) {
            return -1;
        }
        if (!other.getVersion().isNone()) {
            return 1;
        }

        // Two repeatable migrations: sort by description
        return getDescription().compareTo(other.getDescription());
    }

    @SuppressWarnings("SimplifiableIfStatement")
    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof Migration) {
            final Migration other = (Migration) o;
            return this.appliedMigration.equals(other.appliedMigration) &&
                   this.context.equals(other.context) &&
                   this.resolvedMigration.equals(other.resolvedMigration);
        }
        return false;
    }

    @Override public int hashCode()
    {
        int result = this.resolvedMigration.hashCode();
        result = 31 * result + this.appliedMigration.hashCode();
        result = 31 * result + this.context.hashCode();
        return result;
    }

    public boolean isVersioned() {return !getVersion().isNone();}

    public boolean wasApplied() {return getState().isApplied();}

    public boolean inPendingState() {return State.PENDING == getState();}

    public boolean isResolved() {return getState().isResolved();}

    public boolean isFailed() {return getState().isFailed();}

    public boolean isOutOfOrder() {return State.OUT_OF_ORDER == getState();}

    public boolean isInFuture()
    {
        final State state = getState();
        return State.FUTURE_SUCCESS == state || State.FUTURE_FAILED == state;
    }

    public String getCategory()
    {
        final var type = getType();
        if (null != type) {
            if (type.isSynthetic()) {
                return "";
            }
        }
        if (null == getVersion()) {
            return "Repeatable";
        }
        return "Versioned";
    }

    public String getVersionStr() {return null == getVersion() ? "" : getVersion().toString();}

    public String getTypeDisplayName()
    {
        final var type = getType();
        return null == type ? "" : type.getDisplayName();
    }

    public String getInstalledOnIdoDateTime()
    {
        final var installedOn = getInstalledOn();
        return null == installedOn ? "" : DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(installedOn);
    }

    public String getStateDisplayName()
    {
        final var state = getState();
        return null == state ? "" : state.getDisplayName();
    }

    public void migrateWith(final ResolvedMigrationExecutor executor) {this.resolvedMigration.migrateWith(executor);}
}
