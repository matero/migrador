/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador.migrations;

public enum Type
{
    INSTALL("Install", Label.APPLIED, Label.INSTALL),
    BASELINE("Baseline", Label.BASELINE),
    CYPHER("Cypher", Label.APPLIED, Label.CYPHER),
    JAVA("Java", Label.APPLIED, Label.JAVA);

    private final String labels;
    private final String displayName;

    Type(final String displayName, final String extraLabel)
    {
        this.displayName = displayName;
        this.labels = Label.MIGRADOR + extraLabel;
    }

    Type(final String displayName, final String firstExtraLabel, final String secondExtraLabel)
    {
        this.displayName = displayName;
        this.labels = Label.MIGRADOR + firstExtraLabel + secondExtraLabel;
    }

    /**
     * Whether this is a synthetic migration type, which is only ever present in the schema history table,
     * but never discovered by migration resolvers.
     *
     * @return true if and only if its {@link Type#BASELINE}.
     */
    public boolean isSynthetic() {return isBaseline() || isInstall();}

    public boolean isBaseline() {return BASELINE == this;}

    public boolean isInstall() {return INSTALL == this;}

    public String getLabels() {return this.labels;}

    public String getDisplayName() {return this.displayName;}
}
