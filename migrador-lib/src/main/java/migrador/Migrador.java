/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package migrador;

import migrador.actions.Baseline;
import migrador.actions.Clean;
import migrador.actions.CleanResult;
import migrador.actions.GetMigrationsStatus;
import migrador.actions.Migrate;
import migrador.actions.Validate;
import migrador.helpers.Classes;
import migrador.migrations.JavaMigration;
import migrador.migrations.Version;
import migrador.migrations.resolvers.CypherRenderer;
import migrador.migrations.resolvers.MigrationResolver;
import migrador.resources.ClassProvider;
import migrador.resources.ResourceProvider;
import migrador.resources.ResourceScanner;
import org.neo4j.driver.Driver;
import org.neo4j.driver.Session;
import org.neo4j.driver.exceptions.Neo4jException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Objects;

public class Migrador implements AutoCloseable
{
    public static final String NAME = "migrador";

    private static final Logger LOG = LoggerFactory.getLogger(Migrador.class);
    private static final Command[] AS_COMMANDS = {};

    private final Classes classes;
    private final CypherRenderer renderer;
    private final boolean skipDefaultResolvers;
    private final Clean clean;

    private final List<MigrationResolver> customMigrationResolvers;
    private final List<JavaMigration> fixedJavaMigrations;
    private final Driver driver;
    private final ResourceScanner scanner;
    private final Version targetVersion;
    private final boolean allowOutOfOrderMigrations;
    private final boolean allowPendingMigrations;
    private final boolean allowMissingMigrations;
    private final boolean allowIgnoredMigrations;
    private final boolean allowFutureMigrations;
    private final String username;
    private final boolean baselineOnMigrate;

    Migrador(
        final Driver driver,
        final Classes classes,
        final CypherRenderer renderer,
        final ResourceScanner scanner,
        final boolean skipDefaultResolvers,
        final Clean clean,
        final List<MigrationResolver> customMigrationResolvers,
        final List<JavaMigration> fixedJavaMigrations,
        final Version targetVersion,
        final boolean allowOutOfOrderMigrations,
        final boolean allowPendingMigrations,
        final boolean allowMissingMigrations,
        final boolean allowIgnoredMigrations,
        final boolean allowFutureMigrations,
        final String username,
        final boolean baselineOnMigrate)
    {
        this.driver = driver;
        this.classes = classes;
        this.renderer = renderer;
        this.scanner = scanner;
        this.skipDefaultResolvers = skipDefaultResolvers;
        this.clean = clean;
        this.customMigrationResolvers = customMigrationResolvers;
        this.fixedJavaMigrations = fixedJavaMigrations;
        this.targetVersion = targetVersion;
        this.allowOutOfOrderMigrations = allowOutOfOrderMigrations;
        this.allowPendingMigrations = allowPendingMigrations;
        this.allowMissingMigrations = allowMissingMigrations;
        this.allowIgnoredMigrations = allowIgnoredMigrations;
        this.allowFutureMigrations = allowFutureMigrations;
        this.username = username;
        this.baselineOnMigrate = baselineOnMigrate;
    }

    @Override public void close() {this.driver.close();}

    public void execute(final List<Command> commandsToExecute)
    {
        Objects.requireNonNull(commandsToExecute, "commandsToExecute is required");
        final var commands = commandsToExecute.toArray(AS_COMMANDS); // avoid concurrent modification with a local copy
        if (0 == commands.length) {
            throw new IllegalArgumentException("at least one command must be passed");
        }
        for (int i = 0; commands.length > i; i++) {
            if (null == commands[i]) {
                throw new NullPointerException("commandsToExecute[" + i + "] cant be null");
            }
        }

        showVersion();
        if (1 < commandsToExecute.size()) {
            LOG.info("starting sequential execution of {}.", commandsToExecute);
        }
        final var start = Instant.now();
        try (final var session = this.driver.session()) {
            for (final var command : commandsToExecute) {
                executeCommand(command, session);
            }
            if (1 < commandsToExecute.size()) {
                final var elapsedTime = Duration.between(start, Instant.now());
                LOG.info("finished sequential execution of {} after {}ms.", commandsToExecute, elapsedTime.toMillis());
            }
        } catch (final Neo4jException e) {
            LOG.error("Neo4J failure while executing command", e);
            throw e;
        } catch (final MigradorException e) {
            LOG.error("Migrador failure while executing command", e);
            throw e;
        } finally {
            showMemoryUsage();
        }
    }

    void executeCommand(final Command command, final Session session)
    {
        boolean logFailure = true;
        LOG.info("executing command '{}'", command);
        final var start = Instant.now();
        try {
            switch (command) {
                case baseline:
                    baselineDatabase(session);
                    break;
                case clean:
                    cleanDatabase(session);
                    break;
                case info: {
                    showDatabaseStatus(session);
                    break;
                }
                case migrate:
                    applyMigrations(session);
                    break;
                case repair:
                    LOG.warn("currently 'repair' is not implemented.");
                    break;
                case validate: {
                    validateDatabase(session);
                    break;
                }
                default:
                    logFailure = false; // this should never ever happen!
                    throw new IllegalStateException("unexpected command '" + command + "'.");
            }
            final var stop = Instant.now();
            LOG.info("execution of command '{}' finished after {}ms.", command, Duration.between(start, stop).toMillis());
        } catch (final RuntimeException e) {
            if (logFailure && LOG.isErrorEnabled()) {
                final var stop = Instant.now();
                LOG.error("execution of command '" + command + "' failed after " + Duration.between(start, stop).toMillis() + "ms.", e);
            }
            throw e;
        }
    }

    private Integer baselineDatabase(final Session session)
    {
        return Baseline.of(this.targetVersion).executedBy(this.username).execute(session);
    }

    private CleanResult cleanDatabase(final Session session)
    {
        return this.clean.execute(session);
    }

    private void showDatabaseStatus(final Session session)
    {
        final var status = GetMigrationsStatus.of(this.targetVersion)
                                              .migrationResolver(migrationResolverFor(Command.info))
                                              .allowOutOfOrderMigrations(this.allowOutOfOrderMigrations)
                                              .execute(session);
        final var table = MigrationsAsciiTable.of(status);
        LOG.info("\n\nSchema version: {}\n\n{}\n", status.getCurrentVersion(), table);
    }

    private void applyMigrations(final Session session)
    {
        try {
            Migrate.of(this.targetVersion)
                   .executedBy(this.username)
                   .baseline(this.baselineOnMigrate)
                   .migrationResolver(migrationResolverFor(Command.migrate))
                   .allowOutOfOrderMigrations(this.allowOutOfOrderMigrations)
                   .allowPendingMigrations(this.allowPendingMigrations)
                   .allowMissingMigrations(this.allowMissingMigrations)
                   .allowIgnoredMigrations(this.allowIgnoredMigrations)
                   .allowFutureMigrations(this.allowFutureMigrations)
                   .execute(session);
        } catch (final MigradorException e) {
            LOG.error(e.getMessage());
        }
    }

    private void validateDatabase(final Session session)
    {
        try {
            final var validation = Validate.of(this.targetVersion)
                                           .migrationResolver(migrationResolverFor(Command.validate))
                                           .allowOutOfOrderMigrations(this.allowOutOfOrderMigrations)
                                           .allowPendingMigrations(this.allowPendingMigrations)
                                           .allowMissingMigrations(this.allowMissingMigrations)
                                           .allowIgnoredMigrations(this.allowIgnoredMigrations)
                                           .allowFutureMigrations(this.allowFutureMigrations)
                                           .execute(session);
            LOG.info(validation.getMessage());
        } catch (final Validate.InvalidMigrations e) {
            LOG.error(e.getMessage());
        }
    }

    MigrationResolver migrationResolverFor(final Command command)
    {
        return MigrationResolver.composite()
                                .skipDefaultResolvers(this.skipDefaultResolvers)
                                .resourceProvider(command.resourceProvider(this.skipDefaultResolvers, this.scanner))
                                .classProvider(command.classProvider(this.skipDefaultResolvers, this.scanner))
                                .classes(this.classes)
                                .renderer(this.renderer)
                                .customMigrationResolvers(this.customMigrationResolvers)
                                .fixedJavaMigrations(this.fixedJavaMigrations)
                                .build();
    }

    public static Migrador.Factory.PartialSpec withClassLoader(final ClassLoader classLoader)
    {
        Objects.requireNonNull(classLoader, "classLoader is required");
        return new Migrador.Factory.PartialSpec(classLoader);
    }

    public static Migrador.Factory withConfig(final MigradorConfig config)
    {
        Objects.requireNonNull(config, "config is required");
        return new MigradorFactory(config,
                                   Thread.currentThread()
                                         .getContextClassLoader());
    }

    public static String description()
    {
        final Classes classes = Classes.get();
        final String version = MigradorVersion.READER.getVersion(classes);
        return NAME + ", version: '" + version + "', by matero";
    }

    private void showVersion()
    {
        if (LOG.isInfoEnabled()) {
            final String version = MigradorVersion.READER.getVersion(this.classes);
            LOG.info("Migrador, version: '" + version + "', by YeiYei (aka matero)");
        }
    }

    private void showMemoryUsage()
    {
        if (LOG.isInfoEnabled()) {
            final Runtime runtime = Runtime.getRuntime();
            final long free = runtime.freeMemory();
            final long total = runtime.totalMemory();
            final long used = total - free;

            final long totalMB = total / (1024 * 1024);
            final long usedMB = used / (1024 * 1024);
            LOG.info("Memory usage: " + usedMB + " of " + totalMB + "MB");
        }
    }

    public enum Command
    {
        baseline {
            @Override public ResourceProvider resourceProvider(final boolean skipDefaultResolvers, final ResourceScanner scanner)
            {
                return skipDefaultResolvers ? ResourceProvider.NONE : scanner;
            }

            @Override public ClassProvider classProvider(final boolean skipDefaultResolvers, final ResourceScanner scanner)
            {
                return skipDefaultResolvers ? ClassProvider.NONE : scanner;
            }
        },
        /**
         * Drops all objects (nodes and relationships) in the configured db.
         */
        clean {
            @Override public ResourceProvider resourceProvider(final boolean skipDefaultResolvers, final ResourceScanner scanner)
            {
                return ResourceProvider.NONE;
            }

            @Override
            public ClassProvider classProvider(final boolean skipDefaultResolvers, final ResourceScanner scanner) {return ClassProvider.NONE;}
        },
        info,
        migrate,
        repair,
        validate;

        public ResourceProvider resourceProvider(final boolean skipDefaultResolvers, final ResourceScanner scanner) {return scanner;}


        public ClassProvider classProvider(final boolean skipDefaultResolvers, final ResourceScanner scanner) {return scanner;}
    }

    public interface Factory
    {
        Migrador create();

        final class PartialSpec
        {
            private final ClassLoader classLoader;

            PartialSpec(final ClassLoader classLoader) {this.classLoader = classLoader;}

            public Migrador.Factory andConfig(final MigradorConfig config)
            {
                Objects.requireNonNull(config, "config is required");
                return new MigradorFactory(config, this.classLoader);
            }
        }

        final class UnknownAuthTokenType extends MigradorException
        {
            UnknownAuthTokenType(final String type) {super("Unknown auth token type '" + type + "'.");}
        }
    }
}
