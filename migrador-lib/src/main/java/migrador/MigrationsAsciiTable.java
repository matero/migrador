/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador;

import com.github.freva.asciitable.AsciiTable;
import com.github.freva.asciitable.ColumnData;
import migrador.actions.MigrationsStatus;
import migrador.migrations.Migration;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;

public final class MigrationsAsciiTable
{
    private static final class Column
    {
        private static final ColumnData<Migration> CATEGORY = makeColumn("Category", Migration::getCategory);
        private static final ColumnData<Migration> VERSION = makeColumn("Version", Migration::getVersionStr);
        private static final ColumnData<Migration> DESCRIPTION = makeColumn("Description", Migration::getDescription);
        private static final ColumnData<Migration> TYPE = makeColumn("Type", Migration::getTypeDisplayName);
        private static final ColumnData<Migration> INSTALLED_BY = makeColumn("Installed by", Migration::getInstalledBy);
        private static final ColumnData<Migration> INSTALLED_ON = makeColumn("Installed on", Migration::getInstalledOnIdoDateTime);
        private static final ColumnData<Migration> STATE = makeColumn("State", Migration::getStateDisplayName);

        private static ColumnData<Migration> makeColumn(final String title, final Function<Migration, String> getter)
        {
            return new com.github.freva.asciitable.Column().header(title).with(getter);
        }
    }

    private static final class Table
    {
        private static final List<ColumnData<Migration>> COLUMNS = List.of(
            Column.CATEGORY, Column.VERSION, Column.DESCRIPTION, Column.TYPE, Column.INSTALLED_BY, Column.INSTALLED_ON, Column.STATE);
    }

    private final List<Migration> migrations;

    MigrationsAsciiTable(final List<Migration> migrations) {this.migrations = migrations;}

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof MigrationsAsciiTable) {
            final var other = (MigrationsAsciiTable) o;
            return this.migrations.equals(other.migrations);
        }
        return false;
    }

    @Override public int hashCode()
    {
        return this.migrations.hashCode();
    }

    @Override public String toString() {return AsciiTable.getTable(this.migrations, Table.COLUMNS);}

    public static MigrationsAsciiTable of(final Migration... migrations) {
        Objects.requireNonNull(migrations, "migrations is required");
        return new MigrationsAsciiTable(List.of(migrations));
    }

    public static MigrationsAsciiTable of(final List<Migration> migrations) {
        Objects.requireNonNull(migrations, "migrations is required");
        return new MigrationsAsciiTable(List.copyOf(migrations));
    }

    public static MigrationsAsciiTable of(final MigrationsStatus migrationsStatus) {
        Objects.requireNonNull(migrationsStatus, "migrationsStatus is required");
        return new MigrationsAsciiTable(migrationsStatus.getMigrations());
    }
}
