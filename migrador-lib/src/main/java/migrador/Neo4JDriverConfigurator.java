/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package migrador;

import org.neo4j.driver.AuthToken;
import org.neo4j.driver.AuthTokens;
import org.neo4j.driver.Config;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.neo4j.driver.Logging;
import org.neo4j.driver.exceptions.AuthenticationException;
import org.neo4j.driver.internal.logging.JULogging;
import org.neo4j.driver.internal.shaded.io.netty.util.internal.logging.InternalLoggerFactory;
import org.neo4j.driver.internal.shaded.io.netty.util.internal.logging.JdkLoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.logging.Level;

class Neo4JDriverConfigurator
{
    private static final Logger LOG = LoggerFactory.getLogger(Neo4JDriverConfigurator.class);

    final MigradorConfig config;

    Neo4JDriverConfigurator(final MigradorConfig config) {this.config = config;}

    Driver getDriver()
        throws AuthenticationException
    {
        final Driver driver = GraphDatabase.driver(getNeo4jUri(), getAuthToken(), getDriverConfig());
        try (final var session = driver.session()) {
            session.readTransaction(tx -> tx.run("RETURN 1").single().get(0).asInt());
        } catch (final AuthenticationException e) {
            driver.close();
            throw e;
        }
        return driver;
    }

    String getNeo4jUri() {return this.config.neo4jUri();}

    AuthToken getAuthToken()
    {
        final var authTokenType = this.config.authTokenType();
        if (null == authTokenType || authTokenType.trim().isEmpty()) {
            LOG.debug("No 'db.auth.tokenType' defined, assuming \"none\" as desired type.");
            return AuthTokens.none();
        }
        switch (authTokenType) {
            case "none":
                LOG.debug("Using \"none\" as desired 'db.auth.tokenType'.");
                return AuthTokens.none();
            case "basic":
                LOG.debug("Using \"basic\" as desired 'db.auth.tokenType'.");
                return getBasicAuthToken();
            case "kerberos":
                LOG.debug("Using \"kerberos\" as desired 'db.auth.tokenType'.");
                return getKerberosAuthToken();
            case "custom":
                LOG.debug("Using \"custom\" as desired 'db.auth.tokenType'.");
                return getCustomAuthToken();
            default:
                throw new Migrador.Factory.UnknownAuthTokenType(authTokenType);
        }
    }

    AuthToken getBasicAuthToken()
    {
        final var username = this.config.authPrincipal();
        final var password = this.config.authCredentials();
        final var realm = this.config.authRealm();
        return AuthTokens.basic(username, password, realm);
    }

    AuthToken getKerberosAuthToken()
    {
        return AuthTokens.kerberos(this.config.authCredentials());
    }

    AuthToken getCustomAuthToken()
    {
        final var principal = this.config.authPrincipal();
        final var credentials = this.config.authCredentials();
        final var realm = this.config.authRealm();
        final var scheme = this.config.authScheme();
        final var parameters = this.config.authParameters();
        return AuthTokens.custom(principal, credentials, realm, scheme, parameters);
    }

    Config getDriverConfig()
    {
        final Config.ConfigBuilder neo4jConfig = Config.builder()
                                                       .withMaxConnectionPoolSize(this.config.dbPoolSize())
                                                       .withConnectionAcquisitionTimeout(this.config.dbConnectionAcquisitionTimeout(),
                                                                                         this.config.dbConnectionAcquisitionTimeoutUnit())
                                                       .withMaxConnectionLifetime(this.config.dbConnectionLifetime(),
                                                                                  this.config.dbConnectionLifetimeUnit())
                                                       .withMaxTransactionRetryTime(this.config.dbTransactionRetryTime(),
                                                                                    this.config.dbTransactionRetryTimeUnit());

        // uses JDK to internal driver logs
        InternalLoggerFactory.setDefaultFactory(JdkLoggerFactory.INSTANCE);
        // sets internal driver log level at the same as the migrador-lib
        java.util.logging.Logger.getLogger("org.neo4j.driver.internal").setLevel(getLogLevel());

        String dbLogger = this.config.dbLogger();
        if (null == dbLogger || dbLogger.isBlank()) {
            dbLogger = "none";
        }

        switch (dbLogger.toLowerCase()) {
            case "none":
                neo4jConfig.withLogging(Logging.none());
                break;
            case "console":
                neo4jConfig.withLogging(Logging.console(getLogLevel()));
                break;
            case "jul":
                neo4jConfig.withLogging(new JULogging(Level.WARNING));
                break;
            case "slf4j":
                try {
                    neo4jConfig.withLogging(Logging.slf4j());
                } catch (final RuntimeException unavailability) {
                    LOG.error("SLF4J is unavailable to neo4j, defaulting to console", unavailability);
                    neo4jConfig.withLogging(Logging.console(getLogLevel()));
                }
                break;
            default:
                LOG.warn("unknown db logging {}, defaulting to console", dbLogger);
                neo4jConfig.withLogging(Logging.console(getLogLevel()));
        }

        if (this.config.dbConnectionEncrypted()) {
            neo4jConfig.withEncryption();
        } else {
            neo4jConfig.withoutEncryption();
        }
        return neo4jConfig.build();
    }

    private Level getLogLevel()
    {
        final var level = this.config.dbLoggingLevel();
        if (null == level || level.isBlank()) {
            return Level.OFF;
        }
        switch (level) {
            case "OFF":
                return Level.OFF;
            case "SEVERE":
                return Level.SEVERE;
            case "WARNING":
                return Level.WARNING;
            case "INFO":
                return Level.INFO;
            case "CONFIG":
                return Level.CONFIG;
            case "FINE":
                return Level.FINE;
            case "FINER":
                return Level.FINER;
            case "FINEST":
                return Level.FINEST;
            case "ALL":
                return Level.ALL;
            default:
                throw new IllegalArgumentException(
                    "configured db log level can be one of [OFF, SEVERE, WARNING, INFO, CONFIG, FINE, FINER, FINEST, ALL].");
        }
    }
}
